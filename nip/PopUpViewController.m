//
//  PopUpViewController.m
//  Prueba
//
//  Created by MacCMS2 on 07/04/15.
//  Copyright (c) 2015 MacCMS2. All rights reserved.
//

#import "PopUpViewController.h"
#import <QuartzCore/QuartzCore.h>

@interface PopUpViewController ()
@property (nonatomic, assign) CGPoint arrowPosition;
@end

@implementation PopUpViewController

- (void)viewDidLoad {
    
    [super viewDidLoad];
    
    self.view.backgroundColor=[[UIColor whiteColor] colorWithAlphaComponent:.5];
    self.popUp.layer.cornerRadius = 8;
    self.popUp.layer.shadowOpacity = 0.15;
    self.popUp.layer.shadowOffset = CGSizeMake(5.0f, 5.0f);
    
    self.lbText.textColor = LABEL_COLOR;
    self.lbTitle.textColor = LABEL_COLOR;
   
    
    // Do any additional setup after loading the view from its nib.
    
}

- (void)showAnimate
{
    self.view.transform = CGAffineTransformMakeScale(1.3, 1.3);
    self.view.alpha = 0;
    [UIView animateWithDuration:.25 animations:^{
        self.view.alpha = 1;
        self.view.transform = CGAffineTransformMakeScale(1, 1);
    }];   
}


- (void)removeAnimate
{
    [UIView animateWithDuration:.25 animations:^{
        self.view.transform = CGAffineTransformMakeScale(1.3, 1.3);
        self.view.alpha = 0.0;
    } completion:^(BOOL finished) {
        if (finished) {
            [self.view removeFromSuperview];
        }
    }];
}

- (IBAction)closePopup:(id)sender {
    [self removeAnimate];
}

- (void)showInView:(UIView *)aView 
{
    [aView addSubview:self.view];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
