//
//  PaymentViewController.m
//  nip
//
//  Created by MacCMS2 on 12/05/15.
//  Copyright (c) 2015 MacCMS2. All rights reserved.
//

#import "PaymentViewController.h"
#import "CartViewController.h"
#import "Customer.h"
#import "PaymentMethodTableViewCell.h"
#import "Requests.h"
#import "Stripe.h"

#define STRIPE_TEST_PUBLIC_KEY @"pk_test_0uVGnrXH5525TyTmyX5JUHDc"
#define STRIPE_TEST_POST_URL

@interface PaymentViewController ()
@property (strong, nonatomic) NSMutableArray *listCards;
@end

@implementation PaymentViewController
@synthesize listCards;
NSString *cardId;
NSIndexPath *ind;

- (void)viewDidLoad {
    [super viewDidLoad];
   
    //Header
    self.navBar = [[NavBarViewController alloc] init];
    [self.navBar showInView:self withTitle:@"Payment Method" hiddenButtom:NO];
    self.navigationController.navigationBarHidden = YES;
    self.navBar.imgAddress.hidden = YES;
    self.navBar.horizontalConstraint.constant = 0;
    [self.navBar.imgAddress removeFromSuperview];

    
    self.lbSelect.textColor = TITLE_COLOR;
    self.lbTitle.textColor = TITLE_COLOR;
    
    PTKView *view;
    
    if(![Customer isGetListCards])
    {
        [Requests getCardsForCustomer:[Customer getIdCustomer]];
    }
    
    if([Customer getListCards] != nil)
    {
        NSMutableArray *localList = [[NSMutableArray alloc] initWithArray:[Customer getListCards]];
        
        listCards = [[NSMutableArray alloc] initWithArray:localList];
    }
    else
    {
        listCards = [[NSMutableArray alloc] init];
        self.lbSelect.text = @"You do no have any payment method set up yet";

    }
    
    if([[UIScreen mainScreen] bounds].size.height <= 568)
    {
        view = [[PTKView alloc] initWithFrame:CGRectMake(self.viewAddCard.frame.size.width/2-147, self.lbTitle.frame.origin.y+self.lbTitle.frame.size.height+20, 300, 200)];
    }
    //iPhone 6 plus
    else if([[UIScreen mainScreen] bounds].size.height == 736)
    {
        view = [[PTKView alloc] initWithFrame:CGRectMake(self.viewAddCard.frame.size.width/2-100, self.lbTitle.frame.origin.y+self.lbTitle.frame.size.height+20, 300, 200)];
    }
    else
    {
       view = [[PTKView alloc] initWithFrame:CGRectMake(self.viewAddCard.frame.size.width/2-115, self.lbTitle.frame.origin.y+self.lbTitle.frame.size.height+20, 300, 200)];
    }

    self.paymentView = view;
    self.paymentView.delegate = self;
    [self.viewAddCard addSubview:self.paymentView];

    
    NSUserDefaults *datos = [NSUserDefaults standardUserDefaults];
    cardId = [datos stringForKey:@"cardId"];
    
    UITapGestureRecognizer *close = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(closeKeyboard)];
    [self.viewAddCard addGestureRecognizer:close];
    
}

-(void)closeKeyboard
{
    [self.view endEditing:YES];
    
}

- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    [self adjustHeightOfTableview];
    
}

#pragma mark - Add credit card

- (void) paymentView:(PTKView*)paymentView withCard:(PTKCard *)card isValid:(BOOL)valid
{
    NSLog(@"Card number: %@", card.number);
    NSLog(@"Card expiry: %lu/%lu", (unsigned long)card.expMonth, (unsigned long)card.expYear);
    NSLog(@"Card cvc: %@", card.cvc);
    NSLog(@"Address zip: %@", card.addressZip);

}

- (IBAction)addNewCard:(id)sender {
    
    self.stripeCard = [[STPCard alloc] init];
    self.stripeCard.number = self.paymentView.card.number;
    self.stripeCard.cvc = self.paymentView.card.cvc;
    self.stripeCard.expMonth = self.paymentView.card.expMonth;
    self.stripeCard.expYear =  self.paymentView.card.expYear;
    
    if ([self validateCustomerInfo]) {
        [self performSelectorInBackground:@selector(startAnimation) withObject:self];
        [self performStripeOperation];
    }
}

- (BOOL)validateCustomerInfo {
    
    UIAlertView * alert = [[UIAlertView alloc] initWithTitle:@"Whoops!"
                                                     message:@"Seems like some fields are missing"
                                                    delegate:nil
                                           cancelButtonTitle:@"Ok"
                                           otherButtonTitles:nil];
    
    
    //Validate card number, CVC, expMonth, expYear
    NSError* error = nil;
    [self.stripeCard validateCardReturningError:&error];
    
    if (error) {
        alert.message = [error localizedDescription];
        [alert show];
        return NO;
    }
    
    return YES;
}

- (void)performStripeOperation {
    
    self.btAdd.enabled = NO;
    STPAPIClient *client = [[STPAPIClient alloc] initWithPublishableKey:STRIPE_TEST_PUBLIC_KEY];
    [client createTokenWithCard:self.stripeCard completion:^(STPToken* token, NSError* error) {
        if(error)
        {
            NSLog(@"%@", error);
            [Requests stopAnimationInView];
        }
        else
        {
            [self addCardWithToken:token.tokenId];
        }
    }];
    
}

-(void)addCardWithToken: (NSString *)token
{
    NSString *response =[Requests addCardWithToken:token];
    if([response isEqualToString:@"true"])
    {
        [self updateCards];
        
        [Requests stopAnimationInView];
        
        [self.tableView reloadData];
        
        [self adjustHeightOfTableview];
        
        self.lbSelect.text = @"Select your payment method";
    }
    else
    {
        [Requests stopAnimationInView];
        UIAlertView * alert = [[UIAlertView alloc] initWithTitle:@"Something went wrong"
                                                         message:@"We couldn't add your card"
                                                        delegate:nil
                                               cancelButtonTitle:@"Ok"
                                               otherButtonTitles:nil];
        [alert show];
        
    }
    self.btAdd.enabled = YES;
}

-(void)updateCards
{
    [Requests getCardsForCustomer:[Customer getIdCustomer]];
    [listCards removeAllObjects];
    [listCards addObjectsFromArray:[Customer getListCards]];
}



#pragma mark - Table View


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 80;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
   return [listCards count];
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    PaymentMethodTableViewCell *cell =  [tableView dequeueReusableCellWithIdentifier:@"myCellCards"];
    
    if(!cell)
    {
        [tableView  registerNib:[UINib nibWithNibName:@"PaymentMethodTableViewCell" bundle:nil]forCellReuseIdentifier:@"myCellCards"];
        cell =  [tableView  dequeueReusableCellWithIdentifier:@"myCellCards"];
    }
    
        cell.lbEndingIn.text = [NSString stringWithFormat:@"Ending in %@", [[listCards valueForKey:@"last4"] objectAtIndex:indexPath.row]];
        cell.lbDate.text = [NSString stringWithFormat:@"%@/%@", [[listCards valueForKey:@"exp_month"] objectAtIndex:indexPath.row], [[listCards valueForKey:@"exp_year"] objectAtIndex:indexPath.row]];
    
        if([cardId isEqualToString:[[listCards valueForKey:@"id"] objectAtIndex:indexPath.row]])
        {
            [tableView
             selectRowAtIndexPath:indexPath
             animated:TRUE
             scrollPosition:UITableViewScrollPositionNone
             ];
            ind = indexPath;
            
             cell.imgEdit.image = [UIImage imageNamed:@"EditWhite"];
        }
          
        NSString *cardType = [[listCards valueForKey:@"type"] objectAtIndex:indexPath.row];
        NSString *cardTypeName = @"placeholder";
        
        if([cardType isEqualToString:@"American Express"])
        {
            cardTypeName = @"amex";
        }
        else if([cardType isEqualToString:@"Diners Club"])
        {
            cardTypeName = @"diners";
        }
        else if([cardType isEqualToString:@"Discover"])
        {
            cardTypeName = @"discover";
        }
        else if([cardType isEqualToString:@"JCB"])
        {
            cardTypeName = @"jcb";
        }
        else if([cardType isEqualToString:@"MasterCard"])
        {
            cardTypeName = @"mastercard";
        }
        else if([cardType isEqualToString:@"Visa"])
        {
            cardTypeName = @"visa";
        }
        
        cell.imgCardType.image = [UIImage imageNamed:cardTypeName];
        
      //  return cell;
    //}
    
    return cell;
    
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(ind.row != indexPath.row)
    {
        [self.tableView deselectRowAtIndexPath:ind animated:YES];
        
        //Change images the row previous
        PaymentMethodTableViewCell *prevCell = ((PaymentMethodTableViewCell*)[tableView cellForRowAtIndexPath:ind]);
        prevCell.imgEdit.image = [UIImage imageNamed:@"Edit"];
    }
    
    ind = indexPath;
    
    //Change images the row selected
    PaymentMethodTableViewCell *tmpCell = ((PaymentMethodTableViewCell*)[tableView cellForRowAtIndexPath:indexPath]);
    tmpCell.imgEdit.image = [UIImage imageNamed:@"EditWhite"];
    
    NSUserDefaults *datos = [NSUserDefaults standardUserDefaults];
    
    [datos setObject:[[listCards valueForKey:@"id"] objectAtIndex:indexPath.row ]forKey:@"cardId"];
    [datos synchronize];
    
    NSLog(@"%li", (long)indexPath.row);
    
    [self performSelector:@selector(back) withObject:nil afterDelay:0.0];
}


- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    return YES;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    
}

-(NSArray *)tableView:(UITableView *)tableView editActionsForRowAtIndexPath:(NSIndexPath *)indexPath {
  
    //Change images the row previous
    PaymentMethodTableViewCell *prevCell = ((PaymentMethodTableViewCell*)[tableView cellForRowAtIndexPath:ind]);
    prevCell.imgEdit.image = [UIImage imageNamed:@"Edit"];
    
    //Change images the row selected
    PaymentMethodTableViewCell *tmpCell = ((PaymentMethodTableViewCell*)[tableView cellForRowAtIndexPath:indexPath]);
    tmpCell.imgEdit.image = [UIImage imageNamed:@"Edit"];
    
    UITableViewRowAction *button = [UITableViewRowAction rowActionWithStyle:UITableViewRowActionStyleDefault title:@"X" handler:^(UITableViewRowAction *action, NSIndexPath *indexPath)
                                    {
                                       [self performSelectorInBackground:@selector(startAnimation) withObject:self];
                                       NSString *response = [Requests deleteCardForId:[[listCards valueForKey:@"id"]objectAtIndex:indexPath.row]];
                                        [Requests stopAnimationInView];

                                        if([response isEqualToString:@"1"])
                                        {
                                            [self performSelectorInBackground:@selector(updateCards) withObject:self];
                                            [listCards removeObjectAtIndex:indexPath.row];
                                            [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
                                            
                                            [self performSelector:@selector(adjustHeightOfTableview) withObject:nil afterDelay:0.3];
                                            
                                            if([listCards count] <= 0)
                                            {
                                                [[NSUserDefaults standardUserDefaults] removeObjectForKey:@"cardId"];
                                                self.lbSelect.text = @"You do no have any payment method set up yet";
                                            }
                                        }
                                        else
                                        {
                                            UIAlertView * alert = [[UIAlertView alloc] initWithTitle:@"Something went wrong"
                                                                                             message:@"We couldn't delete your card"
                                                                                            delegate:nil
                                                                                   cancelButtonTitle:@"Ok"
                                                                                   otherButtonTitles:nil];
                                            [alert show];
                                        }
                                        
                                    }];
    button.backgroundColor = [[UIColor blackColor]colorWithAlphaComponent:0.1];
    
    return @[button];
}

- (UIImage *)imageWithImage:(UIImage *)image scaledToSize:(CGSize)newSize {
    //UIGraphicsBeginImageContext(newSize);
    // In next line, pass 0.0 to use the current device's pixel scaling factor (and thus account for Retina resolution).
    // Pass 1.0 to force exact pixel size.
    UIGraphicsBeginImageContextWithOptions(newSize, NO, 0.0);
    [image drawInRect:CGRectMake(0, 0, newSize.width, newSize.height)];
    UIImage *newImage = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    return newImage;
}

- (void)adjustHeightOfTableview
{
    CGFloat height = [listCards count]*80;
    CGFloat maxHeight = self.tableView.superview.frame.size.height - self.tableView.frame.origin.y;
    
    // if the height of the content is greater than the maxHeight of
    // total space on the screen, limit the height to the size of the
    // superview.
    
    if (height > maxHeight)
        height = maxHeight;
    
    // now set the height constraint accordingly
    
    [UIView animateWithDuration:0.25 animations:^{
        self.tableViewHeightConstraint.constant = height;
        [self.view setNeedsUpdateConstraints];
    }];
}


#pragma mark -

-(void)back
{
    CartViewController *cart = [[CartViewController alloc] init];
    cart.addressIndex = self.addressIndex;
    
    [self.navBar pushTransition:self];
    
    [self.navigationController pushViewController:cart animated:NO];
}

-(void)startAnimation
{
    [Requests startAnimationInView:self];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
