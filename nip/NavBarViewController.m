//
//  NavBarViewController.m
//  Prueba
//
//  Created by MacCMS2 on 07/04/15.
//  Copyright (c) 2015 MacCMS2. All rights reserved.
//

#import "NavBarViewController.h"


@interface NavBarViewController ()

@end

@implementation NavBarViewController

- (void)viewDidLoad {
    
    int width =[[ UIScreen mainScreen ] bounds ].size.width;
   
    [self.view setFrame:CGRectMake(0, 0, width, self.view.frame.size.height)];
    
    self.view.backgroundColor = GREEN_COLOR;
        
    self.imgCart.hidden = YES;
    self.btCart.enabled = NO;

    [super viewDidLoad];   
    
    
    // Do any additional setup after loading the view from its nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (void)showInView:(UIViewController *)aView withTitle: (NSString *)title hiddenButtom: (BOOL)yesOrNot
{
   [aView.view addSubview:self.view];
    self.lbAddress.text = title;
    
    if(yesOrNot)
    {
         UITapGestureRecognizer *menu = [[UITapGestureRecognizer alloc] initWithTarget:aView action:@selector(showMenu)];
        [self.btMenu addGestureRecognizer:menu];

    }
    else
    {
        self.imgMenu.hidden = YES;
        self.imgBck.hidden = NO;
      
        UITapGestureRecognizer *backTap = [[UITapGestureRecognizer alloc] initWithTarget:aView action:@selector(back)];
        [self.btMenu addGestureRecognizer:backTap];
    }
    
    UITapGestureRecognizer *address = [[UITapGestureRecognizer alloc] initWithTarget:aView action:@selector(goToAddresses)];
    [self.btAddress addGestureRecognizer:address];
    
}


-(void)popTransition: (UIViewController *)view
{
    [UIView animateWithDuration:0.75
                     animations:^{
                         [UIView setAnimationCurve:UIViewAnimationCurveEaseInOut];
                         [UIView setAnimationTransition:UIViewAnimationTransitionFlipFromLeft forView:view.navigationController.view cache:NO];
                     }];

}

-(void)pushTransition: (UIViewController *)view
{
    [UIView animateWithDuration:0.75
                     animations:^{
                         [UIView setAnimationCurve:UIViewAnimationCurveEaseInOut];
                         [UIView setAnimationTransition:UIViewAnimationTransitionFlipFromRight forView:view.navigationController.view cache:NO];
                     }];
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
