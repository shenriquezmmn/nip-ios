//
//  Requests.h
//  nip
//
//  Created by MacCMS2 on 24/04/15.
//  Copyright (c) 2015 MacCMS2. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Requests : NSObject 

+(BOOL)loginForUser:(NSString *) email firtsName: (NSString *) first_name lastName: (NSString *) last_name photo: (NSString *) photoCustomer;
+(id)addAddressForCustomer: (NSString *) customerId deliveryZip: (NSString*) zip deliveryStreet:(NSString *) street deliveryUnit: (NSString*) building deliveryCity: (NSString *) city deliveryState: (NSString *) state placeAlias: (NSString *) addressLabel phoneNumber: (NSString *) phone_number;
+(NSString *)getAddressForCustomer: (NSString *) customerId;

+(NSString *)updateAddressForCustomer: (NSString *) customerId addressId: (NSString *) addressId deliveryZip: (NSString*) zip deliveryStreet:(NSString *) street deliveryUnit: (NSString*) building deliveryCity: (NSString *) city deliveryState: (NSString *) state placeAlias: (NSString *) addressLabel phoneNumber: (NSString *) phone_number;

+(NSString *)placaOrderForCart: (NSArray *)cart token:(NSString*)token orCard:(NSString *) card amount: (NSInteger) amount andAddressIndex:(int) addressIndex priceTotal:(float)total subtotal:(float)subtotal;

+(id)getCardsForCustomer: (NSString *) customerId;
+(id)addCardWithToken:(NSString *)token;

+(NSString *)menuForZipCode: (NSString *) zipCode;
+(id)deleteCardForId:(NSString *)cardId;

+(void)startAnimationInView:(UIViewController *)vista;
+(void)stopAnimationInView;
+(NSString *)deleteAddressWithId: (NSString *) addressId;
+(NSDictionary *)getOrdersForCustomer:(NSString *)customerId;
+(NSDictionary *)getDetailsForOrder:(NSString *)ordergenerateid;
+(BOOL)addRatingForMenu:(NSString *)menuId restaurant_id:(NSString *) resid customer_id: (NSString *)cusid ordergenerateid: (NSString *) ordergenerateid rating: (NSString *) rating;

+(BOOL)isNetworkAvailable;
@end
