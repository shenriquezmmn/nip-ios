//
//  AddressesViewController.h
//  NIP
//
//  Created by MacCMS2 on 18/05/15.
//  Copyright (c) 2015 MacCMS2. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "VisibleFormViewController.h"

@interface AddressesViewController : UIViewController<UITabBarDelegate, UITextFieldDelegate, UITableViewDataSource>

//Header
@property (strong, nonatomic) NavBarViewController *navBar;

@property (weak, nonatomic) IBOutlet UILabel *lbSelect;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UILabel *lbAdd;

@property (weak, nonatomic) IBOutlet UILabel *lbStreet;
@property (weak, nonatomic) IBOutlet UILabel *lbUnit;
@property (weak, nonatomic) IBOutlet UILabel *lbCity;
@property (weak, nonatomic) IBOutlet UILabel *lbState;
@property (weak, nonatomic) IBOutlet UILabel *lbAddresLabel;
@property (weak, nonatomic) IBOutlet UILabel *lbPhoneNumber;
@property (weak, nonatomic) IBOutlet UILabel *lbZipCode;

@property (weak, nonatomic) IBOutlet UITextField *txStreet;
@property (weak, nonatomic) IBOutlet UITextField *txUnit;
@property (weak, nonatomic) IBOutlet UITextField *txCity;
@property (weak, nonatomic) IBOutlet UITextField *txState;
@property (weak, nonatomic) IBOutlet UITextField *txAddresLabel;
@property (weak, nonatomic) IBOutlet UITextField *txPhoneNumber;
@property (weak, nonatomic) IBOutlet UITextField *txZipCode;
@property (weak, nonatomic) IBOutlet NSLayoutConstraint *tableViewHeightConstraint;
@property (weak, nonatomic) IBOutlet UIView *viewAdd;
@property (weak, nonatomic) IBOutlet UIView *viewScroll;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@property (strong, nonatomic) IBOutlet UIView *viewPopUp;
@property (weak, nonatomic) IBOutlet UIView *popUp;

@property (strong, nonatomic) NSString *zipCode;
@property (strong, nonatomic) NSString *street;
@property (strong, nonatomic) NSString *numStreet;
@property (strong, nonatomic) NSString *city;
@property (strong, nonatomic) NSString *state;
@end
