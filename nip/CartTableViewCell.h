//
//  CartTableViewCell.h
//  nip
//
//  Created by MacCMS2 on 06/05/15.
//  Copyright (c) 2015 MacCMS2. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface CartTableViewCell : UITableViewCell

//Labels Cart
@property (weak, nonatomic) IBOutlet UILabel *lbRestaurantName;
@property (weak, nonatomic) IBOutlet UILabel *lbMenuName;
@property (weak, nonatomic) IBOutlet UILabel *lbPriceMenu;
@property (weak, nonatomic) IBOutlet UILabel *lbQuantity;
@property (weak, nonatomic) IBOutlet UILabel *lbNumber;

//Image
@property (weak, nonatomic) IBOutlet UIImageView *imgMenu;

//Buttom
@property (weak, nonatomic) IBOutlet UIButton *btLess;
@property (weak, nonatomic) IBOutlet UIButton *btMore;


@property (nonatomic) int cont;
@end
