//
//  Requests.m
//  nip
//
//  Created by MacCMS2 on 24/04/15.
//  Copyright (c) 2015 MacCMS2. All rights reserved.
//

#import "Requests.h"
#import "ParseJson.h"
#import "Customer.h"
#import "Dishes.h"
#import "indicatorActivityView.h"
#import "Reachability.h"


@implementation Requests

indicatorActivityView * indicador;

#pragma mark - Customer

+(BOOL)loginForUser:(NSString *) email firtsName: (NSString *) first_name lastName: (NSString *) last_name photo: (NSString *) photoCustomer
{
    NSString *urlString = @"";
    urlString = [urlString stringByAppendingString:@"email="];
    urlString = [urlString stringByAppendingString:email];
    urlString = [urlString stringByAppendingString:@"&first_name="];
    urlString = [urlString stringByAppendingString:first_name];
    urlString = [urlString stringByAppendingString:@"&last_name="];
    urlString = [urlString stringByAppendingString:last_name];
    urlString = [urlString stringByAppendingString:@"&action=FBLogin"];
    
    NSLog(@"url: %@", urlString);
    
    NSURL *url = [NSURL URLWithString:WS_LOGIN];
    NSString *length = [NSString stringWithFormat:@"%lu",(unsigned long)[urlString length]];
    NSMutableURLRequest *request =	[NSMutableURLRequest requestWithURL:url];
    [request setHTTPMethod:@"POST"];
    [request addValue:length forHTTPHeaderField:@"Content-Length"];
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPBody:[urlString dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSURLResponse *response;
    NSError *error = nil;
    NSData *receivedData = [NSURLConnection sendSynchronousRequest:request
                                                 returningResponse:&response
                                                             error:&error];
    NSString *responseString = [[NSString alloc] initWithData:receivedData encoding:NSUTF8StringEncoding];
    //NSLog(@"%@",responseString);
    NSDictionary *data = [ParseJson parseJsonData:[responseString dataUsingEncoding:NSUTF8StringEncoding]];
    
    if(error)
    {        
        return NO;
    }
    else
    {
        // test the result
        if ([[data objectForKey:@"success"]  isEqual: @"1"]) {
            
            [Flurry logEvent:@"Login"];
            NSLog(@"Login successfully");
            NSString *userid =[NSString stringWithFormat:@"%@",[data objectForKey:@"userid"]];
            
            NSMutableDictionary *custumerDict = [[NSMutableDictionary alloc]initWithObjectsAndKeys:first_name, @"firtsname", last_name, @"lastname",email, @"email",userid, @"customerid", photoCustomer, @"idphoto",  nil];
            
            [Customer setCustomer:custumerDict];
            
            //List cards
            [Requests getCardsForCustomer:[Customer getIdCustomer]];
            return YES;
            
        } else {
            NSLog(@"Error parsing login!");
            return NO;
        }
    }
     return YES;
}

+(id)addAddressForCustomer: (NSString *) customerId deliveryZip: (NSString*) zip deliveryStreet:(NSString *) street deliveryUnit: (NSString*) building deliveryCity: (NSString *) city deliveryState: (NSString *) state placeAlias: (NSString *) addressLabel phoneNumber: (NSString *) phone_number
{
    NSString *urlString = @"";
    urlString = [urlString stringByAppendingString:@"cusid="];
    urlString = [urlString stringByAppendingString:customerId];
    urlString = [urlString stringByAppendingString:@"&action=addressadd"];
    urlString = [urlString stringByAppendingString:@"&street="];
    urlString = [urlString stringByAppendingString:street];
    urlString = [urlString stringByAppendingString:@"&building="];
    urlString = [urlString stringByAppendingString:building];
    urlString = [urlString stringByAppendingString:@"&city="];
    urlString = [urlString stringByAppendingString:city];
    urlString = [urlString stringByAppendingString:@"&state="];
    urlString = [urlString stringByAppendingString:state];
    urlString = [urlString stringByAppendingString:@"&address_title="];
    urlString = [urlString stringByAppendingString:addressLabel];
    urlString = [urlString stringByAppendingString:@"&phone_number="];
    urlString = [urlString stringByAppendingString:phone_number];
    urlString = [urlString stringByAppendingString:@"&zip="];
    urlString = [urlString stringByAppendingString:zip];
    
    
        NSLog(@"url: %@", urlString);
    
    NSURL *url = [NSURL URLWithString:WS_ADDRESS_BOOK];
    NSString *length = [NSString stringWithFormat:@"%lu",(unsigned long)[urlString length]];
    NSMutableURLRequest *request =	[NSMutableURLRequest requestWithURL:url];
    [request setHTTPMethod:@"POST"];
    [request addValue:length forHTTPHeaderField:@"Content-Length"];
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPBody:[urlString dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSURLResponse *response;
    NSError *error = nil;
    NSData *receivedData = [NSURLConnection sendSynchronousRequest:request
                                                 returningResponse:&response
                                                             error:&error];
    NSString *responseString = [[NSString alloc] initWithData:receivedData encoding:NSUTF8StringEncoding];
    //NSLog(@"%@",responseString);
    NSDictionary *data = [ParseJson parseJsonData:[responseString dataUsingEncoding:NSUTF8StringEncoding]];
    
    // test the result
    if ([[data objectForKey:@"success"]  isEqual: @"1"]) {
     [Flurry logEvent:@"Add_Address"];
        NSLog(@"Add address successfully");
        
     } else {
     NSLog(@"Error parsing add address!");
         
     }
    return [data objectForKey:@"success"];
}

+(NSString *)updateAddressForCustomer: (NSString *) customerId addressId: (NSString *) addressId deliveryZip: (NSString*) zip deliveryStreet:(NSString *) street deliveryUnit: (NSString*) building deliveryCity: (NSString *) city deliveryState: (NSString *) state placeAlias: (NSString *) addressLabel phoneNumber: (NSString *) phone_number
{
    NSString *urlString = @"";
    urlString = [urlString stringByAppendingString:@"cusid="];
    urlString = [urlString stringByAppendingString:customerId];
    urlString = [urlString stringByAppendingString:@"&addressid="];
    urlString = [urlString stringByAppendingString:addressId];
    urlString = [urlString stringByAppendingString:@"&action=addressupdate"];
    urlString = [urlString stringByAppendingString:@"&street="];
    urlString = [urlString stringByAppendingString:street];
    urlString = [urlString stringByAppendingString:@"&building="];
    urlString = [urlString stringByAppendingString:building];
    urlString = [urlString stringByAppendingString:@"&city="];
    urlString = [urlString stringByAppendingString:city];
    urlString = [urlString stringByAppendingString:@"&state="];
    urlString = [urlString stringByAppendingString:state];
    urlString = [urlString stringByAppendingString:@"&address_title="];
    urlString = [urlString stringByAppendingString:addressLabel];
    urlString = [urlString stringByAppendingString:@"&phone_number="];
    urlString = [urlString stringByAppendingString:phone_number];
    urlString = [urlString stringByAppendingString:@"&zip="];
    urlString = [urlString stringByAppendingString:zip];
    
    
    NSLog(@"url: %@", urlString);
    
    NSURL *url = [NSURL URLWithString:WS_ADDRESS_BOOK];
    NSString *length = [NSString stringWithFormat:@"%lu",(unsigned long)[urlString length]];
    NSMutableURLRequest *request =	[NSMutableURLRequest requestWithURL:url];
    [request setHTTPMethod:@"POST"];
    [request addValue:length forHTTPHeaderField:@"Content-Length"];
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPBody:[urlString dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSURLResponse *response;
    NSError *error = nil;
    NSData *receivedData = [NSURLConnection sendSynchronousRequest:request
                                                 returningResponse:&response
                                                             error:&error];
    NSString *responseString = [[NSString alloc] initWithData:receivedData encoding:NSUTF8StringEncoding];
    //NSLog(@"%@",responseString);
    NSDictionary *data = [ParseJson parseJsonData:[responseString dataUsingEncoding:NSUTF8StringEncoding]];
    
    // test the result
    if ([[data objectForKey:@"success"]  isEqual: @"1"]) {
        [Flurry logEvent:@"Edit_Address"];

        NSLog(@"Update address successfully");
    } else {
        NSLog(@"Error parsing update address!");
        
    }
    return [data objectForKey:@"success"];
}

+(NSString *)deleteAddressWithId: (NSString *) addressId
{
    NSString *urlString = @"";
    urlString = [urlString stringByAppendingString:@"address_id="];
    urlString = [urlString stringByAppendingString:addressId];
    urlString = [urlString stringByAppendingString:@"&action=delete_address"];
    
    
    NSLog(@"url: %@", urlString);
    
    NSURL *url = [NSURL URLWithString:WS_ADDRESS_BOOK];
    NSString *length = [NSString stringWithFormat:@"%lu",(unsigned long)[urlString length]];
    NSMutableURLRequest *request =	[NSMutableURLRequest requestWithURL:url];
    [request setHTTPMethod:@"POST"];
    [request addValue:length forHTTPHeaderField:@"Content-Length"];
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPBody:[urlString dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSURLResponse *response;
    NSError *error = nil;
    NSData *receivedData = [NSURLConnection sendSynchronousRequest:request
                                                 returningResponse:&response
                                                             error:&error];
    NSString *responseString = [[NSString alloc] initWithData:receivedData encoding:NSUTF8StringEncoding];
    //NSLog(@"%@",responseString);
    NSDictionary *data = [ParseJson parseJsonData:[responseString dataUsingEncoding:NSUTF8StringEncoding]];
    
    // test the result
    if ([[data objectForKey:@"success"]  isEqual: @"1"]) {
        [Flurry logEvent:@"Delete_Address"];

        NSLog(@"Delete address successfully");
    } else {
        NSLog(@"Error parsing delete addreess!");
        
    }
    return [data objectForKey:@"success"];
    
}



+(NSString *)getAddressForCustomer: (NSString *) customerId
{
    NSString *urlString = @"";
    urlString = [urlString stringByAppendingString:@"cusid="];
    urlString = [urlString stringByAppendingString:customerId];
    urlString = [urlString stringByAppendingString:@"&action=addresslist"];
 
    
    NSLog(@"url: %@", urlString);
    
    NSURL *url = [NSURL URLWithString:WS_ADDRESS_BOOK];
    NSString *length = [NSString stringWithFormat:@"%lu",(unsigned long)[urlString length]];
    NSMutableURLRequest *request =	[NSMutableURLRequest requestWithURL:url];
    [request setHTTPMethod:@"POST"];
    [request addValue:length forHTTPHeaderField:@"Content-Length"];
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPBody:[urlString dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSURLResponse *response;
    NSError *error = nil;
    NSData *receivedData = [NSURLConnection sendSynchronousRequest:request
                                                 returningResponse:&response
                                                             error:&error];
    NSString *responseString = [[NSString alloc] initWithData:receivedData encoding:NSUTF8StringEncoding];
    //NSLog(@"%@",responseString);
    NSDictionary *data = [ParseJson parseJsonData:[responseString dataUsingEncoding:NSUTF8StringEncoding]];
    
    if(error)
    {
         NSLog(@"Error getting addresses!");
        [Customer setIsGetAddresses:NO];
        return @"-2";
    }
    
    // test the result
    if ([[data objectForKey:@"success"] isEqual: @"1"]) {
        NSLog(@"Get addreeses successfully");
       
        NSSortDescriptor *alphaNumSD = [NSSortDescriptor sortDescriptorWithKey:@"id"
                                                                     ascending:YES
                                                                    comparator:^(NSString *string1, NSString *string2)
        {
            return [string1 compare:string2 options:NSNumericSearch];
        }];
        
        NSArray *temp = [NSArray arrayWithArray:[data objectForKey:@"Address"]];
        NSMutableArray *sortedArray = [temp sortedArrayUsingDescriptors:@[alphaNumSD]];
        [Customer setAddresses:sortedArray];
        [Customer setIsGetAddresses:YES];
         return @"1";
        
    } else if ([[data objectForKey:@"success"] isEqual: @"0"]) {
        
        NSLog(@" No address found");
        [Customer setIsGetAddresses:YES];
         return @"0";
    }
    else
    {
        NSLog(@"Error parsing get addreeses!");
        [Customer setIsGetAddresses:NO];
         return @"-1";
    }   
}



#pragma mark - Cards
+(id)getCardsForCustomer: (NSString *) customerId
{
    NSString *urlString = @"";
    urlString = [urlString stringByAppendingString:@"customer_id="];
    urlString = [urlString stringByAppendingString:customerId];
    urlString = [urlString stringByAppendingString:@"&action=listCards"];
    
    NSLog(@"url: %@", urlString);
    
    NSURL *url = [NSURL URLWithString:WS_CHECKOUT];
    NSString *length = [NSString stringWithFormat:@"%lu",(unsigned long)[urlString length]];
    NSMutableURLRequest *request =	[NSMutableURLRequest requestWithURL:url];
    [request setHTTPMethod:@"POST"];
    [request addValue:length forHTTPHeaderField:@"Content-Length"];
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPBody:[urlString dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSURLResponse *response;
    NSError *error = nil;
    NSData *receivedData = [NSURLConnection sendSynchronousRequest:request
                                                 returningResponse:&response
                                                             error:&error];
    NSString *responseString = [[NSString alloc] initWithData:receivedData encoding:NSUTF8StringEncoding];
    //NSLog(@"%@",responseString);
    NSMutableArray *data = [ParseJson parseJsonData:[responseString dataUsingEncoding:NSUTF8StringEncoding]];
    
    // test the result
    if ([[data valueForKey:@"success"]  isEqual: @"1"]) {
        NSLog(@"Get list cards successfully");
        [Customer setListCards:[data valueForKey:@"list_cards"]];
        
    } else {
        NSLog(@"Error parsing get list cards!");
        
    }
    return [data valueForKey:@"success"];
}

+(id)addCardWithToken:(NSString *)token
{
    NSString *urlString = @"";
    urlString = [urlString stringByAppendingString:@"customer_id="];
    urlString = [urlString stringByAppendingString:[Customer getIdCustomer]];
    urlString = [urlString stringByAppendingString:@"&token="];
    urlString = [urlString stringByAppendingString:token];
    urlString = [urlString stringByAppendingString:@"&contactemail="];
    urlString = [urlString stringByAppendingString:[Customer getEmail]];
    urlString = [urlString stringByAppendingString:@"&action=addCard"];
    
    NSLog(@"url: %@", urlString);
    
    NSURL *url = [NSURL URLWithString:WS_CHECKOUT];
    NSString *length = [NSString stringWithFormat:@"%lu",(unsigned long)[urlString length]];
    NSMutableURLRequest *request =	[NSMutableURLRequest requestWithURL:url];
    [request setHTTPMethod:@"POST"];
    [request addValue:length forHTTPHeaderField:@"Content-Length"];
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPBody:[urlString dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSURLResponse *response;
    NSError *error = nil;
    NSData *receivedData = [NSURLConnection sendSynchronousRequest:request
                                                 returningResponse:&response
                                                             error:&error];
    NSString *responseString = [[NSString alloc] initWithData:receivedData encoding:NSUTF8StringEncoding];
    //NSLog(@"%@",responseString);
    NSMutableArray *data = [ParseJson parseJsonData:[responseString dataUsingEncoding:NSUTF8StringEncoding]];
    
    // test the result
    if ([[data valueForKey:@"success"]  isEqual: @"1"]) {
        [Flurry logEvent:@"Add_Card"];

        NSLog(@"Add card successfully");
        
    } else {
        NSLog(@"Error parsing add card!");
        
    }
    return [data valueForKey:@"message"];
}

+(id)deleteCardForId:(NSString *)cardId
{
    NSString *urlString = @"";
    urlString = [urlString stringByAppendingString:@"customer_id="];
    urlString = [urlString stringByAppendingString:[Customer getIdCustomer]];
    urlString = [urlString stringByAppendingString:@"&cardId="];
    urlString = [urlString stringByAppendingString:cardId];
    urlString = [urlString stringByAppendingString:@"&action=deleteCard"];
    
    NSLog(@"url: %@", urlString);
    
    NSURL *url = [NSURL URLWithString:WS_CHECKOUT];
    NSString *length = [NSString stringWithFormat:@"%lu",(unsigned long)[urlString length]];
    NSMutableURLRequest *request =	[NSMutableURLRequest requestWithURL:url];
    [request setHTTPMethod:@"POST"];
    [request addValue:length forHTTPHeaderField:@"Content-Length"];
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPBody:[urlString dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSURLResponse *response;
    NSError *error = nil;
    NSData *receivedData = [NSURLConnection sendSynchronousRequest:request
                                                 returningResponse:&response
                                                             error:&error];
    NSString *responseString = [[NSString alloc] initWithData:receivedData encoding:NSUTF8StringEncoding];
    //NSLog(@"%@",responseString);
    NSMutableArray *data = [ParseJson parseJsonData:[responseString dataUsingEncoding:NSUTF8StringEncoding]];
    
    // test the result
    if ([[data valueForKey:@"success"]  isEqual: @"1"]) {
        [Flurry logEvent:@"Delete_Card"];

        NSLog(@"Delete card successfully");
        
    } else {
        NSLog(@"Error parsing delete card!");
        
    }
    return [data valueForKey:@"success"];
}

#pragma mark - Rating
#pragma mark -


#pragma mark - Dishes

+(NSString *)menuForZipCode: (NSString *) zipCode
{
    NSString *urlString = @"";
    urlString = [urlString stringByAppendingString:@"zipcode="];
    urlString = [urlString stringByAppendingString:zipCode];
    
    NSLog(@"url: %@", urlString);
    
    NSURL *url = [NSURL URLWithString:WS_MENU_LIST];
    NSString *length = [NSString stringWithFormat:@"%lu",(unsigned long)[urlString length]];
    NSMutableURLRequest *request =	[NSMutableURLRequest requestWithURL:url];
    [request setHTTPMethod:@"POST"];
    [request addValue:length forHTTPHeaderField:@"Content-Length"];
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPBody:[urlString dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSURLResponse *response;
    NSError *error = nil;
    NSData *receivedData = [NSURLConnection sendSynchronousRequest:request
                                                 returningResponse:&response
                                                             error:&error];
    NSString *responseString = [[NSString alloc] initWithData:receivedData encoding:NSUTF8StringEncoding];
    //NSLog(@"%@",responseString);
    NSDictionary *data = [ParseJson parseJsonData:[responseString dataUsingEncoding:NSUTF8StringEncoding]];
    
    if(error)
    {
        return @"-2";
    }
    
    if([[data objectForKey:@"success"]  isEqual: @"1"])
    {
        NSLog(@"Get Menu List successfully");
        
        if ([[data valueForKey:@"menu_list"] objectAtIndex:0] != nil)
        {
            NSDictionary *menu_list = data;
            [Dishes setMenu:menu_list];
        }
    }
    else {
        NSLog(@"Error parsing menu list!");
        [Dishes setMenu:[[NSDictionary alloc]init]];
       
    }
    
    return [data objectForKey:@"success"];
}

#pragma mark -


#pragma mark - PlaceOrder
+(NSString *)placaOrderForCart: (NSArray *)cart token:(NSString*)token orCard:(NSString *) card amount: (NSInteger) amount andAddressIndex:(int) addressIndex priceTotal:(float)total subtotal:(float)subtotal
{
    NSDictionary *address = [Customer getAddresses];
    
    NSString *urlString = @"";
    
    if([card isEqualToString:@""])
    {
        //Token
        urlString = [urlString stringByAppendingString:@"action=OrderRegister&stripeToken="];
        urlString = [urlString stringByAppendingString:token];
    }
    else
    {
        //Token
        urlString = [urlString stringByAppendingString:@"action=OrderRegister&cardId="];
        urlString = [urlString stringByAppendingString:card];
    }
    
    //Order Details
    urlString = [urlString stringByAppendingString:@"&deliveryamount="];
    urlString = [urlString stringByAppendingString:[NSString stringWithFormat:@"%ld",(long)amount]];
    urlString = [urlString stringByAppendingString:@"&ordertotalprice="];
    urlString = [urlString stringByAppendingString:[NSString stringWithFormat:@"%.02f",total]];
    urlString = [urlString stringByAppendingString:@"&subtotal="];
    urlString = [urlString stringByAppendingString:[NSString stringWithFormat:@"%.02f",subtotal]];
    
    NSMutableArray *cartSemiFinal = [[NSMutableArray alloc] init];
   
    for (int i=0; i<[cart count]; i++) {
        NSPredicate *predicate = [NSPredicate predicateWithFormat:@"restaurant_name == %@", [[cart valueForKey:@"restaurant_name"] objectAtIndex:i]];
        NSArray *result = [cart filteredArrayUsingPredicate:predicate];
        [cartSemiFinal addObject:result];
    }
    
    NSMutableArray *cartDetails = [NSMutableArray array];
    
    for (id obj in cartSemiFinal) {
        if (![cartDetails containsObject:obj]) {
            [cartDetails addObject:obj];
        }
    }
    
    NSString *cartdetails = @"[";
    for (int c=0; c<[cartDetails count]; c++) {
        
        cartdetails = [cartdetails stringByAppendingString:@"["];
        NSArray *orderRestaurant = [cartDetails objectAtIndex:c];
        for (int i=0; i<[orderRestaurant count]; i++) {
            
            NSString *menu_name = [[orderRestaurant valueForKey:@"menu_name"] objectAtIndex:i];
            
            NSString *menu_name_final = (NSString *)CFBridgingRelease(CFURLCreateStringByAddingPercentEscapes(NULL, (CFStringRef)menu_name, NULL, (CFStringRef)@"!*'();:@&=+$,/?%#[]", kCFStringEncodingUTF8));
            
            cartdetails = [cartdetails stringByAppendingString:@"{\"menu_id\":"];
            cartdetails = [cartdetails stringByAppendingString:[NSString stringWithFormat:@"\"%@\",",[[orderRestaurant valueForKey:@"menu_id"] objectAtIndex:i] ]];
            cartdetails = [cartdetails stringByAppendingString:@"\"res_id\":"];
            cartdetails = [cartdetails stringByAppendingString:[NSString stringWithFormat:@"\"%@\",",[[orderRestaurant valueForKey:@"res_id"] objectAtIndex:i] ]];
            cartdetails = [cartdetails stringByAppendingString:@"\"menu_name\":"];
            cartdetails = [cartdetails stringByAppendingString:[NSString stringWithFormat:@"\"%@\",", menu_name_final]];
            cartdetails = [cartdetails stringByAppendingString:@"\"quantity\":"];
            cartdetails = [cartdetails stringByAppendingString:[NSString stringWithFormat:@"\"%@\",",[[orderRestaurant valueForKey:@"quantity"] objectAtIndex:i] ]];
            cartdetails = [cartdetails stringByAppendingString:@"\"menu_price\":"];
            cartdetails = [cartdetails stringByAppendingString:[NSString stringWithFormat:@"\"%@\",",[[orderRestaurant valueForKey:@"menu_price"] objectAtIndex:i] ]];
            cartdetails = [cartdetails stringByAppendingString:@"\"total\":"];
            cartdetails = [cartdetails stringByAppendingString:[NSString stringWithFormat:@"\"%@\"},",[[orderRestaurant valueForKey:@"total"] objectAtIndex:i] ]];

        }
        
        cartdetails = [cartdetails substringToIndex:[cartdetails length]-1];
        cartdetails = [cartdetails stringByAppendingString:@"],"];
    }
    
    cartdetails = [cartdetails substringToIndex:[cartdetails length]-1];
    cartdetails = [cartdetails stringByAppendingString:@"]"];
    
    urlString = [urlString stringByAppendingString:@"&cartdetails="];
    urlString = [urlString stringByAppendingString:cartdetails];
    
    //Ship to address
    urlString = [urlString stringByAppendingString:@"&deliverystreet="];
    urlString = [urlString stringByAppendingString:[[address valueForKey:@"customer_street"]objectAtIndex:addressIndex]];
    urlString = [urlString stringByAppendingString:@"&deliverycity="];
    urlString = [urlString stringByAppendingString:[[address valueForKey:@"customer_city"]objectAtIndex:addressIndex]];
    urlString = [urlString stringByAppendingString:@"&deliverystate="];
    urlString = [urlString stringByAppendingString:[[address valueForKey:@"customer_state"]objectAtIndex:addressIndex]];
    urlString = [urlString stringByAppendingString:@"&deliveryzip="];
    urlString = [urlString stringByAppendingString:[[address valueForKey:@"customer_zip"]objectAtIndex:addressIndex]];
    urlString = [urlString stringByAppendingString:@"&deliveryapt="];
    urlString = [urlString stringByAppendingString:[[address valueForKey:@"customer_apartment_name"]objectAtIndex:addressIndex]];

    //Customer
    urlString = [urlString stringByAppendingString:@"&customer_id="];
    urlString = [urlString stringByAppendingString:[Customer getIdCustomer]];
    urlString = [urlString stringByAppendingString:@"&contactname="];
    urlString = [urlString stringByAppendingString:[Customer getFirsttName]];
    urlString = [urlString stringByAppendingString:@"&contactlastname="];
    urlString = [urlString stringByAppendingString:[Customer getLastName]];
    urlString = [urlString stringByAppendingString:@"&contactemail="];
    urlString = [urlString stringByAppendingString:[Customer getEmail]];
    urlString = [urlString stringByAppendingString:@"&contactphone="];
    urlString = [urlString stringByAppendingString:[[address valueForKey:@"customer_address_phone_number"]objectAtIndex:addressIndex]];
    
  
    NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
    [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
    // dateFormatter.timeZone = [NSTimeZone timeZoneForSecondsFromGMT:0];
   // [dateFormatter setTimeZone:[NSTimeZone timeZoneWithName:@"GMT"]];
   
    NSString* dateString = [dateFormatter stringFromDate:[[NSDate alloc] init]];
    
    urlString = [urlString stringByAppendingString:@"&orderdate="];
    urlString = [urlString stringByAppendingString:dateString];
    
    NSURL *url = [NSURL URLWithString:WS_CHECKOUT];
    NSString *length = [NSString stringWithFormat:@"%lu",(unsigned long)[urlString length]];
    NSMutableURLRequest *request =	[NSMutableURLRequest requestWithURL:url];
    [request setHTTPMethod:@"POST"];
    [request addValue:length forHTTPHeaderField:@"Content-Length"];
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPBody:[urlString dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSLog(@"url: %@", urlString);
    
    NSURLResponse *response;
    NSError *error = nil;
    NSData *receivedData = [NSURLConnection sendSynchronousRequest:request
                                                 returningResponse:&response
                                                             error:&error];
    NSString *responseString = [[NSString alloc] initWithData:receivedData encoding:NSUTF8StringEncoding];
    NSDictionary *data = [ParseJson parseJsonData:[responseString dataUsingEncoding:NSUTF8StringEncoding]];
    
    if([[data objectForKey:@"success"]  isEqual: @"1"])
    {
        NSLog(@"Purchase successfully");
        
        if ([data valueForKey:@"order_id"] != nil)
        {
            return [data valueForKey:@"order_id"];
        }
    }
    else {
        NSLog(@"Error processing purchase!");
    }
    
    return @"error";
}


+(NSDictionary *)getOrdersForCustomer:(NSString *)customerId
{
    NSString *urlString = @"";
    urlString = [urlString stringByAppendingString:@"cusid="];
    urlString = [urlString stringByAppendingString:customerId];
    urlString = [urlString stringByAppendingString:@"&action=OrderList"];
    
    NSLog(@"url: %@", urlString);
    
    NSURL *url = [NSURL URLWithString:WS_CUSTOMER_ORDER];
    NSString *length = [NSString stringWithFormat:@"%lu",(unsigned long)[urlString length]];
    NSMutableURLRequest *request =	[NSMutableURLRequest requestWithURL:url];
    [request setHTTPMethod:@"POST"];
    [request addValue:length forHTTPHeaderField:@"Content-Length"];
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPBody:[urlString dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSURLResponse *response;
    NSError *error = nil;
    NSData *receivedData = [NSURLConnection sendSynchronousRequest:request
                                                 returningResponse:&response
                                                             error:&error];
    NSString *responseString = [[NSString alloc] initWithData:receivedData encoding:NSUTF8StringEncoding];
    //NSLog(@"%@",responseString);
    NSDictionary *data = [ParseJson parseJsonData:[responseString dataUsingEncoding:NSUTF8StringEncoding]];
    
    // test the result
    if ([[data valueForKey:@"success"]  isEqual: @"1"]) {
        NSLog(@"Get order list successfully");
      
        
    } else {
        NSLog(@"Error getting order list!");
        
    }
    return data;
}


+(NSDictionary *)getDetailsForOrder:(NSString *)ordergenerateid
{
    NSString *urlString = @"";
    urlString = [urlString stringByAppendingString:@"ordergenerateid="];
    urlString = [urlString stringByAppendingString:ordergenerateid];
    urlString = [urlString stringByAppendingString:@"&action=OrderView"];
    
    NSLog(@"url: %@", urlString);
    
    NSURL *url = [NSURL URLWithString:WS_CUSTOMER_ORDER];
    NSString *length = [NSString stringWithFormat:@"%lu",(unsigned long)[urlString length]];
    NSMutableURLRequest *request =	[NSMutableURLRequest requestWithURL:url];
    [request setHTTPMethod:@"POST"];
    [request addValue:length forHTTPHeaderField:@"Content-Length"];
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPBody:[urlString dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSURLResponse *response;
    NSError *error = nil;
    NSData *receivedData = [NSURLConnection sendSynchronousRequest:request
                                                 returningResponse:&response
                                                             error:&error];
    NSString *responseString = [[NSString alloc] initWithData:receivedData encoding:NSUTF8StringEncoding];
    //NSLog(@"%@",responseString);
    NSDictionary *data = [ParseJson parseJsonData:[responseString dataUsingEncoding:NSUTF8StringEncoding]];
    
    // test the result
    if ([[data valueForKey:@"success"]  isEqual: @"1"]) {
        NSLog(@"Get order list successfully");
        
    } else {
        NSLog(@"Error getting order list!");
        
    }
    return data;
}

+(BOOL)addRatingForMenu:(NSString *)menuId restaurant_id:(NSString *) resid customer_id: (NSString *)cusid ordergenerateid: (NSString *) ordergenerateid rating: (NSString *) rating
{
    NSString *urlString = @"";
    urlString = [urlString stringByAppendingString:@"resid="];
    urlString = [urlString stringByAppendingString:resid];
    urlString = [urlString stringByAppendingString:@"&menu_id="];
    urlString = [urlString stringByAppendingString:menuId];
    urlString = [urlString stringByAppendingString:@"&cusid="];
    urlString = [urlString stringByAppendingString:cusid];
    urlString = [urlString stringByAppendingString:@"&ordergenerateid="];
    urlString = [urlString stringByAppendingString:ordergenerateid];
    urlString = [urlString stringByAppendingString:@"&rating="];
    urlString = [urlString stringByAppendingString:rating];
    urlString = [urlString stringByAppendingString:@"&action=addRating"];
    
    NSLog(@"url: %@", urlString);
    
    NSURL *url = [NSURL URLWithString:WS_REVIEWS];
    NSString *length = [NSString stringWithFormat:@"%lu",(unsigned long)[urlString length]];
    NSMutableURLRequest *request =	[NSMutableURLRequest requestWithURL:url];
    [request setHTTPMethod:@"POST"];
    [request addValue:length forHTTPHeaderField:@"Content-Length"];
    [request setValue:@"application/x-www-form-urlencoded" forHTTPHeaderField:@"Content-Type"];
    [request setHTTPBody:[urlString dataUsingEncoding:NSUTF8StringEncoding]];
    
    NSURLResponse *response;
    NSError *error = nil;
    NSData *receivedData = [NSURLConnection sendSynchronousRequest:request
                                                 returningResponse:&response
                                                             error:&error];
    NSString *responseString = [[NSString alloc] initWithData:receivedData encoding:NSUTF8StringEncoding];
    //NSLog(@"%@",responseString);
    NSDictionary *data = [ParseJson parseJsonData:[responseString dataUsingEncoding:NSUTF8StringEncoding]];
    
    // test the result
    if ([[data valueForKey:@"success"]  isEqual: @"1"]) {
        NSLog(@"Add review successfully");
    } else {
        NSLog(@"Error adding review!");
        
    }
    return [[data valueForKey:@"success"]  isEqual: @"1"];
}

#pragma mark -

+(void)startAnimationInView:(UIViewController *)view
{
    if (!indicador)
    {
        indicador = [[indicatorActivityView alloc]init];
    }
    
    [view.view addSubview:indicador];
}

+(void)stopAnimationInView
{
    if (indicador)
    {
        [indicador removeFromSuperview];
        indicador = nil;
    }
    
}

+(BOOL)isNetworkAvailable{
    
    Reachability *networkReachability = [Reachability reachabilityForInternetConnection];
    NetworkStatus networkStatus = [networkReachability currentReachabilityStatus];
    if (networkStatus == NotReachable) {
        return NO;
    }
    else{
        return YES;
    }
}

@end
