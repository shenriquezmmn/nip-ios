//
//  DeliveryZipDetailsViewController.m
//  nip
//
//  Created by MacCMS2 on 27/04/15.
//  Copyright (c) 2015 MacCMS2. All rights reserved.
//

#import "DeliveryZipDetailsViewController.h"
#import "Requests.h"
#import "Customer.h"
#import "HomeViewController.h"
#import "TutorialViewController.h"
@interface DeliveryZipDetailsViewController ()

@end

@implementation DeliveryZipDetailsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    //Header
    self.navBar = [[NavBarViewController alloc] init];
    self.navigationController.navigationBarHidden = YES;
    
    self.txZipCode.text = self.zipCode;
    if(self.numStreet != nil)
    {
        self.txStreet.text = [NSString stringWithFormat:@"%@ %@",self.numStreet, self.street];
    }
    else
    {
        self.txStreet.text = self.street;
    }
    self.txCity.text = self.city;
    self.txState.text = self.state;
    
    self.visibleMargin = 15;
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [super touchesBegan:touches withEvent:event];
    [self.view endEditing:YES];
    
}
- (IBAction)textBegin:(id)sender {
   
    //SET THE LAST VIEW WHICH THE KEYBOARD WILL NOT HIDE
    self.lastVisibleView = sender;

}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    NSString* totalString = [NSString stringWithFormat:@"%@%@",textField.text,string];
    
    // if it's the phone number textfield format it.
    if(textField == self.txPhoneNumber ) {
        if (range.length == 1) {
            // Delete button was hit.. so tell the method to delete the last char.
            textField.text = [self formatPhoneNumber:totalString deleteLastChar:YES];
        } else {
            textField.text = [self formatPhoneNumber:totalString deleteLastChar:NO ];
        }
        return false;
    }
    
    return YES;
}

-(NSString*) formatPhoneNumber:(NSString*) simpleNumber deleteLastChar:(BOOL)deleteLastChar {
    if(simpleNumber.length==0) return @"";
    // use regex to remove non-digits(including spaces) so we are left with just the numbers
    NSError *error = NULL;
    NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:@"[\\s-\\(\\)]" options:NSRegularExpressionCaseInsensitive error:&error];
    simpleNumber = [regex stringByReplacingMatchesInString:simpleNumber options:0 range:NSMakeRange(0, [simpleNumber length]) withTemplate:@""];
    
    // check if the number is to long
    if(simpleNumber.length>10) {
        // remove last extra chars.
        simpleNumber = [simpleNumber substringToIndex:10];
    }
    
    if(deleteLastChar) {
        // should we delete the last digit?
        simpleNumber = [simpleNumber substringToIndex:[simpleNumber length] - 1];
    }
    
    // 123 456 7890
    // format the number.. if it's less then 7 digits.. then use this regex.
    if(simpleNumber.length<7)
        simpleNumber = [simpleNumber stringByReplacingOccurrencesOfString:@"(\\d{3})(\\d+)"
                                                               withString:@"($1) $2"
                                                                  options:NSRegularExpressionSearch
                                                                    range:NSMakeRange(0, [simpleNumber length])];
    
    else   // else do this one..
        simpleNumber = [simpleNumber stringByReplacingOccurrencesOfString:@"(\\d{3})(\\d{3})(\\d+)"
                                                               withString:@"($1) $2-$3"
                                                                  options:NSRegularExpressionSearch
                                                                    range:NSMakeRange(0, [simpleNumber length])];
    return simpleNumber;
}

-(void)startAnimation
{
    [Requests startAnimationInView:self];
}

- (IBAction)goToHome:(id)sender {
    
    if([self.txZipCode.text isEqualToString:@""] || [self.txStreet.text isEqualToString:@""] || [self.txUnit.text isEqualToString:@""] || [self.txCity.text isEqualToString:@""] || [self.txState.text isEqualToString:@""] || [self.txAddresLabel.text isEqualToString:@""]  || [self.txPhoneNumber.text isEqualToString:@""])
    {
    
        UIAlertView * alert = [[UIAlertView alloc] initWithTitle:@"Whoops!"
                                                         message:@"Seems like some fields are missing"
                                                        delegate:nil
                                               cancelButtonTitle:@"Ok"
                                               otherButtonTitles:nil];
        [alert show];

    }
    else if ([self.txZipCode.text length]< LENGTH_ZIPCODE)
    {
        UIAlertView * alert = [[UIAlertView alloc] initWithTitle:@"Could you check again?"
                                                         message:[NSString stringWithFormat:@"Your Zip Code should be %d digits long", LENGTH_ZIPCODE]
                                                        delegate:nil
                                               cancelButtonTitle:@"Ok"
                                               otherButtonTitles:nil];
        [alert show];
        
    }
    else if ([self.txPhoneNumber.text length]< LENGTH_PHONE)
    {
        UIAlertView * alert = [[UIAlertView alloc] initWithTitle:@"Could you check again?"
                                                         message:@"Your phone number should be 10 digits long"
                                                        delegate:nil
                                               cancelButtonTitle:@"Ok"
                                               otherButtonTitles:nil];
        [alert show];
        
    }
    else
    {
        
        [self performSelectorInBackground:@selector(startAnimation) withObject:self];
             
        NSString *success = [Requests addAddressForCustomer:[Customer getIdCustomer]deliveryZip:self.txZipCode.text deliveryStreet:self.txStreet.text deliveryUnit:self.txUnit.text deliveryCity:self.txCity.text deliveryState:self.txState.text placeAlias:self.txAddresLabel.text phoneNumber:self.txPhoneNumber.text];
            
            if([success isEqual:@"1"])
            {
                //List address
               [Requests getAddressForCustomer:[Customer getIdCustomer]];
                
                NSArray *listAddress = [Customer getAddresses];
                NSUserDefaults *datos = [NSUserDefaults standardUserDefaults];
                [datos setObject:[[listAddress valueForKey:@"id"] objectAtIndex:0 ]forKey:@"addressId"];
                [datos synchronize];
               
                //List cards
                [Requests getCardsForCustomer:[Customer getIdCustomer]];
                if (![[NSUserDefaults standardUserDefaults] boolForKey:@"HasLaunchedOnce"])
                {
                    [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"HasLaunchedOnce"];
                    [[NSUserDefaults standardUserDefaults] synchronize];
                    
                    TutorialViewController *tutorial = [[TutorialViewController alloc] init];
                    [self.navBar pushTransition:self];
                    [self.navigationController pushViewController:tutorial animated:NO];

                }
                else
                {
                    HomeViewController *home = [[HomeViewController alloc] init];
                    [self.navBar pushTransition:self];
                    [self.navigationController pushViewController:home animated:NO];                    
                }
            }
            else
            {
                [Requests stopAnimationInView];
            }
    }    
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
