//
//  Customer.m
//  nip
//
//  Created by MacCMS2 on 24/04/15.
//  Copyright (c) 2015 MacCMS2. All rights reserved.
//

#import "Customer.h"

@implementation Customer

NSMutableDictionary *dataUser;
NSMutableArray *addresses;
NSMutableArray *listCards;
bool isGetListCard = NO;
bool isGetAddresses = NO;

+(void)setCustomer : (NSMutableDictionary *) dataCustomer
{
    if(dataUser)
    {
        [dataUser addEntriesFromDictionary:dataCustomer];
        
    }
    else
    {
        dataUser = [[NSMutableDictionary alloc] initWithDictionary:dataCustomer];
    }

}

+(NSDictionary *)getCustomer
{
    if([dataUser count] > 0)
    {
        return dataUser;
    }
    else
    {
        return nil;
    }
}

+(NSString *)getIdCustomer
{
    return [dataUser valueForKey:@"customerid"];
}

+(NSString *)getNameCustomer
{
    return [NSString stringWithFormat:@"%@ %@", [dataUser valueForKey:@"firtsname"], [dataUser valueForKey:@"lastname"]];
}

+(NSString *)getFirsttName{
    return [dataUser valueForKey:@"firtsname"];
}

+(NSString *)getLastName{
    return [dataUser valueForKey:@"lastname"];
}

+(NSString *)getIdPhotoCustomer
{
    return [dataUser valueForKey:@"idphoto"];
}

+(NSString *)getEmail
{
    return [dataUser valueForKey:@"email"];
}


+(void)setAddresses: (NSMutableArray *) dataAddresss
{
    addresses = dataAddresss;
    
}

+(NSMutableArray *)getAddresses
{
    return addresses;
}

+(void)setListCards: (NSMutableArray *) cards
{
    listCards = cards;
    isGetListCard = YES;
}

+(NSMutableArray *)getListCards
{
    if(listCards)
    {
        return listCards;
    }
    
    return nil;

}
+(bool)isGetListCards
{
    return isGetListCard;
}

+(void)setIsGetAddresses: (bool) getAddresses
{
    isGetAddresses = getAddresses;
}

+(bool)IsGetAddresses
{
    return isGetAddresses;
}

+(void)removeAll
{
    [dataUser removeAllObjects];
}
@end
