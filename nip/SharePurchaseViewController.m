//
//  SharePurchaseViewController.m
//  NIP
//
//  Created by MacCMS2 on 28/5/15.
//  Copyright (c) 2015 MacCMS2. All rights reserved.
//

#import "SharePurchaseViewController.h"
#import "HCSStarRatingView.h"
#import "SDWebImage/UIImageView+WebCache.h"
#import "AppDelegate.h"


@interface SharePurchaseViewController ()

@end

@implementation SharePurchaseViewController
@synthesize lbDish,lbRestaurant,lbRate;

- (void)viewDidLoad {

    // Do any additional setup after loading the view from its nib.

    int width =[[ UIScreen mainScreen ] bounds ].size.width;
    int height =[[ UIScreen mainScreen ] bounds ].size.height;
    
    [self.viewPopUp setFrame:CGRectMake((width/2)-(width-40)/2, (height/2)-(width-40)/2, width-40, 257)];
    self.view.backgroundColor = [UIColor clearColor];
    
    self.viewPopUp.backgroundColor = [UIColor whiteColor];
    self.viewPopUp.layer.cornerRadius = 8;
    self.viewPopUp.layer.shadowOpacity = 0.15;
    self.viewPopUp.layer.shadowOffset = CGSizeMake(5.0f, 5.0f);

    lbRestaurant.textColor = TITLE_COLOR;
    lbDish.textColor = TITLE_COLOR;
    lbRate.textColor = TITLE_COLOR;
    
    [self.view addSubview:self.viewPopUp];
    
    //Rounded Border image menu
    self.imgDish.layer.masksToBounds = YES;
    self.imgDish.layer.cornerRadius = 40.0;
    self.imgDish.layer.rasterizationScale = [UIScreen mainScreen].scale;
    self.imgDish.layer.shouldRasterize = YES;
    self.imgDish.clipsToBounds = YES;
    
    //Rating
    self.start.maximumValue = 5;
    self.start.minimumValue = 0;
    self.start.value = 0;
    self.start.spacing = 8.f;
    self.start.allowsHalfStars = NO;
    self.start.tintColor = GREEN_COLOR;
    self.start.offStars = GRAY_LIGHT_COLOR;
   
    [super viewDidLoad];
}

- (IBAction)didChangeValue:(HCSStarRatingView *)sender {

    NSLog(@"Changed rating to %.1f", sender.value);
}

-(void)showInView:(UIViewController*)view
{
    [view.view addSubview:self.view];
    self.view.transform = CGAffineTransformMakeScale(1.3, 1.3);
    self.view.alpha = 0;
    [UIView animateWithDuration:.25 animations:^{
        self.view.alpha = 1;
        self.view.transform = CGAffineTransformMakeScale(1, 1);
    }];
}

-(void)removerFromView;
{
   [self.view removeFromSuperview];
}
- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
