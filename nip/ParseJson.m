//
//  ParseJson.m
//  nip
//
//  Created by MacCMS2 on 24/04/15.
//  Copyright (c) 2015 MacCMS2. All rights reserved.
//

#import "ParseJson.h"

@implementation ParseJson

+(id)parseJsonData:(NSData *)data{
    
    NSError *error;
    
    NSJSONSerialization *json=[NSJSONSerialization JSONObjectWithData:data options:NSJSONReadingAllowFragments error:&error];
    
    if(!error)
        return json;
    
    return nil;
    
}

@end
