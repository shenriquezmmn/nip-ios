//
//  Dishes.m
//  nip
//
//  Created by MacCMS2 on 27/04/15.
//  Copyright (c) 2015 MacCMS2. All rights reserved.
//

#import "Dishes.h"

@implementation Dishes

NSDictionary *data;

+(void)setMenu : (NSDictionary *) dataMenu
{
    data = dataMenu;
}

+(NSDictionary *)getMenu
{
    return data;
}


@end
