//
//  LoginViewController.m
//  nip
//
//  Created by MacCMS2 on 24/04/15.
//  Copyright (c) 2015 MacCMS2. All rights reserved.
//

#import "LoginViewController.h"
#import "Requests.h"
#import "Customer.h"
#import "DeliveryZipViewController.h"
#import "HomeViewController.h"
#import "TutorialViewController.h"


@interface LoginViewController ()

@end

int cont = 0;

@implementation LoginViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    
    cont =0;
    
    //Permission Login FB
    self.btLogin.readPermissions = @[@"public_profile", @"email"];
    // Do any additional setup after loading the view from its nib.
    
    //Header
    self.navBar = [[NavBarViewController alloc] init];
    self.navigationController.navigationBarHidden = YES;
   
}

#pragma mark LoginFB
- (void)loginView:(FBLoginView *)loginView handleError:(NSError *)error
{
    if (error != nil) {
        NSString *alertMessage;
        NSString *alertTitle;
        
        // Facebook SDK * error handling *
        // Error handling is an important part of providing a good user experience.
        // Since this sample uses the FBLoginView, this delegate will respond to
        // login failures, or other failures that have closed the session (such
        // as a token becoming invalid). Please see the [- postOpenGraphAction:]
        // and [- requestPermissionAndPost] on `SCViewController` for further
        // error handling on other operations.
        if ([FBErrorUtility shouldNotifyUserForError:error]) {
            // If the SDK has a message for the user, surface it. This conveniently
            // handles cases like password change or iOS6 app slider state.
            alertTitle = @"Something Went Wrong";
            alertMessage = [FBErrorUtility userMessageForError:error];
        } else {
            switch ([FBErrorUtility errorCategoryForError:error]) {
                case FBErrorCategoryAuthenticationReopenSession:{
                    // It is important to handle session closures as mentioned. You can inspect
                    // the error for more context but this sample generically notifies the user.
                    alertTitle = @"Session Error";
                    alertMessage = @"Your current session is no longer valid. Please log in again.";
                    break;
                }
                case FBErrorCategoryUserCancelled:{
                    // The user has cancelled a login. You can inspect the error
                    // for more context. For this sample, we will simply ignore it.
                    NSLog(@"user cancelled login");
                    break;
                }
                default:{
                    // For simplicity, this sample treats other errors blindly, but you should
                    // refer to https://developers.facebook.com/docs/technical-guides/iossdk/errors/
                    // for more information.
                    alertTitle = @"Unknown Error";
                    alertMessage = @"Error.  Please try again later.";
                    NSLog(@"Unexpected error:%@", error);
                    break;
                }
            }
        }
        
        if (alertMessage) {
            [[[UIAlertView alloc] initWithTitle:alertTitle
                                        message:alertMessage
                                       delegate:nil
                              cancelButtonTitle:@"OK"
                              otherButtonTitles:nil] show];
        }
    }
    
}

- (void)loginViewFetchedUserInfo:(FBLoginView *)loginView user:(id<FBGraphUser>)user
{
    if (cont == 1) {
        cont++;
        NSUserDefaults *datos = [NSUserDefaults standardUserDefaults];
        
        [datos setObject:user.objectID forKey:@"id"];
        [datos setObject:[user first_name] forKey:@"first_name"];
        [datos setObject:[user last_name] forKey:@"last_name"];
        [datos setObject:[user objectForKey:@"email"] forKey:@"email"];
        [datos synchronize];
        
        
        [Requests loginForUser:[user objectForKey:@"email"] firtsName:[user first_name] lastName:[user last_name] photo:user.objectID];
        
        NSString *res = [Requests getAddressForCustomer:[Customer getIdCustomer]];
        
        if ([res isEqual:@"1"]) {
         
            if (![[NSUserDefaults standardUserDefaults] boolForKey:@"HasLaunchedOnce"])
            {
                [[NSUserDefaults standardUserDefaults] setBool:YES forKey:@"HasLaunchedOnce"];
                [[NSUserDefaults standardUserDefaults] synchronize];
                TutorialViewController *tutorial = [[TutorialViewController alloc] init];
                [self.navBar pushTransition:self];
                [self.navigationController pushViewController:tutorial animated:NO];
            }
            else
            {
                HomeViewController *home = [[HomeViewController alloc] init];
                home.haveAlreadyReceivedCoordinates = NO;
                home.logoTransparente.hidden = NO;
                [self.navBar pushTransition:self];
                
                [self.navigationController pushViewController:home animated:NO];
            }           
        }
        else if ([res isEqual:@"0"]) {
            
            DeliveryZipViewController *deliveryZip = [[DeliveryZipViewController alloc] init];
            [self.navBar pushTransition:self];
            
            [self.navigationController pushViewController:deliveryZip animated:NO];
        }

    }
    
}

-(void)startAnimation
{
    [Requests startAnimationInView:self];
}

- (void)loginViewShowingLoggedInUser:(FBLoginView *)loginView
{
    if(cont == 0)
    {
        cont++;
        NSLog(@"You are logged in");
        [self performSelectorInBackground:@selector(startAnimation) withObject:self];
    }
}

- (void)loginViewShowingLoggedOutUser:(FBLoginView *)loginView
{
    NSLog(@"You are logged out");
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
