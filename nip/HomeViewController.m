//
//  HomeViewController.m
//  nip
//
//  Created by MacCMS2 on 27/04/15.
//  Copyright (c) 2015 MacCMS2. All rights reserved.
//

#import "HomeViewController.h"
#import "HCSStarRatingView.h"
#import "Customer.h"
#import "Dishes.h"
#import "AMPopTip.h"
#import "CartViewController.h"
#import "PopUpViewController.h"
#import "Cart.h"
#import "AddressesViewController.h"
#import "SDWebImage/UIImageView+WebCache.h"
#import "CNPGridMenu.h"
#import "PaymentViewController.h"
#import "LoginViewController.h"
#import "AboutNipViewController.h"
#import <CoreLocation/CoreLocation.h>
#import <CoreLocation/CLLocationManagerDelegate.h>
#import "Requests.h"
#import "SharePurchaseViewController.h"

#define IS_OS_8_OR_LATER ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0)

@interface HomeViewController () <CNPGridMenuDelegate>
{
    CLLocationManager *locationManager;
}

@property (nonatomic, strong) NSDictionary *dishes;
@property (nonatomic, assign) BOOL wrap;
@property (nonatomic, strong) AMPopTip *popTip;
@property (nonatomic, strong) NSArray *addToCart;
@property (nonatomic, strong) CNPGridMenu *gridMenu;

@end

int height;
int size;
int addressIndex;
UIButton *bt=nil;
BOOL up;
NSArray *address;
AddressesViewController *addressVC;
UIAlertView * alert;
UIAlertView * alertLocation;
UIAlertView * alertErrorDishes;
UIAlertView * alertErrorAddress;
UIImageView *tutorial;

NSDictionary *details;
NSArray *menu;
NSArray *address;
int count;
int value=0;

@implementation HomeViewController

@synthesize carousel, wrap,addToCart, cancelOrder, share, haveAlreadyReceivedCoordinates;
//static BOOL haveAlreadyReceivedCoordinates = NO;

- (void)setUp
{
     //set up data
    self.wrap = YES;
    self.dishes = [Dishes getMenu];
}

- (id)initWithNibName:(NSString *)nibNameOrNil bundle:(NSBundle *)nibBundleOrNil
{
    if ((self = [super initWithNibName:nibNameOrNil bundle:nibBundleOrNil]))
    {
        [self setUp];
    }
    return self;
}

- (id)initWithCoder:(NSCoder *)aDecoder
{
    if ((self = [super initWithCoder:aDecoder]))
    {
        [self setUp];
    }
    return self;
}

- (void)dealloc
{
    //it's a good idea to set these to nil here to avoid
    //sending messages to a deallocated viewcontroller
    carousel.delegate = nil;
    carousel.dataSource = nil;
    
}

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.

    addressVC = [[AddressesViewController alloc] init];
    
    //Instantiate a location object.
    locationManager = [[CLLocationManager alloc] init];
    locationManager.delegate = self;
    locationManager.distanceFilter = kCLDistanceFilterNone;
    locationManager.desiredAccuracy = kCLLocationAccuracyBest;
    
    //Header
    self.navBar = [[NavBarViewController alloc] init];
    [self.navBar showInView:self withTitle:@"" hiddenButtom:YES];
    self.navBar.imgAddress.hidden = YES;
    self.navigationController.navigationBarHidden = YES;
    
    address = [Customer getAddresses];
    
    //If exist addressId on memory
    NSUserDefaults *datos = [NSUserDefaults standardUserDefaults];
    NSString *addressId = [datos stringForKey:@"addressId"];
    
    addressIndex = 0;
    
    if(addressId != nil && ![addressId isEqualToString:@""])
    {
        for (int i=0; i< [address count]; i++) {
            if([[[address valueForKey:@"id"] objectAtIndex:i] isEqualToString:addressId])
            {
                addressIndex = i;
                break;
            }
        }
    }
    else
    {
        [datos setObject:[[address valueForKey:@"id"] objectAtIndex:0] forKey:@"addressId"];
    }
       
    [self showIcons];
    
    //configure gridMenu
    self.showGridButton.layer.cornerRadius = 4;
    
    //configure carousel
    self.carousel.type = iCarouselTypeLinear;
        
    height=[[ UIScreen mainScreen ] bounds ].size.height;
    
    //Size image properties
    size = 27;
    
    //Mini popUp
    [[AMPopTip appearance] setFont:[UIFont fontWithName:@"Avenir-Medium" size:12]];
    
    self.popTip = [AMPopTip popTip];
    self.popTip.shouldDismissOnTap = YES;
    self.popTip.edgeMargin = 5;
    self.popTip.offset = 2;
    self.popTip.edgeInsets = UIEdgeInsetsMake(0, 10, 0, 10);
    self.popTip.tapHandler = ^{
        NSLog(@"Tap!");
    };
    self.popTip.dismissHandler = ^{
        NSLog(@"Dismiss!");
    };
    
    addToCart = [[self.dishes valueForKey:@"menu_list"] objectAtIndex:0];
    
    self.lbText.textColor = LABEL_COLOR;
    self.lbTitle.textColor = LABEL_COLOR;
    
    if(cancelOrder)
    {
        self.navBar.view.backgroundColor = [GREEN_COLOR colorWithAlphaComponent:.5];
        self.viewPopUp.backgroundColor=[[UIColor whiteColor] colorWithAlphaComponent:.8];
        self.popUp.layer.cornerRadius = 8;
        self.popUp.layer.shadowOpacity = 0.15;
        self.popUp.layer.shadowOffset = CGSizeMake(5.0f, 5.0f);
        self.viewPopUp.hidden = NO;

    }
    
    if(haveAlreadyReceivedCoordinates)
    {
        [self buildAddressForIndex:addressIndex];
        //[self performSelectorInBackground:@selector(requestOrders:) withObject:[[NSDate alloc]init]];
    }
}

- (void)viewDidUnload
{
    [super viewDidUnload];
    self.carousel = nil;
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

- (BOOL)shouldAutorotateToInterfaceOrientation:(__unused UIInterfaceOrientation)interfaceOrientation
{
    return YES;
}

-(void)showIcons
{
    //If exist cart on memory
    NSUserDefaults *datos = [NSUserDefaults standardUserDefaults];
    
    NSDate *placeOrderDate = nil;
    
    if([datos objectForKey:@"placeOrderDate"])
    {
        placeOrderDate = [datos objectForKey:@"placeOrderDate"];
    }
    
    if([[Cart getCart] count] > 0)
    {
        self.navBar.imgCart.hidden = NO;
        self.navBar.btCart.enabled = YES;
        self.navBar.imgCart.image = [UIImage imageNamed:@"cart"];
        UITapGestureRecognizer *cart = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(goToCart)];
        [self.navBar.btCart addGestureRecognizer:cart];
        
        if(placeOrderDate !=nil)
        {
            NSTimeInterval distanceBetweenDates = [[[NSDate alloc] init] timeIntervalSinceDate:placeOrderDate];
            int time = distanceBetweenDates * 1;
            
            if(time < 1500)
            {
                self.navBar.imgOrder.hidden = NO;
                UITapGestureRecognizer *alert = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(alertProcessingOrder)];
                [self.navBar.btOrder addGestureRecognizer:alert];
            }
            if(time > 7200 && haveAlreadyReceivedCoordinates && !cancelOrder)
            {
                @try {
                    [self requestOrders:[[NSDate alloc]init]];
                    [datos removeObjectForKey:@"placeOrderDate"];
                }
                @catch (NSException *exception) {
                }
                
            }
        }
    }
    else
    {
        self.navBar.imgCart.hidden = YES;
        self.navBar.btCart.enabled = NO;
      
        if(placeOrderDate !=nil)
        {
            NSTimeInterval distanceBetweenDates = [[[NSDate alloc] init] timeIntervalSinceDate:placeOrderDate];
            int time = distanceBetweenDates * 1;
            
            if(time < 1500)
            {
                self.navBar.imgOrder.hidden = YES;
                self.navBar.imgCart.hidden = NO;
                self.navBar.btCart.enabled = YES;
                
                self.navBar.imgCart.image = [UIImage imageNamed:@"Check"];
                UITapGestureRecognizer *alertProcessingOrder = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(alertProcessingOrder)];
                [self.navBar.btCart addGestureRecognizer:alertProcessingOrder];
            }
            if(time > 7200 && haveAlreadyReceivedCoordinates && !cancelOrder )
            {
                @try {
                    [self requestOrders:[[NSDate alloc]init]];
                    [datos removeObjectForKey:@"placeOrderDate"];
                }
                @catch (NSException *exception) {
                }
                
            }
        }
    }


}

-(void)buildAddressForIndex: (int) addressIndex
{
    self.logoTransparente.hidden = YES;
    self.navBar.imgAddress.hidden = NO;
    self.navBar.lbAddress.text = [NSString stringWithFormat:@"%@ - %@", [[address valueForKey:@"customer_address_title"] objectAtIndex:addressIndex], [[address valueForKey:@"customer_zip"] objectAtIndex:addressIndex]];
    
    if([[[address valueForKey:@"customer_address_title"] objectAtIndex:addressIndex] caseInsensitiveCompare:@"home"] == NSOrderedSame  || [[[address valueForKey:@"customer_address_title"] objectAtIndex:addressIndex] caseInsensitiveCompare:@"house"]== NSOrderedSame )
    {
        self.navBar.imgAddress.image = [UIImage imageNamed:@"Home.png"];
    }
    else if ([[[address valueForKey:@"customer_address_title"] objectAtIndex:addressIndex] caseInsensitiveCompare:@"office"] == NSOrderedSame || [[[address valueForKey:@"customer_address_title"] objectAtIndex:addressIndex] caseInsensitiveCompare:@"work"] == NSOrderedSame )
    {
        self.navBar.imgAddress.image = [UIImage imageNamed:@"OfficeWhite.png"];
    }
    else
    {
        self.navBar.imgAddress.image = [UIImage imageNamed:@"OtherWhite.png"];
    }
    
}

- (void)alertView:(UIAlertView *)alertView
clickedButtonAtIndex:(NSInteger)buttonIndex{
    
    if(alertView == alert )
    {
        [self goToAddresses];
    }
    else if(alertView == alertLocation)
    {
        if(buttonIndex == alertView.firstOtherButtonIndex)
        {
            [self findLocation];
        }
        else
        {
            [self findDishesWithOutLocation];
        }
    }
    else if (alertErrorDishes)
    {
        if(buttonIndex == alertView.firstOtherButtonIndex)
        {
            if([CLLocationManager authorizationStatus] == kCLAuthorizationStatusAuthorizedWhenInUse)
            {
                haveAlreadyReceivedCoordinates = NO;
                [self findLocation];
            }
            else
            {
                [self findDishesWithOutLocation];
            }
            
        }
        else
        {
            exit(0);
        }
    }
    else if (alertErrorAddress)
    {
        if(buttonIndex == alertView.firstOtherButtonIndex)
        {
            [self goToAddresses];
        }
        else
        {
            exit(0);
        }
    }

    
}

-(void)alertProcessingOrder
{
    UIAlertView *alert = [[UIAlertView alloc] initWithTitle:@"Thanks for nipping!"
                                       message:@"You order will be delivered soon. Please call us at XXX for any questions!"
                                      delegate:self
                             cancelButtonTitle:@"Ok"
                             otherButtonTitles:nil];
    [alert show];

}

#pragma mark - Location

- (void)findLocation {
    
#ifdef __IPHONE_8_0
    if(IS_OS_8_OR_LATER) {
        // Use one or the other, not both. Depending on what you put in info.plist
        [locationManager requestWhenInUseAuthorization];
        //  [locationManager requestAlwaysAuthorization];
    }
#endif
     [locationManager startUpdatingLocation];
    
}

-(void)locationManager:(CLLocationManager *)manager didChangeAuthorizationStatus:(CLAuthorizationStatus)status {
  
    if ((!haveAlreadyReceivedCoordinates && status == kCLAuthorizationStatusNotDetermined)) {
        alertLocation = [[UIAlertView alloc] initWithTitle:@"Hi there!"
                                                   message:@"May we access your location to deliver awesomeness?"
                                                  delegate:self
                                         cancelButtonTitle:@"No"
                                         otherButtonTitles:@"Yes", nil];
        [alertLocation show];
        
    }
    else if (!haveAlreadyReceivedCoordinates && status == kCLAuthorizationStatusAuthorizedWhenInUse)
    {
        [self findLocation];
    }
    else if (!haveAlreadyReceivedCoordinates && status == kCLAuthorizationStatusDenied)
    {
        [self findDishesWithOutLocation];
    }
}

-(void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations
{
    if(haveAlreadyReceivedCoordinates) {
        return;
    }
    
    [self performSelectorInBackground:@selector(startAnimation) withObject:self];
    
    [locationManager stopUpdatingLocation];
    haveAlreadyReceivedCoordinates = YES;

    [self reverseGeocode:locationManager.location];
}

- (void)reverseGeocode:(CLLocation *)location {
    
    haveAlreadyReceivedCoordinates = YES;
    CLGeocoder *geocoder = [[CLGeocoder alloc] init];
    [geocoder reverseGeocodeLocation:location completionHandler:^(NSArray *placemarks, NSError *error) {
        NSLog(@"Finding address");
        [Requests stopAnimationInView];
        if (error) {
            NSLog(@"Error %@", error.description);
        } else {
            CLPlacemark *placemark = [placemarks lastObject];
            NSString *zipcode = placemark.postalCode;
            
            if(zipcode != nil)
            {
                bool exist = NO;
                
                for (int i=0; i< [address count]; i++)
                {
                    if([[[address valueForKey:@"customer_zip"] objectAtIndex:i] isEqualToString:zipcode])
                    {
                        [self buildAddressForIndex:i];
                        NSUserDefaults *datos = [NSUserDefaults standardUserDefaults];
                        if(![[datos objectForKey:@"addressId"] isEqualToString:[[address valueForKey:@"id"] objectAtIndex:addressIndex]])
                        {
                            [Cart removeAll];
                        }
                        [datos setObject:[[address valueForKey:@"id"] objectAtIndex:i]forKey:@"addressId"];
                        addressIndex = i;
                        [datos synchronize];
                        exist = YES;
                        break;
                    }
                }
                
                NSString *response = [Requests menuForZipCode:zipcode];
                self.dishes = [Dishes getMenu];
                [self.carousel reloadData];
                
                if([response isEqualToString:@"-1"] && exist)
                {
                    alert = [[UIAlertView alloc] initWithTitle:@"We’re sorry!"
                                                       message:@"Our awesome service is not available in this area. Please select another address"
                                                      delegate:self
                                             cancelButtonTitle:@"Ok"
                                             otherButtonTitles:nil];
                    [alert show];
                }
                else if ([response isEqualToString:@"-2"] && exist)
                {
                    [Requests stopAnimationInView];
                    alertErrorDishes= [[UIAlertView alloc] initWithTitle:@"We missed you!"
                                                           message:@"Check yor internet conection and try again"
                                                          delegate:self
                                                 cancelButtonTitle:@"Cancel"
                                                 otherButtonTitles:@"Retry",nil];
                    [alertErrorDishes show];
                }
               
                if(!exist)
                {
                    
                    alert = [[UIAlertView alloc] initWithTitle:@"Aw snap!"
                    message:@"We need your address to deliver awesomeness"
                    delegate:self
                    cancelButtonTitle:@"Ok"
                    otherButtonTitles:nil];
                    
                    addressVC.zipCode = placemark.postalCode;
                    addressVC.street = placemark.thoroughfare;
                    addressVC.numStreet = placemark.subThoroughfare;
                    addressVC.city = placemark.locality;
                    addressVC.state = placemark.administrativeArea;
                    
                    [alert show];
                }
            }
            else
            {
                [self findDishesWithOutLocation];
            }
        }
    }];
    
}


-(void)findDishesWithOutLocation
{
    haveAlreadyReceivedCoordinates = YES;
    [self performSelectorInBackground:@selector(startAnimation) withObject:self];
    [self buildAddressForIndex:addressIndex];
    NSString* response = [Requests menuForZipCode:[[address valueForKey:@"customer_zip"]objectAtIndex:addressIndex]];
    self.dishes = [Dishes getMenu];
    [self.carousel reloadData];
    [Requests stopAnimationInView];
    
    if([response isEqualToString:@"-1"])
    {
        alert = [[UIAlertView alloc] initWithTitle:@"We’re sorry!"
                                           message:@"Our awesome service is not available in this area. Please select another address"
                                          delegate:self
                                 cancelButtonTitle:@"Ok"
                                 otherButtonTitles:nil];
        [alert show];
    }
    else if ([response isEqualToString:@"-2"])
    {
        [Requests stopAnimationInView];
        alertErrorDishes= [[UIAlertView alloc] initWithTitle:@"We missed you!"
                                                         message:@"Check yor internet conection and try again"
                                                        delegate:self
                                               cancelButtonTitle:@"Cancel"
                                               otherButtonTitles:@"Retry",nil];
        [alertErrorDishes show];
    }

}

#pragma mark - iCarousel methods

- (NSInteger)numberOfItemsInCarousel:(__unused iCarousel *)carousel
{
    NSLog(@"%li",(long)[[self.dishes valueForKey:@"menu_list"] count]);
    
    return (NSInteger)[[self.dishes valueForKey:@"menu_list"] count];
}
- (UIView *)carousel:(__unused iCarousel *)carousel viewForItemAtIndex:(NSInteger)index reusingView:(UIView *)view
{
    UILabel *labelName = nil;
    UILabel *labelDescription = nil;
    UILabel *labelNameRestaurant = nil;
    UILabel *labelPrice = nil;
    UIImageView *image = nil;
    UIImageView *gluten = nil;
    UIImageView *organic = nil;
    UIImageView *spicy = nil;
    UIImageView *healthy = nil;
    UIImageView *new = nil;
    UIImageView *gourmet = nil;
    UIImageView *vegetarian = nil;
    UIImageView *bestseller = nil;
    UIImageView *peanut = nil;
    UIView *descriptionMenu = nil;
    UIButton *button;
    HCSStarRatingView *starRatingView = nil;
    
    
    //Hide the miniPopUp if it is visible
    if ([self.popTip isVisible]) {
       [self.popTip hide];
    }
    up = NO;
    
    int cont = 15;
    
    //create new view if no view is available for recycling
    if (view == nil)
    {
        view = [[UIView alloc] initWithFrame:CGRectMake(self.carousel.frame.origin.x, self.carousel.frame.origin.y, self.carousel.frame.size.width, self.carousel.frame.size.height)];
        
        //Image
        
        image = [[UIImageView alloc] initWithFrame:CGRectMake(0, 0, view.frame.size.width, view.frame.size.height)];
        
        image.tag = 2;
        image.userInteractionEnabled = YES;
        
        //Show and hide description when view description is swiped
        UISwipeGestureRecognizer *up = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(showDescription:)];
        [up setDirection:UISwipeGestureRecognizerDirectionUp];
        
        UISwipeGestureRecognizer *down = [[UISwipeGestureRecognizer alloc] initWithTarget:self action:@selector(hideDescription:)];
        [down setDirection:UISwipeGestureRecognizerDirectionDown];
        
        
        //View description
        descriptionMenu = [[UIView alloc] init];
        descriptionMenu.backgroundColor = [[UIColor blackColor] colorWithAlphaComponent:.5];
        descriptionMenu.tag = 16;
        
        labelName = [[UILabel alloc] initWithFrame:CGRectMake(15, (image.frame.size.height*10/100)/2-(25/2), view.frame.size.width-50, 45)];
        labelName.textColor = [UIColor whiteColor];
        labelName.textAlignment = NSTextAlignmentJustified;
        labelName.font = [UIFont fontWithName:@"Century Gothic" size:18];
        labelName.tag = 1;
        labelName.lineBreakMode = NSLineBreakByWordWrapping;
        labelName.numberOfLines = 2;
        
        labelDescription = [[UILabel alloc] initWithFrame:CGRectMake(15, (image.frame.size.height*15/100), view.frame.size.width-30, image.frame.size.height-labelName.frame.size.height)];
        labelDescription.textColor = [UIColor whiteColor];
        labelDescription.textAlignment = NSTextAlignmentJustified;
        labelDescription.font =  FONT_NORMAL_TEXT;
        labelDescription.tag = 15;
        labelDescription.lineBreakMode = NSLineBreakByWordWrapping;
        labelDescription.numberOfLines = 0;
        
        bt = [[UIButton alloc] initWithFrame:CGRectMake(view.frame.size.width-35, (image.frame.size.height*15/100)/2-(8/2), 15, 8)];
        
        button = [[UIButton alloc] initWithFrame:CGRectMake(0,descriptionMenu.frame.origin.y, 414, image.frame.size.height)];
        button.tag = 18;
        
        //Show and hide description when touch button
        UITapGestureRecognizer *upBt = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(showAndHideDescription:)];
        UITapGestureRecognizer *upBt2 = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(showAndHideDescription:)];
      
        [button addGestureRecognizer: upBt];
        bt.tag = 17;
        [bt addGestureRecognizer:upBt2];
        
        //Add elements for descriptionMenu
        [descriptionMenu addSubview:labelName];
        [descriptionMenu addSubview:labelDescription];
        [descriptionMenu addSubview:bt];
        [descriptionMenu addSubview:button];
        [descriptionMenu addGestureRecognizer:up];
        [descriptionMenu addGestureRecognizer:down];
        [image addSubview:descriptionMenu];
        
        
        //Detalis
        
        if([[UIScreen mainScreen] bounds].size.height == 480)
        {
            labelNameRestaurant = [[UILabel alloc] initWithFrame:CGRectMake(15, labelName.frame.origin.x+labelName.frame.size.height-15, 200, 20)];
            
            labelPrice = [[UILabel alloc] initWithFrame:CGRectMake(view.frame.size.width-15-100, labelName.frame.origin.x+labelName.frame.size.height-15, 100, 20)];
        }
        else if([[UIScreen mainScreen] bounds].size.height == 568)
        {
            labelNameRestaurant = [[UILabel alloc] initWithFrame:CGRectMake(15, labelName.frame.origin.x+labelName.frame.size.height, 200, 20)];
        
            labelPrice = [[UILabel alloc] initWithFrame:CGRectMake(view.frame.size.width-15-100, labelName.frame.origin.x+labelName.frame.size.height, 100, 20)];
        }
        else
        {
            labelNameRestaurant = [[UILabel alloc] initWithFrame:CGRectMake(15, labelName.frame.origin.x+labelName.frame.size.height, 200, 20)];
            
            labelPrice = [[UILabel alloc] initWithFrame:CGRectMake(view.frame.size.width-15-100, labelName.frame.origin.x+labelName.frame.size.height, 100, 20)];
        }
        
        labelNameRestaurant.textColor = [UIColor whiteColor];
        labelNameRestaurant.textAlignment = NSTextAlignmentJustified;
        labelNameRestaurant.font =  FONT_NORMAL_TEXT;
        labelNameRestaurant.tag = 3;
        
        labelPrice.textColor = [UIColor whiteColor];
        labelPrice.textAlignment = NSTextAlignmentRight;
        labelPrice.font =  FONT_NORMAL_TEXT;
        labelPrice.tag = 4;
  
        
        //Menu Properties
        organic = [[UIImageView alloc] init];
        organic.userInteractionEnabled = YES;
        organic.tag = 5;
        
        gluten = [[UIImageView alloc] init];
        gluten.userInteractionEnabled = YES;
        gluten.tag = 6;
        
        spicy = [[UIImageView alloc] init];
        spicy.userInteractionEnabled = YES;
        spicy.tag = 7;
        
        healthy = [[UIImageView alloc] init];
        healthy.userInteractionEnabled = YES;
        healthy.tag = 8;
        
        new = [[UIImageView alloc] init];
        new.userInteractionEnabled = YES;
        new.tag = 9;
        
        gourmet = [[UIImageView alloc] init];
        gourmet.userInteractionEnabled = YES;
        gourmet.tag = 10;
        
        vegetarian = [[UIImageView alloc] init];
        vegetarian.userInteractionEnabled = YES;
        vegetarian.tag = 11;
        
        bestseller = [[UIImageView alloc] init];
        bestseller.userInteractionEnabled = YES;
        bestseller.tag = 12;
        
        peanut = [[UIImageView alloc] init];
        peanut.userInteractionEnabled = YES;
        peanut.tag = 13;
        
        
        //Rating
        starRatingView = [[HCSStarRatingView alloc] initWithFrame:CGRectMake(12, labelNameRestaurant.frame.origin.y+labelNameRestaurant.frame.size.height-2, 100, 30)];
        starRatingView.maximumValue = 5;
        starRatingView.minimumValue = 0;
        starRatingView.allowsHalfStars = YES;
        starRatingView.tag = 14;
        starRatingView.enabled = NO;
        starRatingView.tintColor = GREEN_COLOR;
        [view addSubview:starRatingView];
        
        //Add elements for detailsMenu
        [descriptionMenu addSubview:labelNameRestaurant];
        [descriptionMenu addSubview:labelPrice];
        [descriptionMenu addSubview:organic];
        [descriptionMenu addSubview:gluten];
        [descriptionMenu addSubview:spicy];
        [descriptionMenu addSubview:healthy];
        [descriptionMenu addSubview:new];
        [descriptionMenu addSubview:gourmet];
        [descriptionMenu addSubview:bestseller];
        [descriptionMenu addSubview:peanut];
        [descriptionMenu addSubview:vegetarian];
        [descriptionMenu addSubview:starRatingView];

        //Add elements for the recycled view
        [view addSubview:image];
        
    }
    else
    {
       //get a reference to the label, images, views and startRating in the recycled view
        labelName = (UILabel *)[view viewWithTag:1];
        labelDescription = (UILabel *)[view viewWithTag:15];
        image = (UIImageView *)[view viewWithTag:2];
        labelNameRestaurant = (UILabel *)[view viewWithTag:3];
        labelPrice = (UILabel *)[view viewWithTag:4];
        organic = (UIImageView *)[view viewWithTag:5];
        gluten = (UIImageView *)[view viewWithTag:6];
        spicy = (UIImageView *)[view viewWithTag:7];
        healthy = (UIImageView *)[view viewWithTag:8];
        new = (UIImageView *)[view viewWithTag:9];
        gourmet = (UIImageView *)[view viewWithTag:10];
        vegetarian = (UIImageView *)[view viewWithTag:11];
        bestseller = (UIImageView *)[view viewWithTag:12];
        peanut = (UIImageView *)[view viewWithTag:13];
        starRatingView = (HCSStarRatingView *) [view viewWithTag:14];
        descriptionMenu = (UIView *)[view viewWithTag:16];
        bt = (UIButton *)[view viewWithTag:17];
        button = (UIButton *)[view viewWithTag:18];
        
    }
    
    //Image menu
    [image sd_setImageWithURL: [NSURL URLWithString:[[[self.dishes valueForKey:@"menu_list"] objectAtIndex:index] valueForKey:@"menu_photo"]] placeholderImage:[UIImage imageNamed:@"LogoTransparente"]];
    
   //Image buttom
    [bt setImage:[UIImage imageNamed:@"Flecha"] forState:UIControlStateNormal];
    
    //Hide description menu
    if([[UIScreen mainScreen] bounds].size.height == 480)
    {
        [descriptionMenu setFrame:CGRectMake(0,  image.frame.origin.y+image.frame.size.height- (image.frame.size.height*34.5/100), view.frame.size.width, image.frame.size.height)];
    }
    
    else if([[UIScreen mainScreen] bounds].size.height == 568)
    {
        [descriptionMenu setFrame:CGRectMake(0,  image.frame.origin.y+image.frame.size.height- (image.frame.size.height*31/100), view.frame.size.width, image.frame.size.height)];
    }
    //iPhone 6 plus
    else if([[UIScreen mainScreen] bounds].size.height == 736)
    {
        [descriptionMenu setFrame:CGRectMake(0,  image.frame.origin.y+image.frame.size.height- (image.frame.size.height*21.5/100), view.frame.size.width, image.frame.size.height)];
    }
    else
    {
        [descriptionMenu setFrame:CGRectMake(0,  image.frame.origin.y+image.frame.size.height- (image.frame.size.height*25/100), view.frame.size.width, image.frame.size.height)];
    }

    
    //Text for labels
    labelName.text = [[[self.dishes valueForKey:@"menu_list"] objectAtIndex:index] valueForKey:@"menu_name"];
  /*  if(labelName.text.length >= 30)
    {
        if([[UIScreen mainScreen] bounds].size.height == 480)
        {
           labelName.font = [UIFont fontWithName:@"Century Gothic" size:15];
        }
    }*/
    
    if([[[[self.dishes valueForKey:@"menu_list"] objectAtIndex:index] valueForKey:@"menu_calories"] isEqualToString:@"0"])
    {
        labelDescription.text = [[[self.dishes valueForKey:@"menu_list"] objectAtIndex:index] valueForKey:@"menu_description"];
    }
    else
    {
        labelDescription.text = [NSString stringWithFormat:@"%@. The dish contains %@ calories", [[[self.dishes valueForKey:@"menu_list"] objectAtIndex:index] valueForKey:@"menu_description"], [[[self.dishes valueForKey:@"menu_list"] objectAtIndex:index] valueForKey:@"menu_calories"]];
    }
    
    labelNameRestaurant.text = [[[self.dishes valueForKey:@"menu_list"] objectAtIndex:index] valueForKey:@"restaurant_name"];
    labelPrice.text = [NSString stringWithFormat:@"$%@", [[[self.dishes valueForKey:@"menu_list"] objectAtIndex:index] valueForKey:@"menu_price"]];
    
    //Menu Properties
    
    if([[[[self.dishes valueForKey:@"menu_list"] objectAtIndex:index] valueForKey:@"menu_organic"] isEqualToString:@"Yes"])
    {
        [organic setFrame:CGRectMake(view.frame.size.width-cont-size, labelPrice.frame.size.height+labelPrice.frame.origin.y+5, size,size)];
       organic.image = [UIImage imageNamed:@"Organic"];
        UITapGestureRecognizer *showPopUp = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(showMiniPopUp:)];

       [organic addGestureRecognizer:showPopUp];

       cont = cont +size+2;
        organic.hidden = NO;

    }
    else
    {
        organic.hidden = YES;
    }

    if([[[[self.dishes valueForKey:@"menu_list"] objectAtIndex:index] valueForKey:@"menu_gluten"] isEqualToString:@"Yes"])
    {
        [gluten setFrame:CGRectMake(view.frame.size.width-cont-size, labelPrice.frame.size.height+labelPrice.frame.origin.y+5, size,size)];
        gluten.image = [UIImage imageNamed:@"GLutenFree"];
        UITapGestureRecognizer *showPopUp = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(showMiniPopUp:)];

        [gluten addGestureRecognizer:showPopUp];
        cont = cont +size+2;
        gluten.hidden = NO;
    }
    else
    {
          gluten.hidden = YES;
    }
    
    if([[[[self.dishes valueForKey:@"menu_list"] objectAtIndex:index] valueForKey:@"menu_spicy"] isEqualToString:@"Yes"])
    {
        [spicy setFrame:CGRectMake(view.frame.size.width-cont-size, labelPrice.frame.size.height+labelPrice.frame.origin.y+5, size,size)];
        
        spicy.image = [UIImage imageNamed:@"Spicy"];
        UITapGestureRecognizer *showPopUp = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(showMiniPopUp:)];

        [spicy addGestureRecognizer:showPopUp];
        cont = cont +size+2;
        spicy.hidden = NO;
    }
    else
    {
           spicy.hidden = YES;
    }
    
    
    if([[[[self.dishes valueForKey:@"menu_list"] objectAtIndex:index] valueForKey:@"menu_healthy"] isEqualToString:@"Yes"])
    {
        [healthy setFrame:CGRectMake(view.frame.size.width-cont-size, labelPrice.frame.size.height+labelPrice.frame.origin.y+5, size,size)];
        
        healthy.image = [UIImage imageNamed:@"Healthy"];
        UITapGestureRecognizer *showPopUp = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(showMiniPopUp:)];

        [healthy addGestureRecognizer:showPopUp];
        cont = cont +size+2;
        healthy.hidden = NO;
    }
    else
    {
        healthy.hidden = YES;
    }
    
    if([[[[self.dishes valueForKey:@"menu_list"] objectAtIndex:index] valueForKey:@"menu_new"] isEqualToString:@"Yes"])
    {
        [new setFrame:CGRectMake(view.frame.size.width-cont-size, labelPrice.frame.size.height+labelPrice.frame.origin.y+5, size,size)];
        
        new.image = [UIImage imageNamed:@"New"];
        UITapGestureRecognizer *showPopUp = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(showMiniPopUp:)];

        [new addGestureRecognizer:showPopUp];
        cont = cont +size+2;
         new.hidden = NO;
    }
    else
    {
        new.hidden = YES;
    }
    
    if([[[[self.dishes valueForKey:@"menu_list"] objectAtIndex:index] valueForKey:@"menu_gourmet"] isEqualToString:@"Yes"])
    {
        [gourmet setFrame:CGRectMake(view.frame.size.width-cont-size,labelPrice.frame.size.height+labelPrice.frame.origin.y+5, size,size)];
        
        gourmet.image = [UIImage imageNamed:@"Gourmet"];
        UITapGestureRecognizer *showPopUp = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(showMiniPopUp:)];

        [gourmet addGestureRecognizer:showPopUp];
        cont = cont +size+2;
          gourmet.hidden = NO;

    }
    else
    {
          gourmet.hidden = YES;
    }
    
    if([[[[self.dishes valueForKey:@"menu_list"] objectAtIndex:index] valueForKey:@"menu_vegetarian"] isEqualToString:@"Yes"])
    {
        [vegetarian setFrame:CGRectMake(view.frame.size.width-cont-size, labelPrice.frame.size.height+labelPrice.frame.origin.y+5, size,size)];
        
        vegetarian.image = [UIImage imageNamed:@"Vegetarian"];
        UITapGestureRecognizer *showPopUp = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(showMiniPopUp:)];

        [vegetarian addGestureRecognizer:showPopUp];
        cont = cont +size+2;
         vegetarian.hidden = NO;
    }
    else
    {
          vegetarian.hidden = YES;
    }
    
    if([[[[self.dishes valueForKey:@"menu_list"] objectAtIndex:index] valueForKey:@"menu_bestseller"] isEqualToString:@"Yes"])
    {
        [bestseller setFrame:CGRectMake(view.frame.size.width-cont-size,labelPrice.frame.size.height+labelPrice.frame.origin.y+5, size,size)];
        
        bestseller.image = [UIImage imageNamed:@"BestSeller"];
        UITapGestureRecognizer *showPopUp = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(showMiniPopUp:)];

        [bestseller addGestureRecognizer:showPopUp];
        cont = cont +size+2;
        bestseller.hidden = NO;

    }
    else
    {
           bestseller.hidden = YES;
    }
    
    if([[[[self.dishes valueForKey:@"menu_list"] objectAtIndex:index] valueForKey:@"menu_peanut"] isEqualToString:@"Yes"])
    {
        [peanut setFrame:CGRectMake(view.frame.size.width-cont-size, labelPrice.frame.size.height+labelPrice.frame.origin.y+5, size,size)];
        
        peanut.image = [UIImage imageNamed:@"Peanut"];
        UITapGestureRecognizer *showPopUp = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(showMiniPopUp:)];

        [peanut addGestureRecognizer:showPopUp];
        cont = cont +size+2;
          peanut.hidden = NO;
    }
    else
    {
        peanut.hidden = YES;
    }
   
    NSString * rating = [[[self.dishes valueForKey:@"menu_list"] objectAtIndex:index] valueForKey:@"restaurant_rating"] ;
    starRatingView.value = [rating floatValue];
    
    return view;
}
-(UIView *)carousel:(iCarousel *)carousel placeholderViewAtIndex:(NSInteger)index reusingView:(UIView *)view
{
    return view;
        
}
- (NSInteger)numberOfPlaceholdersInCarousel:(__unused iCarousel *)carousel
{
    //note: placeholder views are only displayed on some carousels if wrapping is disabled
    return 1;
}


- (CGFloat)carousel:(__unused iCarousel *)carousel valueForOption:(iCarouselOption)option withDefault:(CGFloat)value
{
    //customize carousel display
    switch (option)
    {
        case iCarouselOptionWrap:
        {
            //normally you would hard-code this to YES or NO
            return self.wrap;
        }
        case iCarouselOptionSpacing:
        {
            //add a bit of spacing between the item views
            return value * 1.00f;
        }
        case iCarouselOptionFadeMax:
        {
            if (self.carousel.type == iCarouselTypeCustom)
            {
                //set opacity based on distance from camera
                return 0.0f;
            }
            return value;
        }
        case iCarouselOptionShowBackfaces:
        case iCarouselOptionRadius:
        case iCarouselOptionAngle:
        case iCarouselOptionArc:
        case iCarouselOptionTilt:
        case iCarouselOptionCount:
        case iCarouselOptionFadeMin:
        case iCarouselOptionFadeMinAlpha:
        case iCarouselOptionFadeRange:
        case iCarouselOptionOffsetMultiplier:
        case iCarouselOptionVisibleItems:
        {
            return value;
        }
    }
}

#pragma mark - iCarousel taps

- (void)carousel:(__unused iCarousel *)carousel didSelectItemAtIndex:(NSInteger)index
{
    //  NSNumber *item = (self.items)[(NSUInteger)index];
    NSLog(@"Tapped view number: %ld", (long)index);
}

- (void)carouselCurrentItemIndexDidChange:(__unused iCarousel *)carousl
{
    NSLog(@"Index: %@", @(self.carousel.currentItemIndex));
  
        addToCart = [[self.dishes valueForKey:@"menu_list"] objectAtIndex:(self.carousel.currentItemIndex)];
        [carousl reloadData];
    
}

#pragma mark - Dish Description taps

- (void)showDescription: (UITapGestureRecognizer *) sender
{
    up = YES;
    
    if ([self.popTip isVisible]) {
         [self.popTip hide];
    }
    
    bt = (UIButton *)[sender.view viewWithTag:17];
    
    [bt setImage:[UIImage imageNamed:@"FlechaAbajo"] forState:UIControlStateNormal];

    [UIView animateWithDuration:0.50 delay:0 options:UIViewAnimationOptionCurveEaseOut animations:^{
        
        sender.view.frame = CGRectMake(0, 0, [[UIScreen mainScreen] bounds].size.width, sender.view.frame.size.height);
    } completion:^(BOOL finished) {
        
    }];
}

- (void)hideDescription: (UITapGestureRecognizer *) sender
{
    if ([self.popTip isVisible]) {
        [self.popTip hide];
    }

    up = NO;
    bt = (UIButton *)[sender.view viewWithTag:17];

    [bt setImage:[UIImage imageNamed:@"Flecha"] forState:UIControlStateNormal];
  
    [UIView animateWithDuration:0.50 delay:0 options:UIViewAnimationOptionCurveEaseOut animations:^{
        
        if([[UIScreen mainScreen] bounds].size.height == 480)
        {
            sender.view.frame = CGRectMake(0, (carousel.frame.size.height*65.5/100), [[UIScreen mainScreen] bounds].size.width,         sender.view.frame.size.height);

        }
        else if([[UIScreen mainScreen] bounds].size.height == 568)
        {
            sender.view.frame = CGRectMake(0, (carousel.frame.size.height*69/100), [[UIScreen mainScreen] bounds].size.width,         sender.view.frame.size.height);
        }
        //iPhone 6 plus
        else if([[UIScreen mainScreen] bounds].size.height == 736)
        {
            sender.view.frame = CGRectMake(0, (carousel.frame.size.height*78.5/100), [[UIScreen mainScreen] bounds].size.width,         sender.view.frame.size.height);
        }
        else
        {
            sender.view.frame = CGRectMake(0, (carousel.frame.size.height*75/100), [[UIScreen mainScreen] bounds].size.width,         sender.view.frame.size.height);

        }
        
    } completion:^(BOOL finished) {
        
    }];
}

- (void)showAndHideDescription: (UITapGestureRecognizer *) sender
{
    
    if ([self.popTip isVisible]) {
        [self.popTip hide];
    }

    bt = (UIButton *)[sender.view.superview viewWithTag:17];
    
    if( sender.view.superview.frame.origin.y != 0)
    {
         up = YES;
        [bt setImage:[UIImage imageNamed:@"FlechaAbajo"] forState:UIControlStateNormal];
        [UIView animateWithDuration:0.50 delay:0 options:UIViewAnimationOptionCurveEaseOut animations:^{
        
            sender.view.superview.frame = CGRectMake(0, 0, [[UIScreen mainScreen] bounds].size.width,         sender.view.superview.frame.size.height);
        } completion:^(BOOL finished) {
        
        }];
    }
    else
    {
         up = NO;
        [bt setImage:[UIImage imageNamed:@"Flecha"] forState:UIControlStateNormal];

        [UIView animateWithDuration:0.50 delay:0 options:UIViewAnimationOptionCurveEaseOut animations:^{
          
            if([[UIScreen mainScreen] bounds].size.height == 480)
            {
                sender.view.superview.frame = CGRectMake(0, (carousel.frame.size.height*65.5/100), [[UIScreen mainScreen] bounds].size.width,         sender.view.superview.frame.size.height);
            }
            else if([[UIScreen mainScreen] bounds].size.height == 568)
            {
                sender.view.superview.frame = CGRectMake(0, (carousel.frame.size.height*69/100), [[UIScreen mainScreen] bounds].size.width,         sender.view.superview.frame.size.height);
            }
            //iPhone 6 plus
            else if([[UIScreen mainScreen] bounds].size.height == 736)
            {
                sender.view.superview.frame = CGRectMake(0, (carousel.frame.size.height*78.5/100), [[UIScreen mainScreen] bounds].size.width,         sender.view.superview.frame.size.height);
            }
            else
            {
                sender.view.superview.frame = CGRectMake(0, (carousel.frame.size.height*75/100), [[UIScreen mainScreen] bounds].size.width,         sender.view.superview.frame.size.height);
            }

        } completion:^(BOOL finished) {
            
        }];

    }
}

//MiniPopup
- (void)showMiniPopUp: (UITapGestureRecognizer*) sender
{
    [self.popTip hide];
    
    if ([self.popTip isVisible]) {
        return;
    }
    
    self.popTip.popoverColor = GREEN_COLOR;
    
    NSString *text;
    
    if(sender.view.tag == 5)
    {
        text = @"Organic";
    }
    if(sender.view.tag == 6)
    {
        text = @"Gluten Free";
    }
    if(sender.view.tag == 7)
    {
        text = @"Spicy";
    }
    if(sender.view.tag == 8)
    {
        text = @"Healthy";
    }
    if(sender.view.tag == 9)
    {
        text = @"New";
    }
    if(sender.view.tag == 10)
    {
        text = @"Gourmet";
    }
    if(sender.view.tag == 11)
    {
        text = @"Vegetarian";
    }
    if(sender.view.tag == 12)
    {
        text = @"Bestseller";
    }
    if(sender.view.tag == 13)
    {
        text = @"Contains peanut";
    }
    
    if(up)
    {
        [self.popTip showText:text direction:AMPopTipDirectionDown maxWidth:200 inView:self.view fromFrame:CGRectMake(sender.view.frame.origin.x, sender.view.frame.size.height+sender.view.frame.origin.y+45, sender.view.frame.size.width, sender.view.frame.size.height)];
    }
    else
    {
        [self.popTip showText:text direction:AMPopTipDirectionDown maxWidth:200 inView:self.view fromFrame:CGRectMake(sender.view.frame.origin.x, carousel.frame.size.height+carousel.frame.origin.y-32, sender.view.frame.size.width, sender.view.frame.size.height)];
    }
}

#pragma mark - PopUp Cancel Order

- (IBAction)cancelOrder:(id)sender {

     [Flurry logEvent:@"Cancel_Order"];
    self.imgFace.image = [UIImage imageNamed:@"HappyFace"];
    self.lbText.text = @"Maybe next time!";
    [self.imgFace removeConstraints:[self.lbTitle constraints]];
    [self.lbTitle removeConstraints:[self.lbTitle constraints]];
    
    [self.lbText removeConstraints:[self.lbTitle constraints]];
    
    self.lbText.font = self.lbTitle.font;
    UILabel *text = [[UILabel alloc] initWithFrame:CGRectMake(self.popUp.frame.size.width/2-150, self.lbText.frame.origin.y+20, 300,50)];
    
    text.font =[UIFont fontWithName:@"Century Gothic" size:15];
    text.textColor = LABEL_COLOR;
    text.textAlignment = NSTextAlignmentCenter;
    text.text = @"Your order has been cancelled";
    [self.popUp addSubview:text];
   
    self.btAccept.hidden = YES;
    self.btCancel.hidden = YES;
    UITapGestureRecognizer *close = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(closePopUpCancelOrder)];
    [self.viewPopUp addGestureRecognizer:close];
    [Cart removeAll];
    [self showIcons];
    
    [self performSelector:@selector(closePopUpCancelOrder) withObject:nil afterDelay:3.0 ];
}

-(void)closePopUpCancelOrder
{
    self.navBar.view.backgroundColor = GREEN_COLOR;
    self.viewPopUp.hidden = YES;
}

- (IBAction)continueOrder:(id)sender {
    CartViewController *cart = [[CartViewController alloc] init];
      
    [self.navigationController pushViewController:cart animated:NO];
}

#pragma mark - Go to

- (void)goToAddresses {
    if(![Customer IsGetAddresses])
    {
        [self performSelectorInBackground:@selector(startAnimation) withObject:self];
        [Requests getAddressForCustomer:[Customer getIdCustomer]];
        [Requests stopAnimationInView];
        if(![Customer IsGetAddresses])
        {
            alertErrorAddress = [[UIAlertView alloc] initWithTitle:@"We missed you!"
                                                             message:@"Check yor internet conection and try again"
                                                            delegate:self
                                                   cancelButtonTitle:@"Ok"
                                                   otherButtonTitles:@"Retry",nil];
            [alertErrorAddress show];
            return;
        }
    }
    
    [self.navBar pushTransition:self];

    [self.navigationController pushViewController:addressVC animated:NO];
}

- (IBAction)goToCart:(id)sender {
    
    [Flurry logEvent:@"Add_Dish"];
    CartViewController *cart = [[CartViewController alloc] init];
    cart.addMenu = addToCart;
    cart.addressIndex = addressIndex;
    
    [self.navBar pushTransition:self];
    
    [self.navigationController pushViewController:cart animated:NO];
}

- (void)goToCart {
    
    [Flurry logEvent:@"Open_Cart"];
    CartViewController *cart = [[CartViewController alloc] init];
    cart.addressIndex = addressIndex;
    
    [self.navBar pushTransition:self];
    
    [self.navigationController pushViewController:cart animated:NO];
}

#pragma mark - Menu

- (void)showMenu
{
    CNPGridMenuItem *PaymentMethods = [[CNPGridMenuItem alloc] init];
    PaymentMethods.icon = [UIImage imageNamed:@"Payment"];
    PaymentMethods.title = @"Payment";
       
    CNPGridMenuItem *Addresses = [[CNPGridMenuItem alloc] init];
    Addresses.icon = [UIImage imageNamed:@"Addresses"];
    Addresses.title = @"Addresses";
    
    CNPGridMenuItem *Logout = [[CNPGridMenuItem alloc] init];
    Logout.icon = [UIImage imageNamed:@"Logout"];
    Logout.title = @"Logout";
    
    CNPGridMenuItem *About = [[CNPGridMenuItem alloc] init];
    About.icon = [UIImage imageNamed:@"About"];
    About.title = @"About";
    
    CNPGridMenuItem *Cart = [[CNPGridMenuItem alloc] init];
    Cart.icon = [UIImage imageNamed:@"About"];
    Cart.title = @"Cart";
    
    CNPGridMenu *gridMenu = [[CNPGridMenu alloc] initWithMenuItems:@[PaymentMethods, Addresses, Logout, About]];
    gridMenu.delegate = self;
    [self presentGridMenu:gridMenu animated:YES completion:^{
        NSLog(@"Grid Menu Presented");
    }];
    
}

- (void)gridMenuDidTapOnBackground:(CNPGridMenu *)menu {
    [self dismissGridMenuAnimated:YES completion:^{
        NSLog(@"Grid Menu Dismissed With Background Tap");
    }];
}

- (void)gridMenu:(CNPGridMenu *)menu didTapOnItem:(CNPGridMenuItem *)item {
    [self dismissGridMenuAnimated:NO completion:^{
        NSLog(@"Grid Menu Did Tap On Item: %@", item.title);
        
        if ([item.title isEqualToString:@"Payment"])
        {
            PaymentViewController *pay = [[PaymentViewController alloc] init];
            [self.navBar pushTransition:self];
            [self.navigationController pushViewController:pay animated:NO];
        }
        
        if ([item.title isEqualToString:@"Addresses"])
        {
            [self goToAddresses];
        }
        
        if ([item.title isEqualToString:@"Logout"])
        {
            [Flurry logEvent:@"Logout"];
            NSDictionary *defaultsDictionary = [[NSUserDefaults standardUserDefaults] dictionaryRepresentation];
            for (NSString *key in [defaultsDictionary allKeys]) {
                if(![key isEqualToString:@"HasLaunchedOnce"])
                {
                    [[NSUserDefaults standardUserDefaults] removeObjectForKey:key];
                }
            }
            [FBSession.activeSession closeAndClearTokenInformation];
           
            [Cart removeAll];
            [Customer removeAll];
         
            LoginViewController *login = [[LoginViewController alloc] init];
            [self.navBar pushTransition:self];
            [self.navigationController pushViewController:login animated:NO];
            
        }
        
        if ([item.title isEqualToString:@"About"])
        {
            AboutNipViewController *about = [[AboutNipViewController alloc]init];
            [self.navBar pushTransition:self];
            [self.navigationController pushViewController:about animated:NO];
        }


    }];
}

#pragma mark - Rate and share menu
-(void)requestOrders : (NSDate *)timeNow
{
    count=0;
    
    NSDictionary *orders = [Requests getOrdersForCustomer:[Customer getIdCustomer]];
    
    if([orders objectForKey:@"orders"] != nil && [[orders objectForKey:@"orders"] count] )
    {
        for (int i=0; i<[[orders objectForKey:@"orders"]count]; i++)
        {
            NSString *str =[[[orders objectForKey:@"orders"]valueForKey:@"orderdate"]objectAtIndex:i];
            NSDateFormatter *dateFormatter = [[NSDateFormatter alloc] init];
            [dateFormatter setDateFormat:@"yyyy-MM-dd HH:mm:ss"];
           // dateFormatter.timeZone = [NSTimeZone timeZoneForSecondsFromGMT:0];
           //[dateFormatter setTimeZone:[NSTimeZone timeZoneWithName:@"GMT"]];
            
           NSDate *myDate = [dateFormatter dateFromString: str];
                      
            NSTimeInterval distanceBetweenDates = [timeNow timeIntervalSinceDate:myDate];
            int time = distanceBetweenDates * 1;
            
            if(time > 7200)
            {
                details = [Requests getDetailsForOrder:[[[orders objectForKey:@"orders"]valueForKey:@"ordergenerateid"]objectAtIndex:i]];
                menu = [details objectForKey:@"menu"];
                
                [self  rateAndShareMenu:[menu objectAtIndex:0]];
                return;
            }
            
        }
    }
}

-(void)rateAndShareMenu:(NSArray *)menu
{
    share = [[SharePurchaseViewController alloc]init];
        
    [share showInView:self];
    
    share.btShareGreen.hidden = YES;
    
    share.lbDish.text =[menu valueForKey:@"menu_name"];
    
    share.lbRestaurant.text =[menu valueForKey:@"restaurant_name"];
    
    [share.imgDish sd_setImageWithURL: [NSURL URLWithString:[menu valueForKey:@"menu_photo"]] placeholderImage:[UIImage imageNamed:@"LogoTransparente"]];
    
    [share.start addTarget:self action:@selector(didChangeValue:) forControlEvents:UIControlEventValueChanged];
    
    [share.btRating addTarget:self action:@selector(addRating) forControlEvents:UIControlEventTouchUpInside];
    
    [share.btClose addTarget:self action:@selector(closeShare) forControlEvents:UIControlEventTouchUpInside];
    
    [share.btShareGray addTarget:self action:@selector(shareMenu) forControlEvents:UIControlEventTouchUpInside];
    
    [share.btShareGreen addTarget:self action:@selector(shareMenu) forControlEvents:UIControlEventTouchUpInside];
    
    
    count++;
}
- (IBAction)didChangeValue:(HCSStarRatingView *)sender {
    
    NSLog(@"Changed rating to %.1f", sender.value);
    value = sender.value;
}

-(void)addRating
{
    [Requests addRatingForMenu:[[[details objectForKey:@"menu"] valueForKey:@"id"] objectAtIndex:count-1]  restaurant_id:[[[details objectForKey:@"menu"] valueForKey:@"restaurant_id"] objectAtIndex:count-1]  customer_id:[Customer getIdCustomer] ordergenerateid:[[details objectForKey:@"order"] valueForKey:@"ordergenerateid"]  rating:[NSString stringWithFormat:@"%d", value]];
    
    [self viewThanksWithValue:value];
}

-(void)viewThanksWithValue:(double)value
{
    share.imgDish.image = [UIImage imageNamed:@"HappyFace"];
    share.lbDish.text = @"Thanks for the input, now share the love!";
    share.lbDish.font = [UIFont fontWithName:@"Century Gothic" size:16];
    share.btShareGreen.hidden = NO;
    share.lbRestaurant.hidden = YES;
    share.btShareGray.hidden = YES;
    share.lbRate.hidden = YES;
    share.start.hidden = YES;
    share.start.value = value;
    share.btShareGray.hidden = YES;
    share.btRating.hidden = YES;
    
    HCSStarRatingView *starRatingView;
    //Rating
    if(!starRatingView)
    {
        starRatingView = [[HCSStarRatingView alloc] init];
    }
   
    if(share.lbDish.frame.size.height == 37)
    {
       [starRatingView setFrame:CGRectMake((share.viewPopUp.frame.size.width/2)-85, share.lbDish.frame.origin.y+share.lbDish.frame.size.height, 170, 40)];

    }
    else{
        [starRatingView setFrame:CGRectMake((share.viewPopUp.frame.size.width/2)-85, share.lbDish.frame.origin.y+share.lbDish.frame.size.height+15, 170, 40)];
    }
    
    starRatingView.maximumValue = 5;
    starRatingView.minimumValue = 0;
    starRatingView.allowsHalfStars = YES;
    starRatingView.value = value;
    starRatingView.tag = 14;
    starRatingView.offStars = GRAY_LIGHT_COLOR;
    starRatingView.enabled = NO;
    starRatingView.tintColor = GREEN_COLOR;
    [share.viewPopUp addSubview:starRatingView];
    
}

-(IBAction)closeShare
{
    [share removerFromView];
    
    [Requests addRatingForMenu:[[[details objectForKey:@"menu"] valueForKey:@"id"] objectAtIndex:0]  restaurant_id:[[[details objectForKey:@"menu"] valueForKey:@"restaurant_id"] objectAtIndex:0]  customer_id:[Customer getIdCustomer] ordergenerateid:[[details objectForKey:@"order"] valueForKey:@"ordergenerateid"]  rating:@"ready"];
    
    if([menu count]>count)
    {
        [self rateAndShareMenu:[menu objectAtIndex:count]];
        return;
    }
    
    @try {
        [self requestOrders:[[NSDate alloc]init]];
    }
    @catch (NSException *exception) {
        
    }

}

-(void)shareMenu
{
    //NSArray *dish = [menu objectAtIndex:count-1];
    
    UIImage *image = [UIImage imageNamed:@"Share"];

    
   // NSString *shareString = [NSString stringWithFormat:@"I just ordered this amazing meal using Nip \n %@", [dish valueForKey:@"menu_photo"]];
    
    
    NSArray *activityItems = [NSArray arrayWithObjects:image, nil];
    
    UIActivityViewController *activityViewController = [[UIActivityViewController alloc] initWithActivityItems:activityItems applicationActivities:nil];
    
    activityViewController.excludedActivityTypes = @[UIActivityTypePostToWeibo,
                                                     UIActivityTypePrint,
                                                     UIActivityTypeCopyToPasteboard,
                                                     UIActivityTypeAssignToContact,
                                                     UIActivityTypeSaveToCameraRoll,
                                                     UIActivityTypeAddToReadingList,
                                                     UIActivityTypePostToFlickr,
                                                     UIActivityTypePostToVimeo,
                                                     UIActivityTypePostToTencentWeibo,
                                                     UIActivityTypeAirDrop];
    
    
    
    activityViewController.modalTransitionStyle = UIModalTransitionStyleCoverVertical;
    
    [self presentViewController:activityViewController animated:NO completion:nil];
 
}

#pragma mark -

-(void)startAnimation
{
    [Requests startAnimationInView:self];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
