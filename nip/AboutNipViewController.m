//
//  AboutNipViewController.m
//  NIP
//
//  Created by MacCMS2 on 29/7/15.
//  Copyright (c) 2015 MacCMS2. All rights reserved.
//

#import "AboutNipViewController.h"
#import "AboutViewController.h"
#import "HomeViewController.h"

@interface AboutNipViewController ()

@property (weak, nonatomic) IBOutlet UILabel *lbTerms;
@property (weak, nonatomic) IBOutlet UILabel *lbVersion;
@property (weak, nonatomic) IBOutlet UILabel *lbEatnip;
@property (weak, nonatomic) IBOutlet UILabel *lbRate;
@property (weak, nonatomic) IBOutlet UILabel *lbMobileMedia;
@end

@implementation AboutNipViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    self.view.backgroundColor = GREEN_COLOR;
    //Header
    self.navBar = [[NavBarViewController alloc] init];
    self.navigationController.navigationBarHidden = YES;
    
    NSString * version = [[NSBundle mainBundle] objectForInfoDictionaryKey: @"CFBundleShortVersionString"];
    
    self.lbVersion.text = version;
    
    UITapGestureRecognizer * tapLbTerms = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapOnTerms:)];
    [self.lbTerms addGestureRecognizer:tapLbTerms];
    
    UITapGestureRecognizer * tapLbEatnip = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapOnSite:)];
    [self.lbEatnip addGestureRecognizer:tapLbEatnip];
    
    UITapGestureRecognizer * tapLbRate = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapOnRate:)];
    [self.lbRate addGestureRecognizer:tapLbRate];
    
    UITapGestureRecognizer * tapLbMobileMedia = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(aboutMobileMedia:)];
    [self.lbMobileMedia addGestureRecognizer:tapLbMobileMedia];
    
}

- (void) tapOnSite:(UITapGestureRecognizer *)tapRecognizer
{
   // [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"http://www.mobilemedia.net"]];
}

- (void) tapOnRate:(UITapGestureRecognizer *)tapRecognizer
{
    //[[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"http://www.mobilemedia.net"]];
}

- (void) tapOnTerms:(UITapGestureRecognizer *)tapRecognizer
{
    //[[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"http://www.mobilemedia.net"]];
}


- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (void)aboutMobileMedia:(id)sender {
    AboutViewController *about = [[AboutViewController alloc]init];
    [self.navBar pushTransition:self];
    [self.navigationController pushViewController:about animated:NO];

}
- (IBAction)goToHome:(id)sender {
    HomeViewController *home = [[HomeViewController alloc]init];
    home.haveAlreadyReceivedCoordinates = YES;
    [self.navBar pushTransition:self];
    
    [self.navigationController pushViewController:home animated:NO];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
