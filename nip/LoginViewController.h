//
//  LoginViewController.h
//  nip
//
//  Created by MacCMS2 on 24/04/15.
//  Copyright (c) 2015 MacCMS2. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <FacebookSDK/FacebookSDK.h>

@interface LoginViewController : UIViewController

//LoginFB
@property (weak, nonatomic) IBOutlet FBLoginView *btLogin;

//Header
@property (strong, nonatomic) NavBarViewController *navBar;


@end
