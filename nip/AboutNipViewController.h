//
//  AboutNipViewController.h
//  NIP
//
//  Created by MacCMS2 on 29/7/15.
//  Copyright (c) 2015 MacCMS2. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AboutNipViewController : UIViewController

//Header
@property (strong, nonatomic) NavBarViewController *navBar;
@property (weak, nonatomic) IBOutlet UILabel *lbAbout;
@end
