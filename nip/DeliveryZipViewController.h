//
//  DeliveryZipViewController.h
//  nip
//
//  Created by MacCMS2 on 24/04/15.
//  Copyright (c) 2015 MacCMS2. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "VisibleFormViewController.h"
#import <CoreLocation/CoreLocation.h>
#import <CoreLocation/CLLocationManagerDelegate.h>

@interface DeliveryZipViewController : VisibleFormViewController <CLLocationManagerDelegate>

//Header
@property (strong, nonatomic) NavBarViewController *navBar;

@property (weak, nonatomic) IBOutlet UIImageView *imgLogo;

@property (weak, nonatomic) IBOutlet UIButton *btGPS;

@property (weak, nonatomic) IBOutlet UITextField *txZipCode;
@end
