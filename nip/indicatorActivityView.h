//
//  indicadorActividadView.h
//  BancaMobilIphone
//
//  Created by Medianet on 16/10/14.
//  Copyright (c) 2014 com.provincial.bancamovil2. All rights reserved.
//


#import <UIKit/UIKit.h>
#define TRANSPARENCIA_INDICADOR_ACT 0.5
#define COLOR_INDICADOR_ACT [UIColor blackColor]



UIActivityIndicatorView * activityIndicator;
@interface indicatorActivityView : UIView

@end
