//
//  CartViewController.m
//  nip
//
//  Created by MacCMS2 on 05/05/15.
//  Copyright (c) 2015 MacCMS2. All rights reserved.
//

#import "CartViewController.h"
#import "Cart.h"
#import "HomeViewController.h"
#import "CartTableViewCell.h"
#import "Stripe.h"
#import "PaymentViewController.h"
#import "PTKView.h"
#import "Requests.h"
#import "Customer.h"
#import "SDWebImage/UIImageView+WebCache.h"
#import "PopUpViewController.h"

#define STRIPE_TEST_PUBLIC_KEY @"pk_test_0uVGnrXH5525TyTmyX5JUHDc"
#define STRIPE_TEST_POST_URL

@interface CartViewController ()

@end

@implementation CartViewController
@synthesize lbAddress,lbGrandTotal,lbMethod,lbOrderSumary,lbPaymentMethod,lbPriceGrandTotal,lbPriceSubtotal,lbPriceTax,lbShipTo,lbSubtotal,lbTax,btAddAnother,btCancelOrder,btEditPaymentMethod,btPlaceOrder,cellOrderSumery,cellPaymentMethod,cellShipTo, lbFree, lbDeliveryFee;

NSMutableArray *cart;
UILabel *lbPriceSub;
UILabel *lbPricetax;
UILabel *lbPriceGrand;
NSMutableArray *listCard;
NSIndexPath *ind;

float subtotal= 0;
float tax =0;
float grandTotal =0;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    ind = nil;
    //Header
    self.navBar = [[NavBarViewController alloc] init];
    [self.navBar showInView:self withTitle:@"Your Order" hiddenButtom:NO];
    self.navigationController.navigationBarHidden = YES;
    self.navBar.imgAddress.hidden = YES;
    self.navBar.btAddress.enabled = NO;
    self.navBar.horizontalConstraint.constant = 0;
    [self.navBar.imgAddress removeFromSuperview];

    
    NSArray *address = [Customer getAddresses];
    
    lbAddress.text = [NSString stringWithFormat:@"\"%@\" - %@ %@ %@ %@ %@ %@", [[address valueForKey:@"customer_address_title"] objectAtIndex:self.addressIndex], [[address valueForKey:@"customer_apartment_name"] objectAtIndex:self.addressIndex], [[address valueForKey:@"customer_street"] objectAtIndex:self.addressIndex],  [[address valueForKey:@"customer_city"] objectAtIndex:self.addressIndex], [[address valueForKey:@"customer_state"] objectAtIndex:self.addressIndex] , [[address valueForKey:@"customer_zip"] objectAtIndex:self.addressIndex],[[address valueForKey:@"customer_address_phone_number"] objectAtIndex:self.addressIndex]];

    
    //Color of Labels Titles
    lbOrderSumary.textColor = TITLE_COLOR;
    lbShipTo.textColor = TITLE_COLOR;
    lbPaymentMethod.textColor = TITLE_COLOR;
   
      
    //Color of the other labels
    lbSubtotal.textColor = LABEL_COLOR;
    lbTax.textColor = LABEL_COLOR;
    lbGrandTotal.textColor = LABEL_COLOR;
    lbPriceSubtotal.textColor = TITLE_COLOR;
    lbPriceTax.textColor = TITLE_COLOR;
    lbPriceGrandTotal.textColor = TITLE_COLOR;
    lbAddress.textColor = LABEL_COLOR;
    lbMethod.textColor = LABEL_COLOR;
    btCancelOrder.tintColor = LABEL_COLOR;
    lbFree.textColor = TITLE_COLOR;
    lbDeliveryFee.textColor = LABEL_COLOR;

    
    //Rounded Border Buttom
    btAddAnother.backgroundColor = GRAY_COLOR;
    [self setRoundedBorder:1 borderWidth:0 color:GRAY_COLOR forButton:btAddAnother];
    
    btPlaceOrder.backgroundColor = GREEN_COLOR;
    [self setRoundedBorder:2 borderWidth:0 color:GREEN_COLOR forButton:btPlaceOrder];
    
    
    if([[UIScreen mainScreen] bounds].size.height == 480)
    {
        lbPriceSub = [[UILabel alloc] initWithFrame:CGRectMake(lbPriceSubtotal.frame.origin.x-55, lbPriceSubtotal.frame.origin.y, 90, lbPriceSubtotal.frame.size.height)];
        
        lbPricetax = [[UILabel alloc] initWithFrame:CGRectMake(lbPriceTax.frame.origin.x-55, lbPriceTax.frame.origin.y, 90, lbPriceTax.frame.size.height)];
        
        lbPriceGrand = [[UILabel alloc] initWithFrame:CGRectMake(lbPriceGrandTotal.frame.origin.x-55, lbPriceGrandTotal.frame.origin.y, 90, lbPriceGrandTotal.frame.size.height)];

    }
    
    else if([[UIScreen mainScreen] bounds].size.height == 568)
    {
        lbPriceSub = [[UILabel alloc] initWithFrame:CGRectMake(lbPriceSubtotal.frame.origin.x-57, lbPriceSubtotal.frame.origin.y,90, lbPriceSubtotal.frame.size.height)];
        
        lbPricetax = [[UILabel alloc] initWithFrame:CGRectMake(lbPriceTax.frame.origin.x-57, lbPriceTax.frame.origin.y, 90, lbPriceTax.frame.size.height)];
        
        lbPriceGrand = [[UILabel alloc] initWithFrame:CGRectMake(lbPriceGrandTotal.frame.origin.x-57, lbPriceGrandTotal.frame.origin.y, 90, lbPriceGrandTotal.frame.size.height)];
    }
    //iPhone 6 plus
    else if([[UIScreen mainScreen] bounds].size.height == 736)
    {
        lbPriceSub = [[UILabel alloc] initWithFrame:CGRectMake(lbPriceSubtotal.frame.origin.x+32, lbPriceSubtotal.frame.origin.y,90, lbPriceSubtotal.frame.size.height)];
        
        lbPricetax = [[UILabel alloc] initWithFrame:CGRectMake(lbPriceTax.frame.origin.x+32, lbPriceTax.frame.origin.y, 90, lbPriceTax.frame.size.height)];
        
        lbPriceGrand = [[UILabel alloc] initWithFrame:CGRectMake(lbPriceGrandTotal.frame.origin.x+32, lbPriceGrandTotal.frame.origin.y, 90, lbPriceGrandTotal.frame.size.height)];
    }
    else
    {
        lbPriceSub = [[UILabel alloc] initWithFrame:CGRectMake(lbPriceSubtotal.frame.origin.x, lbPriceSubtotal.frame.origin.y, 90, lbPriceSubtotal.frame.size.height)];
        
        lbPricetax = [[UILabel alloc] initWithFrame:CGRectMake(lbPriceTax.frame.origin.x, lbPriceTax.frame.origin.y, 90, lbPriceTax.frame.size.height)];
        
         lbPriceGrand = [[UILabel alloc] initWithFrame:CGRectMake(lbPriceGrandTotal.frame.origin.x, lbPriceGrandTotal.frame.origin.y,90 , lbPriceGrandTotal.frame.size.height)];
    }
    
    lbPriceSub.font = lbPriceSubtotal.font;
    lbPriceSub.textAlignment =  NSTextAlignmentRight;
    
    lbPricetax.font = lbPriceSubtotal.font;
    lbPricetax.textAlignment =  NSTextAlignmentRight;
   
    lbPriceGrand.font = lbPriceSubtotal.font;
    lbPriceGrand.textAlignment = NSTextAlignmentRight;
    
    lbPriceSub.textColor = TITLE_COLOR;
    lbPricetax.textColor = TITLE_COLOR;
    lbPriceGrand.textColor = TITLE_COLOR;
    
    [cellOrderSumery addSubview:lbPriceSub];
    [cellOrderSumery addSubview:lbPricetax];
    [cellOrderSumery addSubview:lbPriceGrand];
     
    if([Customer getListCards] != nil && [[Customer getListCards] count] >0 )
    {
        listCard = [Customer getListCards];
        NSUserDefaults *datos = [NSUserDefaults standardUserDefaults];
        NSString *cardId = [datos stringForKey:@"cardId"];
        
        if(cardId != nil && ![cardId isEqualToString:@""])
        {
            for(int i=0; i<[listCard count]; i++)
            {
                if([cardId isEqualToString:[[listCard valueForKey:@"id"] objectAtIndex:i]])
                {
                    lbMethod.text = [NSString stringWithFormat:@"Ending %@",[[listCard valueForKey:@"last4"]objectAtIndex:i]];
                    
                    NSString *cardType = [[listCard valueForKey:@"type"] objectAtIndex:i];
                    NSString *cardTypeName = @"placeholder";
                    
                    if([cardType isEqualToString:@"American Express"])
                    {
                        cardTypeName = @"amex";
                    }
                    else if([cardType isEqualToString:@"Diners Club"])
                    {
                        cardTypeName = @"diners";
                    }
                    else if([cardType isEqualToString:@"Discover"])
                    {
                        cardTypeName = @"discover";
                    }
                    else if([cardType isEqualToString:@"JCB"])
                    {
                        cardTypeName = @"jcb";
                    }
                    else if([cardType isEqualToString:@"MasterCard"])
                    {
                        cardTypeName = @"mastercard";
                    }
                    else if([cardType isEqualToString:@"Visa"])
                    {
                        cardTypeName = @"visa";
                    }
                    
                    self.imgCardType.image = [UIImage imageNamed:cardTypeName];
                    ind = [NSIndexPath indexPathForRow:i inSection:i];
                    break;
                }
            }
        }
    }
    
    
    NSMutableArray *temp = [[NSMutableArray alloc] initWithArray:[Cart getCart]];
    
    [self addDish:self.addMenu noRepeatIn:temp];
    cart = [[NSMutableArray alloc] initWithArray:[Cart getCart]];
    
    //Calculated subtotal, tax and grandtotal
    [self calculatePrice];
}

#pragma mark - Cart

-(void)addDish:(NSArray*)addMenu noRepeatIn:(NSMutableArray *)tempArray
{
    //If a menu item is selected add to cart
    if(addMenu !=nil && [addMenu count] > 0)
    {
        bool repeat = NO;

        @try {
            for (int i=0; i<[tempArray count]; i++) {
                if([[[tempArray valueForKey:@"menu_id"] objectAtIndex:i] isEqualToString:[addMenu valueForKey:@"id"]])
                {
                    [[tempArray objectAtIndex:i] setValue:[NSString stringWithFormat:@"%i", [[[tempArray valueForKey:@"quantity"]objectAtIndex:i] intValue]+1]forKey:@"quantity"];
                    
                    float total =  [[[cart valueForKey:@"menu_price"] objectAtIndex:i] floatValue] * [[[tempArray valueForKey:@"quantity"]objectAtIndex:i] floatValue];
                    
                    [[tempArray objectAtIndex:i] setValue:[NSString stringWithFormat:@"%f", total] forKey:@"total"];
                    repeat = YES;
                    break;
                }
            }

        }
        @catch (NSException *exception) {
            
        }
        
        if(!repeat)
        {
            NSMutableDictionary *dish = [[NSMutableDictionary alloc] initWithObjectsAndKeys:[addMenu valueForKey:@"id"], @"menu_id", [addMenu valueForKey:@"restaurant_id"], @"res_id", [addMenu valueForKey:@"menu_name"], @"menu_name", @"1", @"quantity", [addMenu valueForKey:@"menu_price"], @"menu_price", [addMenu valueForKey:@"menu_price"], @"total", [addMenu valueForKey:@"restaurant_name"], @"restaurant_name", [addMenu valueForKey:@"restaurant_salestax"], @"restaurant_salestax",[addMenu valueForKey:@"menu_photo"], @"menu_photo", nil];
            [Cart addDishToCart:dish];
        }
    }
}

-(void)calculatePrice
{
    subtotal = 0;
    tax = 0;
    if([cart count] <= 0)
    {
        lbPriceSub.text = @"$ 0.00";
        lbPriceGrand.text =  @"$ 0.00";
        lbPricetax.text =  @"$ 0.00";
        HomeViewController *home = [[HomeViewController alloc] init];
        home.haveAlreadyReceivedCoordinates = YES;
        [self.navBar pushTransition:self];
        
        [self.navigationController pushViewController:home animated:NO];
    }
    else
    {
        for(int i=0; i< [cart count]; i++)
        {
            subtotal = [[[cart valueForKey:@"total"] objectAtIndex:i] floatValue] + subtotal;
            tax = (([[[cart valueForKey:@"total"] objectAtIndex:i]floatValue]*[[[cart valueForKey:@"restaurant_salestax"] objectAtIndex:i]floatValue ])/100)+tax;
        }
        
        grandTotal= subtotal + tax;
        
        lbPriceSub.text = [NSString stringWithFormat:@"$ %.02f", subtotal];
        lbPricetax.text = [NSString stringWithFormat:@"$ %.02f", tax];
        lbPriceGrand.text = [NSString stringWithFormat:@"$ %.02f", grandTotal];

    }
    
}

- (IBAction)addAnotherOrder:(id)sender {
    
    [Flurry logEvent:@"Add_Another_Dish"];
    [self back];
}

#pragma mark - Table View


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(indexPath.row == [cart count])
    {
        return 207;
    }
    else if(indexPath.row == [cart count]+1)
    {
        return 72;
    }
    else if(indexPath.row == [cart count]+2)
    {
        return 190;
    }
    else
    {
         return 140;
    }

    return 40;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [cart count]+3;
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    CartTableViewCell *cell =  [tableView dequeueReusableCellWithIdentifier:@"myCell"];
    
    if(!cell)
    {
        [tableView  registerNib:[UINib nibWithNibName:@"CartTableViewCell" bundle:nil]forCellReuseIdentifier:@"myCell"];
        cell =  [tableView  dequeueReusableCellWithIdentifier:@"myCell"];
    }
   
    if(indexPath.row == [cart count])
    {
        return cellOrderSumery;
    }
    else if(indexPath.row == [cart count]+1)
    {
        return cellShipTo;
    }
    else if(indexPath.row == [cart count]+2)
    {
        return cellPaymentMethod;
    }
    else
    {
        cell.lbRestaurantName.text =[[cart valueForKey:@"menu_name"] objectAtIndex:indexPath.row];
        cell.lbMenuName.text = [[cart valueForKey:@"restaurant_name"] objectAtIndex:indexPath.row];
        cell.lbPriceMenu.text = [NSString stringWithFormat:@"$ %@",[[cart valueForKey:@"menu_price"] objectAtIndex:indexPath.row]];
        cell.btLess.tag = indexPath.row;
        cell.btMore.tag = indexPath.row;
        cell.lbNumber.tag =indexPath.row;
        NSLog(@"%li", (long)indexPath.row);
        
        cell.cont = [[[cart valueForKey:@"quantity"] objectAtIndex:indexPath.row] intValue];

        cell.lbNumber.text = [[cart valueForKey:@"quantity"] objectAtIndex:indexPath.row];
        
        //Image menu
        [cell.imgMenu sd_setImageWithURL: [NSURL URLWithString:[[cart valueForKey:@"menu_photo"] objectAtIndex:indexPath.row]] placeholderImage:[UIImage imageNamed:@"LogoTransparente"]];
      
        return cell;
  
    }
    
    
    return cell;    
   
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    if(indexPath.row == [cart count])
    {
        return NO;
    }
    else if(indexPath.row == [cart count]+1)
    {
        return NO;
    }
    else if(indexPath.row == [cart count]+2)
    {
        return NO;
    }
    else
    {
        return YES;
    }

    return YES;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{

}

-(NSArray *)tableView:(UITableView *)tableView editActionsForRowAtIndexPath:(NSIndexPath *)indexPath {
    UITableViewRowAction *button = [UITableViewRowAction rowActionWithStyle:UITableViewRowActionStyleDefault title:@"X" handler:^(UITableViewRowAction *action, NSIndexPath *indexPath)
        {
            [cart removeObjectAtIndex:indexPath.row];
            //Calculated subtotal, tax and grandtotal
            [self calculatePrice];
            [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
            [Flurry logEvent:@"Delete_Dish"];
         }];
    
    button.backgroundColor = [[UIColor blackColor]colorWithAlphaComponent:0.1];
    
    return @[button];
}

+(void)updatePrice:(UIButton *)button operation : (NSString *) operation quantity : (NSString *) quantity
{
    
    float total =  [[[cart valueForKey:@"menu_price"] objectAtIndex:button.tag] floatValue] * [quantity floatValue];
    
    [[cart objectAtIndex:button.tag] setValue:quantity forKey:@"quantity"];
    [[cart objectAtIndex:button.tag] setValue:[NSString stringWithFormat:@"%f", total] forKey:@"total"];
    
    if([operation isEqualToString:@"+"])
    {
        subtotal = [[[cart valueForKey:@"menu_price"] objectAtIndex:button.tag] floatValue] + subtotal;
        tax = (([[[cart valueForKey:@"menu_price"] objectAtIndex:button.tag]floatValue]*[[[cart valueForKey:@"restaurant_salestax"] objectAtIndex:button.tag]floatValue ])/100)+tax;
        [Flurry logEvent:@"Add_Quantity_Dish"];
    }
    else
    {
        subtotal = subtotal - [[[cart valueForKey:@"menu_price"] objectAtIndex:button.tag] floatValue];
        tax = tax - (([[[cart valueForKey:@"menu_price"] objectAtIndex:button.tag]floatValue]*[[[cart valueForKey:@"restaurant_salestax"] objectAtIndex:button.tag]floatValue ])/100);
        [Flurry logEvent:@"Deduct_Quantity_Dish"];
    }
    
    lbPriceSub.text = [NSString stringWithFormat:@"$ %.02f", subtotal];
    lbPricetax.text = [NSString stringWithFormat:@"$ %.02f", tax];
    lbPriceGrand.text = [NSString stringWithFormat:@"$ %.02f", subtotal + tax];
    
}

#pragma mark - Order

- (IBAction)placeOrder:(id)sender {

    if(ind == nil)
    {
        UIAlertView * alert = [[UIAlertView alloc] initWithTitle:@"Aw snap!"
                                                         message:@"Seems like you didn't select a payment method"
                                                        delegate:nil
                                               cancelButtonTitle:@"Ok"
                                               otherButtonTitles:nil];
        [alert show];

    }
    else
    {
        self.btPlaceOrder.enabled = NO;
        [self performSelectorInBackground:@selector(startAnimation) withObject:self];

        NSString *response = [Requests placaOrderForCart:cart token:@"" orCard:[[listCard valueForKey:@"id"]objectAtIndex:ind.row] amount:[cart count] andAddressIndex:self.addressIndex priceTotal:grandTotal subtotal:subtotal];
        if(![response isEqualToString:@"error"])
        {
            [Requests stopAnimationInView];
            
            PopUpViewController *popUp = [[PopUpViewController alloc] init];
            [popUp showInView:self.view];
            popUp.lbText.text =[NSString stringWithFormat:@"Your order #%@ is carefully being prepared. We sent a confirmation to %@", response, [Customer getEmail]];
                      
            NSUserDefaults *datos = [NSUserDefaults standardUserDefaults];
            NSDate *time = [[NSDate alloc] init];
            [datos setObject:time forKey:@"placeOrderDate"];
            [datos removeObjectForKey:@"cart"];
            
            [Cart removeAll];
            [datos synchronize];
            
            [Flurry logEvent:@"Place_Order"];
            
            UITapGestureRecognizer *close = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(closePopUp)];
            [popUp.view addGestureRecognizer:close];
            
            [self performSelector:@selector(closePopUp) withObject:nil afterDelay:4.0];
        }
        else
        {
            [Requests stopAnimationInView];
            UIAlertView * alert = [[UIAlertView alloc] initWithTitle:@"Something went wrong"
                                                             message:@"We couldn't process your purchase"
                                                            delegate:nil
                                                   cancelButtonTitle:@"Ok"
                                                   otherButtonTitles:nil];
            [alert show];
            self.btPlaceOrder.enabled = YES;
        }
    }
}

- (IBAction)cancelOrder:(id)sender {
    
    HomeViewController *home = [[HomeViewController alloc] init];
    home.cancelOrder = YES;
    home.haveAlreadyReceivedCoordinates = YES;
    [self.navigationController pushViewController:home animated:NO];
}

- (IBAction)editPaymentMethod:(id)sender {
    
    PaymentViewController *payment = [[PaymentViewController alloc] init];
    payment.addressIndex = self.addressIndex;
    
    if(![Customer isGetListCards])
    {
        [self performSelectorInBackground:@selector(startAnimation) withObject:self];
    }
    
    [self.navBar pushTransition:self];
    [self.navigationController pushViewController:payment animated:NO];
    
}

-(void)closePopUp
{
    UIApplicationState state = [[UIApplication sharedApplication] applicationState];
    
    if(state != UIApplicationStateBackground & state != UIApplicationStateInactive && ![self.navigationController.visibleViewController isKindOfClass:[HomeViewController class]])
    {       
        [self back];
    }
    
}

#pragma mark -

-(void)back
{
    HomeViewController *home = [[HomeViewController alloc] init];
    home.haveAlreadyReceivedCoordinates = YES;
    [self.navBar pushTransition:self];
    
    [self.navigationController pushViewController:home animated:NO];
}

//Rounded Border Buttom
- (void)setRoundedBorder:(float)radius borderWidth:(float)borderWidth color:(UIColor*)color forButton:(UIButton *)button
{
    CALayer * l = [button layer];
    [l setMasksToBounds:YES];
    [l setCornerRadius:radius];
    // You can even add a border
    [l setBorderWidth:borderWidth];
    [l setBorderColor:[color CGColor]];
}

-(void)startAnimation
{
    [Requests startAnimationInView:self];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}



/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
