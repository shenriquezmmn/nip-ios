//
//  UpdateAddressViewController.m
//  NIP
//
//  Created by MacCMS2 on 20/5/15.
//  Copyright (c) 2015 MacCMS2. All rights reserved.
//

#import "UpdateAddressViewController.h"
#import "AddressesViewController.h"
#import "Requests.h"
#import "Customer.h"
@interface UpdateAddressViewController ()

@end

@implementation UpdateAddressViewController
@synthesize lbAddresLabel,lbCity,lbEdit,lbPhoneNumber,lbState,lbStreet,lbUnit,lbZipCode,txZipCode,txAddresLabel,txCity,txPhoneNumber,txState,txStreet,txUnit;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    //Header
    self.navBar = [[NavBarViewController alloc] init];
    [self.navBar showInView:self withTitle:@"Addresses" hiddenButtom:NO];
    self.navigationController.navigationBarHidden = YES;
    self.navBar.imgAddress.hidden = YES;
    self.navBar.horizontalConstraint.constant = 0;
    [self.navBar.imgAddress removeFromSuperview];

   
    lbEdit.textColor = TITLE_COLOR;
    lbAddresLabel.textColor = LABEL_COLOR;
    lbCity.textColor = LABEL_COLOR;
    lbPhoneNumber.textColor = LABEL_COLOR;
    lbState.textColor = LABEL_COLOR;
    lbStreet.textColor = LABEL_COLOR;
    lbUnit.textColor = LABEL_COLOR;
    lbZipCode.textColor = LABEL_COLOR;
    
    txAddresLabel.textColor = TITLE_COLOR;
    txCity.textColor = TITLE_COLOR;
    txPhoneNumber.textColor = TITLE_COLOR;
    txState.textColor = TITLE_COLOR;
    txStreet.textColor = TITLE_COLOR;
    txUnit.textColor = TITLE_COLOR;
    txZipCode.textColor = TITLE_COLOR;

    txState.text = [self.address valueForKey:@"customer_state"];
    txStreet.text = [self.address valueForKey:@"customer_street"];
    txUnit.text = [self.address valueForKey:@"customer_apartment_name"];
    txCity.text = [self.address valueForKey:@"customer_city"];
    txZipCode.text = [self.address valueForKey:@"customer_zip"];
    txAddresLabel.text = [self.address valueForKey:@"customer_address_title"];
    txPhoneNumber.text = [self.address valueForKey:@"customer_address_phone_number"];
    
    UITapGestureRecognizer *close = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(closeKeyboard)];
    [self.viewEdit addGestureRecognizer:close];


}

#pragma mark - Update Address

- (IBAction)updateAddress:(id)sender {
   
    NSLog(@"%lu", (unsigned long)[txPhoneNumber.text length]);
    if([txZipCode.text isEqualToString:@""] || [txStreet.text isEqualToString:@""] || [txUnit.text isEqualToString:@""] || [txCity.text isEqualToString:@""] || [txState.text isEqualToString:@""] || [txAddresLabel.text isEqualToString:@""]  || [txPhoneNumber.text isEqualToString:@""])
    {
        
        UIAlertView * alert = [[UIAlertView alloc] initWithTitle:@"Whoops"
                                                         message:@"Seems like some fields are missing"
                                                        delegate:nil
                                               cancelButtonTitle:@"Ok"
                                               otherButtonTitles:nil];
        [alert show];
        
    }
    else if ([txZipCode.text length]< LENGTH_ZIPCODE)
    {
        UIAlertView * alert = [[UIAlertView alloc] initWithTitle:@"Could you check again? "
                                                         message:[NSString stringWithFormat:@"Your Zip Code should be %d digits long", LENGTH_ZIPCODE]
                                                        delegate:nil
                                               cancelButtonTitle:@"Ok"
                                               otherButtonTitles:nil];
        [alert show];
        
    }
    else if ([txPhoneNumber.text length]< LENGTH_PHONE)
    {
        UIAlertView * alert = [[UIAlertView alloc] initWithTitle:@"Could you check again? "
                                                         message:@"Your phone number should be 10 digits long"
                                                        delegate:nil
                                               cancelButtonTitle:@"Ok"
                                               otherButtonTitles:nil];
        [alert show];
        
    }
    else
    {
        
        [self performSelectorInBackground:@selector(startAnimation) withObject:self];
      
        NSString *success = [Requests updateAddressForCustomer:[Customer getIdCustomer] addressId:[self.address valueForKey:@"id"] deliveryZip:txZipCode.text deliveryStreet:txStreet.text deliveryUnit:txUnit.text deliveryCity:txCity.text deliveryState:txState.text placeAlias:txAddresLabel.text phoneNumber:txPhoneNumber.text];
            
            if([success isEqual:@"1"])
            {
                [Requests getAddressForCustomer:[Customer getIdCustomer]];
                [self back];
            }
            else
            {
                [Requests stopAnimationInView];
                UIAlertView * alert = [[UIAlertView alloc] initWithTitle:@"Something went wrong"
                                                                 message:@"We couldn't update your address"
                                                                delegate:nil
                                                       cancelButtonTitle:@"Ok"
                                                       otherButtonTitles:nil];
                [alert show];
            }
    }
}

#pragma mark - TextField

- (IBAction)textBegin:(UITextField *)sender {
    
    if([[UIScreen mainScreen] bounds].size.height == 480)
    {
        [self.scrollView setContentSize:CGSizeMake([[UIScreen mainScreen] bounds].size.width, 700)];
        [self.scrollView setContentOffset:CGPointMake(0,sender.frame.origin.y-120) animated:NO];
    }
    else if([[UIScreen mainScreen] bounds].size.height == 568)
    {
        [self.scrollView setContentSize:CGSizeMake([[UIScreen mainScreen] bounds].size.width, 650)];
        
        if (sender == txPhoneNumber || sender == txAddresLabel ) {
            [self.scrollView setContentOffset:CGPointMake(0,sender.frame.origin.y-200) animated:NO];
            
        }
    }
    else
    {
        [self.scrollView setContentSize:CGSizeMake([[UIScreen mainScreen] bounds].size.width, 650)];
    }
}

- (IBAction)textEnd:(UITextField *)sender {
    
    [self.scrollView setContentSize:CGSizeMake([[UIScreen mainScreen] bounds].size.width, self.viewEdit.frame.size.height)];
   
}

-(void)closeKeyboard
{
    [self.view endEditing:YES];
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    NSString* totalString = [NSString stringWithFormat:@"%@%@",textField.text,string];
    
    // if it's the phone number textfield format it.
    if(textField == txPhoneNumber ) {
        if (range.length == 1) {
            // Delete button was hit.. so tell the method to delete the last char.
            textField.text = [self formatPhoneNumber:totalString deleteLastChar:YES];
        } else {
            textField.text = [self formatPhoneNumber:totalString deleteLastChar:NO ];
        }
        return false;
    }
    
    return YES;
}

-(NSString*) formatPhoneNumber:(NSString*) simpleNumber deleteLastChar:(BOOL)deleteLastChar {
    if(simpleNumber.length==0) return @"";
    // use regex to remove non-digits(including spaces) so we are left with just the numbers
    NSError *error = NULL;
    NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:@"[\\s-\\(\\)]" options:NSRegularExpressionCaseInsensitive error:&error];
    simpleNumber = [regex stringByReplacingMatchesInString:simpleNumber options:0 range:NSMakeRange(0, [simpleNumber length]) withTemplate:@""];
    
    // check if the number is to long
    if(simpleNumber.length>10) {
        // remove last extra chars.
        simpleNumber = [simpleNumber substringToIndex:10];
    }
    
    if(deleteLastChar) {
        // should we delete the last digit?
        simpleNumber = [simpleNumber substringToIndex:[simpleNumber length] - 1];
    }
    
    // 123 456 7890
    // format the number.. if it's less then 7 digits.. then use this regex.
    if(simpleNumber.length<7)
        simpleNumber = [simpleNumber stringByReplacingOccurrencesOfString:@"(\\d{3})(\\d+)"
                                                               withString:@"($1) $2"
                                                                  options:NSRegularExpressionSearch
                                                                    range:NSMakeRange(0, [simpleNumber length])];
    
    else   // else do this one..
        simpleNumber = [simpleNumber stringByReplacingOccurrencesOfString:@"(\\d{3})(\\d{3})(\\d+)"
                                                               withString:@"($1) $2-$3"
                                                                  options:NSRegularExpressionSearch
                                                                    range:NSMakeRange(0, [simpleNumber length])];
    return simpleNumber;
}

#pragma mark -

-(void)back
{
    AddressesViewController *addresses = [[AddressesViewController alloc] init];
    [self.navBar pushTransition:self];
    
    [self.navigationController pushViewController:addresses animated:NO];
}

-(void)startAnimation
{
    [Requests startAnimationInView:self];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
