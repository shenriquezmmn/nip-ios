//
//  PaymentMethodTableViewCell.m
//  nip
//
//  Created by MacCMS2 on 14/05/15.
//  Copyright (c) 2015 MacCMS2. All rights reserved.
//

#import "PaymentMethodTableViewCell.h"

@implementation PaymentMethodTableViewCell
@synthesize lbDate,lbEndingIn,imageView;

- (void)awakeFromNib {
    // Initialization code
    lbDate.textColor = LABEL_COLOR;
    lbEndingIn.textColor = TITLE_COLOR;
    self.imgEdit.hidden = YES;
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}

@end
