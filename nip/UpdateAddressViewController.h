//
//  UpdateAddressViewController.h
//  NIP
//
//  Created by MacCMS2 on 20/5/15.
//  Copyright (c) 2015 MacCMS2. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "VisibleFormViewController.h"

@interface UpdateAddressViewController : UIViewController

//Header
@property (strong, nonatomic) NavBarViewController *navBar;

@property (weak, nonatomic) IBOutlet UILabel *lbEdit;

@property (weak, nonatomic) NSArray *address;

@property (weak, nonatomic) IBOutlet UILabel *lbStreet;
@property (weak, nonatomic) IBOutlet UILabel *lbUnit;
@property (weak, nonatomic) IBOutlet UILabel *lbCity;
@property (weak, nonatomic) IBOutlet UILabel *lbState;
@property (weak, nonatomic) IBOutlet UILabel *lbAddresLabel;
@property (weak, nonatomic) IBOutlet UILabel *lbPhoneNumber;
@property (weak, nonatomic) IBOutlet UILabel *lbZipCode;
@property (weak, nonatomic) IBOutlet UIView *viewEdit;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;


@property (weak, nonatomic) IBOutlet UITextField *txStreet;
@property (weak, nonatomic) IBOutlet UITextField *txUnit;
@property (weak, nonatomic) IBOutlet UITextField *txCity;
@property (weak, nonatomic) IBOutlet UITextField *txState;
@property (weak, nonatomic) IBOutlet UITextField *txAddresLabel;
@property (weak, nonatomic) IBOutlet UITextField *txPhoneNumber;
@property (weak, nonatomic) IBOutlet UITextField *txZipCode;

@end
