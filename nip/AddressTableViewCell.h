//
//  AddressTableViewCell.h
//  NIP
//
//  Created by MacCMS2 on 18/05/15.
//  Copyright (c) 2015 MacCMS2. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AddressTableViewCell : UITableViewCell

@property (weak, nonatomic) IBOutlet UILabel *lbAddress;
@property (weak, nonatomic) IBOutlet UILabel *lbCity;
@property (weak, nonatomic) IBOutlet UIImageView *imgLabel;
@property (weak, nonatomic) IBOutlet UIButton *btEdit;
@property (weak, nonatomic) IBOutlet UIImageView *imgEdit;

@end
