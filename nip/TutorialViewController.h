//
//  TutorialViewController.h
//  NIP
//
//  Created by MacCMS2 on 10/8/15.
//  Copyright (c) 2015 MacCMS2. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface TutorialViewController : UIViewController
@property (weak, nonatomic) IBOutlet UIImageView *imgTutorial;

@end
