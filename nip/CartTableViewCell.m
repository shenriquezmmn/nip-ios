//
//  CartTableViewCell.m
//  nip
//
//  Created by MacCMS2 on 06/05/15.
//  Copyright (c) 2015 MacCMS2. All rights reserved.
//

#import "CartTableViewCell.h"
#import "CartViewController.h"

@implementation CartTableViewCell
@synthesize imgMenu, lbPriceMenu, lbMenuName, lbRestaurantName, lbNumber, lbQuantity, cont;

- (void)awakeFromNib {
    // Initialization code
  
    lbRestaurantName.textColor = TITLE_COLOR;
    lbPriceMenu.textColor = TITLE_COLOR;
    lbQuantity.textColor = LABEL_COLOR;
    lbNumber.textColor = TITLE_COLOR;
    lbMenuName.textColor = LABEL_COLOR;
    
    //Rounded Border image menu
    imgMenu.layer.masksToBounds = YES;
    imgMenu.layer.cornerRadius = 40.0;
    imgMenu.layer.rasterizationScale = [UIScreen mainScreen].scale;
    imgMenu.layer.shouldRasterize = YES;
    imgMenu.clipsToBounds = YES;
   
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}


- (IBAction)quantityMore:(id)sender {
    cont++;
    lbNumber.text = [NSString stringWithFormat:@"%i",cont];
  
    [CartViewController updatePrice:sender operation:@"+" quantity:[NSString stringWithFormat:@"%i", cont]];
}

- (IBAction)quantityLess:(id)sender {
    if(cont == 1)
    {
        lbNumber.text = [NSString stringWithFormat:@"%i", cont];

    }
    else
    {
        cont--;
        lbNumber.text = [NSString stringWithFormat:@"%i",cont];
        [CartViewController updatePrice:sender operation:@"-" quantity:[NSString stringWithFormat:@"%i", cont]];
    }
    
}


@end
