//
//  ParseJson.h
//  nip
//
//  Created by MacCMS2 on 24/04/15.
//  Copyright (c) 2015 MacCMS2. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface ParseJson : NSObject

+(id)parseJsonData:(NSData *)data;
@end
