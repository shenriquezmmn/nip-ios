//
//  CartViewController.h
//  nip
//
//  Created by MacCMS2 on 05/05/15.
//  Copyright (c) 2015 MacCMS2. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "STPCard.h"
#import "PTKView.h"


@interface CartViewController : UIViewController<UITableViewDataSource, UITableViewDelegate>

//Stripe
@property (strong, nonatomic) STPCard* stripeCard;

//Header
@property (strong, nonatomic) NavBarViewController *navBar;

//Add menu
@property (strong, nonatomic) NSArray *addMenu;

//Address ship
@property ( nonatomic) int addressIndex;

//Table View
@property (weak, nonatomic) IBOutlet UITableView *tableView;

//Cells
@property (strong, nonatomic) IBOutlet UITableViewCell *cellOrderSumery;
@property (strong, nonatomic) IBOutlet UITableViewCell *cellShipTo;
@property (strong, nonatomic) IBOutlet UITableViewCell *cellPaymentMethod;

//Labels Order Sumary
@property (weak, nonatomic) IBOutlet UILabel *lbOrderSumary;
@property (weak, nonatomic) IBOutlet UILabel *lbSubtotal;
@property (weak, nonatomic) IBOutlet UILabel *lbTax;
@property (weak, nonatomic) IBOutlet UILabel *lbGrandTotal;
@property (weak, nonatomic) IBOutlet UILabel *lbPriceSubtotal;
@property (weak, nonatomic) IBOutlet UILabel *lbPriceTax;
@property (weak, nonatomic) IBOutlet UILabel *lbPriceGrandTotal;
@property (weak, nonatomic) IBOutlet UILabel *lbDeliveryFee;
@property (weak, nonatomic) IBOutlet UILabel *lbFree;



//Labels Ship to
@property (weak, nonatomic) IBOutlet UILabel *lbShipTo;
@property (weak, nonatomic) IBOutlet UILabel *lbAddress;

//Labels Payment Method
@property (weak, nonatomic) IBOutlet UILabel *lbPaymentMethod;
@property (weak, nonatomic) IBOutlet UILabel *lbMethod;
@property (weak, nonatomic) IBOutlet UIImageView *imgCardType;

//Buttoms
@property (weak, nonatomic) IBOutlet UIButton *btAddAnother;
@property (weak, nonatomic) IBOutlet UIButton *btPlaceOrder;
@property (weak, nonatomic) IBOutlet UIButton *btEditPaymentMethod;
@property (weak, nonatomic) IBOutlet UIButton *btCancelOrder;

+(void)updatePrice:(UIButton *)button operation : (NSString *) simbol quantity : (NSString *) quantity;



@end
