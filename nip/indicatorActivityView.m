//
//  indicadorActividadView.m
//  BancaMobilIphone
//
//  Created by Medianet on 16/10/14.
//  Copyright (c) 2014 com.provincial.bancamovil2. All rights reserved.
//

#import "indicatorActivityView.h"

@implementation indicatorActivityView

-(UIActivityIndicatorView *)activity
{
    return activityIndicator;
}
-(id)init
{
    self = [super init];
    if (self) {
        
        [self setBackgroundColor:COLOR_INDICADOR_ACT];
        [self setAlpha:TRANSPARENCIA_INDICADOR_ACT];
        [self setFrame:[[UIScreen mainScreen] bounds]];
       
        
        activityIndicator = [[UIActivityIndicatorView alloc]initWithActivityIndicatorStyle:UIActivityIndicatorViewStyleWhiteLarge];
            
            [activityIndicator setCenter:[self center]];
            [self addSubview:activityIndicator];
            [activityIndicator startAnimating];
        
   
    }
    return self;

}






@end
