//
//  Cart.m
//  nip
//
//  Created by MacCMS2 on 05/05/15.
//  Copyright (c) 2015 MacCMS2. All rights reserved.
//

#import "Cart.h"

@implementation Cart

NSMutableArray *cart;

+(NSArray *)getCart
{
    if(cart)
    {
        return cart;
    }
    
    return nil;
    
}

+(void)setCart:(NSMutableArray*) cartData
{
    if(cart)
    {
        return;
    }

    cart = [[NSMutableArray alloc] initWithArray:cartData];
}

+(void)addDishToCart : (NSMutableDictionary *) dish
{
    if(cart)
    {
        [cart addObject:dish];
            
    }
    else
    {
        cart = [[NSMutableArray alloc] init];
        
        [cart addObject:dish];
        
    }
}
+(void)removeAll
{
    [cart removeAllObjects];
}
@end
