//
//  AppDelegate.m
//  nip
//
//  Created by MacCMS2 on 24/04/15.
//  Copyright (c) 2015 MacCMS2. All rights reserved.
//

#import "AppDelegate.h"
#import "LoginViewController.h"
#import "DeliveryZipViewController.h"
#import "HomeViewController.h"
#import <FacebookSDK/FacebookSDK.h>
#import "Requests.h"
#import "Customer.h"
#import "Cart.h"
#import "CartViewController.h"
#import "SDWebImage/UIImageView+WebCache.h"
#import "HCSStarRatingView.h"
#import "Flurry.h"
#import "TutorialViewController.h"
#import "Reachability.h"


@interface AppDelegate ()

@end

@implementation AppDelegate

NSArray *menu;
NSArray *address;
UIImage *image;


- (BOOL)application:(UIApplication *)application didFinishLaunchingWithOptions:(NSDictionary *)launchOptions {
    // Override point for customization after application launch.
    
    [Flurry setCrashReportingEnabled:YES];
    [Flurry startSession:@"TG8Y9RKXVWWSCBNR96JD"];
    
    [FBLoginView class];
    [FBProfilePictureView class];
    
    self.window = [[UIWindow alloc] initWithFrame:[[UIScreen mainScreen] bounds]];
    self.window.backgroundColor = [UIColor whiteColor];
    [self.window makeKeyAndVisible];
    
    UIGraphicsBeginImageContext( self.window.frame.size);
    if(self.window.frame.size.height == 480)
    {
        [[UIImage imageNamed:@"Splash640"] drawInRect: self.window.bounds];
    }
    else
    {
         [[UIImage imageNamed:@"Splash"] drawInRect: self.window.bounds];
    }   
    UIImage *image = UIGraphicsGetImageFromCurrentImageContext();
    UIGraphicsEndImageContext();
    
    if (![Requests isNetworkAvailable]) {
        
       self.window.backgroundColor = [UIColor colorWithPatternImage:image];
        
        UIAlertView * alert = [[UIAlertView alloc] initWithTitle:@"We missed you!"
                                                         message:@"Check yor internet conection and try again"
                                                        delegate:self
                                               cancelButtonTitle:@"Ok"
                                               otherButtonTitles:nil];
        [alert show];
        
      
    }
    else
    {
        self.loginViewController = [[LoginViewController alloc] init];
        self.deliveryZipViewController = [[DeliveryZipViewController alloc] init];
        
        [[UIApplication sharedApplication] setStatusBarStyle:UIStatusBarStyleLightContent];
        
        NSUserDefaults *datos = [NSUserDefaults standardUserDefaults];
        NSString *first_name = [datos stringForKey:@"first_name"];
        
        if(first_name != nil && ![first_name isEqualToString:@""])
        {
            NSString *photoId = [datos stringForKey:@"id"];
            NSString *last_name = [datos stringForKey:@"last_name"];
            NSString *email = [datos stringForKey:@"email"];
            
            //Login
            if([Requests loginForUser:email firtsName:first_name lastName:last_name photo:photoId])
            {
                //Address
                NSString *res = [Requests getAddressForCustomer:[Customer getIdCustomer]];
                if ([res isEqual:@"1"])
                {
                    NSMutableArray *tempArrayMemory = [[datos objectForKey:@"cart"] mutableCopy];
                    if([tempArrayMemory count] != 0 && tempArrayMemory != nil)
                    {
                        [Cart setCart:tempArrayMemory];
                    }
                   
                    [self isTimeCartExpired];
                    HomeViewController *home = [[HomeViewController alloc] init];
                    home.haveAlreadyReceivedCoordinates = NO;
                    home.logoTransparente.hidden = NO;
                    self.navController = [[UINavigationController alloc] initWithRootViewController:home];
                    
                }
                else if ([res isEqual:@"0"])
                {
                    self.navController = [[UINavigationController alloc] initWithRootViewController:[[DeliveryZipViewController alloc]init]];
                }
            }
            else
            {
                self.window.backgroundColor = [UIColor colorWithPatternImage:image];
                UIAlertView * alert = [[UIAlertView alloc] initWithTitle:@"We missed you!"
                                                                 message:@"Check yor internet conection and try again"
                                                                delegate:self
                                                       cancelButtonTitle:@"Ok"
                                                       otherButtonTitles:nil];
                [alert show];
            }
        }
        else
        {
            self.navController = [[UINavigationController alloc] initWithRootViewController:self.loginViewController];
        }
    }
    
    self.navController.navigationBarHidden = YES;
    [self.window addSubview:self.navController.view];

    
    return YES;
}

- (void)alertView:(UIAlertView *)alertView
clickedButtonAtIndex:(NSInteger)buttonIndex{
    exit(0);
}


- (BOOL)application:(UIApplication *)application
            openURL:(NSURL *)url
  sourceApplication:(NSString *)sourceApplication
         annotation:(id)annotation {
    // Facebook SDK * login flow *
    // Attempt to handle URLs to complete any auth (e.g., SSO) flow.
    return [FBAppCall handleOpenURL:url sourceApplication:sourceApplication];
}


- (void)applicationWillResignActive:(UIApplication *)application {
    // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
    // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
}

- (void)applicationDidEnterBackground:(UIApplication *)application {
    // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
    // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    
    NSUserDefaults *datos = [NSUserDefaults standardUserDefaults];
    if([datos objectForKey:@"cart"])
    {
        [datos removeObjectForKey:@"cart"];
    }
    
    [datos setObject:[Cart getCart] forKey:@"cart"];
    
    NSDate *date = [[NSDate alloc]init];
    
    [datos setObject:date forKey:@"timeBefore"];
    
}

- (void)applicationWillEnterForeground:(UIApplication *)application {
    // Called as part of the transition from the background to the inactive state; here you can undo many of the changes made on entering the background.
    
    if ([self isTimeCartExpired]) {
        HomeViewController *home = [[HomeViewController alloc] init];
        home.haveAlreadyReceivedCoordinates = NO;
        home.logoTransparente.hidden = NO;
        
        [self.navController pushViewController:home   animated:NO];
    }
    else
    {
        NSUserDefaults *datos = [NSUserDefaults standardUserDefaults];
        NSDate *timeBefore = nil;
        if([datos objectForKey:@"timeBefore"])
        {
            timeBefore = [datos objectForKey:@"timeBefore"];
        }
        
        NSDate *timeNow = [[NSDate alloc]init];
        
        if (timeBefore != nil && [Customer getCustomer] != nil) {
            NSTimeInterval distanceBetweenDates = [timeNow timeIntervalSinceDate:timeBefore];
            int time = distanceBetweenDates * 1;
            if(time >600)
            {
                HomeViewController *home = [[HomeViewController alloc] init];
                home.haveAlreadyReceivedCoordinates = NO;
                home.logoTransparente.hidden = NO;
                    
                [self.navController pushViewController:home    animated:NO];
            }
        }
    }
}

- (void)applicationDidBecomeActive:(UIApplication *)application {
    // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.

}


- (void)applicationWillTerminate:(UIApplication *)application {
    // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    
}

-(BOOL)isTimeCartExpired
{
    NSUserDefaults *datos = [NSUserDefaults standardUserDefaults];
    NSDate *timeBefore = nil;
    if([datos objectForKey:@"timeBefore"])
    {
        timeBefore = [datos objectForKey:@"timeBefore"];
    }
    
    NSDate *timeNow = [[NSDate alloc]init];
    
    if (timeBefore != nil && [Customer getCustomer] != nil) {
        NSTimeInterval distanceBetweenDates = [timeNow timeIntervalSinceDate:timeBefore];
        int time = distanceBetweenDates * 1;
        //If the user close the app for one day, the cart will expire
        
        if(time >86400 && [[Cart getCart] count] > 0)
        {
            UIAlertView * alert = [[UIAlertView alloc] initWithTitle:@"We missed you!"
                                                             message:@"Your cart has expired"
                                                            delegate:nil
                                                   cancelButtonTitle:@"Ok"
                                                   otherButtonTitles:nil];
            [alert show];
            [Cart removeAll];
            [datos removeObjectForKey:@"timeBefore"];
            [datos setObject:[Cart getCart] forKey:@"cart"];
            return YES;
        }
    }
    
    return NO;
}

@end
