//
//  AddressesViewController.m
//  NIP
//
//  Created by MacCMS2 on 18/05/15.
//  Copyright (c) 2015 MacCMS2. All rights reserved.
//

#import "AddressesViewController.h"
#import "Customer.h"
#import "AddressTableViewCell.h"
#import "HomeViewController.h"
#import "Requests.h"
#import "UpdateAddressViewController.h"
#import "Cart.h"

@interface AddressesViewController ()

@end

@implementation AddressesViewController
@synthesize lbAdd,lbAddresLabel,lbCity,lbPhoneNumber,lbSelect,lbState,lbStreet,lbUnit,lbZipCode,txAddresLabel,txCity,txPhoneNumber,txState,txStreet,txUnit,txZipCode;

NSMutableArray *listAddress;
NSString *addressId;
NSIndexPath *ind;


- (void)viewDidLoad {
    
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    //Header
    self.navBar = [[NavBarViewController alloc] init];
    [self.navBar showInView:self withTitle:@"Addresses" hiddenButtom:NO];
    self.navigationController.navigationBarHidden = YES;
    self.navBar.btAddress.enabled = NO;
    self.navBar.imgAddress.hidden = YES;
    self.navBar.horizontalConstraint.constant = 0;
    [self.navBar.imgAddress removeFromSuperview];
    
    lbSelect.textColor = TITLE_COLOR;
    lbAdd.textColor = TITLE_COLOR;
    lbAddresLabel.textColor = LABEL_COLOR;
    lbCity.textColor = LABEL_COLOR;
    lbPhoneNumber.textColor = LABEL_COLOR;
    lbState.textColor = LABEL_COLOR;
    lbStreet.textColor = LABEL_COLOR;
    lbUnit.textColor = LABEL_COLOR;
    lbZipCode.textColor = LABEL_COLOR;
    
    txAddresLabel.textColor = TITLE_COLOR;
    txCity.textColor = TITLE_COLOR;
    txPhoneNumber.textColor = TITLE_COLOR;
    txState.textColor = TITLE_COLOR;
    txStreet.textColor = TITLE_COLOR;
    txUnit.textColor = TITLE_COLOR;
    txZipCode.textColor = TITLE_COLOR;
    
    self.txZipCode.text = self.zipCode;
    if(self.numStreet != nil)
    {
         self.txStreet.text = [NSString stringWithFormat:@"%@ %@",self.numStreet, self.street];
    }
    else
    {
        self.txStreet.text = self.street;
    }
   
    self.txCity.text = self.city;
    self.txState.text = self.state;

    listAddress = [[NSMutableArray alloc] initWithArray:[Customer getAddresses]];
    
    UITapGestureRecognizer *close = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(closeKeyboard)];
    [self.viewAdd addGestureRecognizer:close];
    
    //If exist addressId on memory
    NSUserDefaults *datos = [NSUserDefaults standardUserDefaults];
    addressId = [datos stringForKey:@"addressId"];

}


- (void)viewDidAppear:(BOOL)animated
{
    [super viewDidAppear:animated];
    
    [self adjustHeightOfTableview];
   
    if(self.zipCode != nil)
    {
         NSLog(@"%f", self.tableView.frame.size.height);
        [self.scrollView setContentOffset:CGPointMake(0,[listAddress count] *80) animated:YES];
    }
}

#pragma mark - TextField

-(void)textFieldDidBeginEditing:(UITextField *)textField
{
    NSLog(@"%f", textField.frame.origin.y);
    NSLog(@"%f", self.tableView.frame.size.height);
    
    [self.scrollView setContentSize:CGSizeMake([[UIScreen mainScreen] bounds].size.width, self.tableView.frame.size.height+self.viewAdd.frame.size.height+350)];
    
    [self.scrollView setContentOffset:CGPointMake(0,self.tableView.frame.size.height+(textField.frame.origin.y/2)+25) animated:NO];
}

-(void)textFieldDidEndEditing:(UITextField *)textField
{
    [self.scrollView setContentSize:CGSizeMake([[UIScreen mainScreen] bounds].size.width, self.tableView.frame.size.height+self.viewAdd.frame.size.height+100)];
}

-(void)closeKeyboard
{
    [self.view endEditing:YES];
}

- (BOOL)textField:(UITextField *)textField shouldChangeCharactersInRange:(NSRange)range replacementString:(NSString *)string {
    NSString* totalString = [NSString stringWithFormat:@"%@%@",textField.text,string];
    
    // if it's the phone number textfield format it.
    if(textField == txPhoneNumber ) {
        if (range.length == 1) {
            // Delete button was hit.. so tell the method to delete the last char.
            textField.text = [self formatPhoneNumber:totalString deleteLastChar:YES];
        } else {
            textField.text = [self formatPhoneNumber:totalString deleteLastChar:NO ];
        }
        return false;
    }
    
    return YES; 
}

-(NSString*) formatPhoneNumber:(NSString*) simpleNumber deleteLastChar:(BOOL)deleteLastChar {
    if(simpleNumber.length==0) return @"";
    // use regex to remove non-digits(including spaces) so we are left with just the numbers
    NSError *error = NULL;
    NSRegularExpression *regex = [NSRegularExpression regularExpressionWithPattern:@"[\\s-\\(\\)]" options:NSRegularExpressionCaseInsensitive error:&error];
    simpleNumber = [regex stringByReplacingMatchesInString:simpleNumber options:0 range:NSMakeRange(0, [simpleNumber length]) withTemplate:@""];
    
    // check if the number is to long
    if(simpleNumber.length>10) {
        // remove last extra chars.
        simpleNumber = [simpleNumber substringToIndex:10];
    }
    
    if(deleteLastChar) {
        // should we delete the last digit?
        simpleNumber = [simpleNumber substringToIndex:[simpleNumber length] - 1];
    }
    
    // 123 456 7890
    // format the number.. if it's less then 7 digits.. then use this regex.
    if(simpleNumber.length<7)
        simpleNumber = [simpleNumber stringByReplacingOccurrencesOfString:@"(\\d{3})(\\d+)"
                                                               withString:@"($1) $2"
                                                                  options:NSRegularExpressionSearch
                                                                    range:NSMakeRange(0, [simpleNumber length])];
    
    else   // else do this one..
        simpleNumber = [simpleNumber stringByReplacingOccurrencesOfString:@"(\\d{3})(\\d{3})(\\d+)"
                                                               withString:@"($1) $2-$3"
                                                                  options:NSRegularExpressionSearch
                                                                    range:NSMakeRange(0, [simpleNumber length])];
    return simpleNumber;
}

#pragma mark - Table View


- (NSInteger)numberOfSectionsInTableView:(UITableView *)tableView
{
    return 1;
}

- (CGFloat)tableView:(UITableView *)tableView heightForRowAtIndexPath:(NSIndexPath *)indexPath
{
    return 80;
}


- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section
{
    return [listAddress count];
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath
{
    AddressTableViewCell *cell =  [tableView dequeueReusableCellWithIdentifier:@"myCellAddress"];
    
    if(!cell)
    {
        [tableView  registerNib:[UINib nibWithNibName:@"AddressTableViewCell" bundle:nil]forCellReuseIdentifier:@"myCellAddress"];
        cell =  [tableView  dequeueReusableCellWithIdentifier:@"myCellAddress"];
    }
    
    cell.lbAddress.text = [NSString stringWithFormat:@"\"%@\" %@ %@ %@ %@ %@ %@",[[listAddress valueForKey:@"customer_address_title"] objectAtIndex:indexPath.row], [[listAddress valueForKey:@"customer_apartment_name"] objectAtIndex:indexPath.row], [[listAddress valueForKey:@"customer_street"] objectAtIndex:indexPath.row],  [[listAddress valueForKey:@"customer_city"] objectAtIndex:indexPath.row], [[listAddress valueForKey:@"customer_state"] objectAtIndex:indexPath.row] , [[listAddress valueForKey:@"customer_zip"] objectAtIndex:indexPath.row],[[listAddress valueForKey:@"customer_address_phone_number"] objectAtIndex:indexPath.row]];
    
    cell.lbCity.text = [[listAddress valueForKey:@"customer_city"] objectAtIndex:indexPath.row];
    
    cell.btEdit.tag = indexPath.row;
    
    [cell.btEdit addTarget:self action:@selector(updateAddress:) forControlEvents:UIControlEventTouchUpInside];
    
    [cell.imgLabel setIsAccessibilityElement:YES];
    
    if([[[listAddress valueForKey:@"customer_address_title"] objectAtIndex:indexPath.row] caseInsensitiveCompare:@"home"] == NSOrderedSame || [[[listAddress valueForKey:@"customer_address_title"] objectAtIndex:indexPath.row] caseInsensitiveCompare:@"house"] == NSOrderedSame )
    {
        cell.imgLabel.image = [UIImage imageNamed:@"HomeGris.png"];
        [cell.imgLabel setAccessibilityIdentifier:@"home" ];
        
    }
    else if ([[[listAddress valueForKey:@"customer_address_title"] objectAtIndex:indexPath.row] caseInsensitiveCompare:@"office"] == NSOrderedSame  || [[[listAddress valueForKey:@"customer_address_title"] objectAtIndex:indexPath.row] caseInsensitiveCompare:@"work"]== NSOrderedSame )
    {
        cell.imgLabel.image = [UIImage imageNamed:@"OfficeGray.png"];
        [cell.imgLabel setAccessibilityIdentifier:@"work" ];
    }
    else
    {
        cell.imgLabel.image = [UIImage imageNamed:@"OtherGray.png"];
        [cell.imgLabel setAccessibilityIdentifier:@"other" ];
    }
    
    if(self.zipCode == nil)
    {
        
        if([addressId isEqualToString:[[listAddress valueForKey:@"id"] objectAtIndex:indexPath.row]])
        {
            [tableView
             selectRowAtIndexPath:indexPath
             animated:TRUE
             scrollPosition:UITableViewScrollPositionNone
             ];
            
            cell.imgEdit.image = [UIImage imageNamed:@"EditWhite"];
            if([[[listAddress valueForKey:@"customer_address_title"] objectAtIndex:indexPath.row] caseInsensitiveCompare:@"home"] == NSOrderedSame || [[[listAddress valueForKey:@"customer_address_title"] objectAtIndex:indexPath.row] caseInsensitiveCompare:@"house"] == NSOrderedSame )
            {
                cell.imgLabel.image = [UIImage imageNamed:@"Home.png"];
                [cell.imgLabel setAccessibilityIdentifier:@"home" ];
                
            }
            else if ([[[listAddress valueForKey:@"customer_address_title"] objectAtIndex:indexPath.row] caseInsensitiveCompare:@"office"] == NSOrderedSame  || [[[listAddress valueForKey:@"customer_address_title"] objectAtIndex:indexPath.row] caseInsensitiveCompare:@"work"]== NSOrderedSame )
            {
                cell.imgLabel.image = [UIImage imageNamed:@"OfficeWhite.png"];
                [cell.imgLabel setAccessibilityIdentifier:@"work" ];
            }
            else
            {
                cell.imgLabel.image = [UIImage imageNamed:@"OtherWhite.png"];
                [cell.imgLabel setAccessibilityIdentifier:@"other" ];
            }
            
            ind = indexPath;
        }
    }
    else
    {
        addressId = @"";
    }
   
    return cell;
    
}

-(void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath
{
    NSLog(@"%li", (long)indexPath.row);

    NSLog(@"%li", (long)ind.row);
    
   if(ind.row != indexPath.row)
   {
       //Deselect the row previous
       [tableView deselectRowAtIndexPath:ind animated:true];
       
       //Change images the row previous
       AddressTableViewCell *prevCell = ((AddressTableViewCell*)[tableView cellForRowAtIndexPath:ind]);
       prevCell.imgEdit.image = [UIImage imageNamed:@"Edit"];
        
       NSString *imgName = prevCell.imgLabel.accessibilityIdentifier;
       
       if([imgName isEqualToString:@"home"])
       {
           prevCell.imgLabel.image = [UIImage imageNamed:@"HomeGris.png"];
       }
       else if ([imgName isEqualToString:@"work"])
       {
           prevCell.imgLabel.image = [UIImage imageNamed:@"OfficeGray.png"];
       }
       else
       {
           prevCell.imgLabel.image = [UIImage imageNamed:@"OtherGray.png"];
       }
   }

    //Change images the row selected
    AddressTableViewCell *tmpCell = ((AddressTableViewCell*)[tableView cellForRowAtIndexPath:indexPath]);
    tmpCell.imgEdit.image = [UIImage imageNamed:@"EditWhite"];
    
    NSString *imgName = tmpCell.imgLabel.accessibilityIdentifier;
    
    if([imgName isEqualToString:@"home"])
    {
        tmpCell.imgLabel.image = [UIImage imageNamed:@"Home.png"];
    }
    else if ([imgName isEqualToString:@"work"])
    {
        tmpCell.imgLabel.image = [UIImage imageNamed:@"OfficeWhite.png"];
    }
    else
    {
        tmpCell.imgLabel.image = [UIImage imageNamed:@"OtherWhite.png"];
    }
    
    NSUserDefaults *datos = [NSUserDefaults standardUserDefaults];
    NSLog(@"%@", [datos objectForKey:@"addressId"]);
    if([datos objectForKey:@"addressId"] != nil)
    {
        if(![[datos objectForKey:@"addressId"] isEqualToString:[[listAddress valueForKey:@"id"] objectAtIndex:indexPath.row]])
        {
            [Cart removeAll];
        }
    }
    
    ind = indexPath;
    
    [datos setObject:[[listAddress valueForKey:@"id"] objectAtIndex:indexPath.row ]forKey:@"addressId"];
    [datos synchronize];
    
    addressId = [datos stringForKey:@"addressId"];
    
    [self performSelectorInBackground:@selector(startAnimation) withObject:self];
    
    [self performSelector:@selector(menuForZip:) withObject:[[listAddress valueForKey:@"customer_zip"]objectAtIndex:indexPath.row]afterDelay:0.1];
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    return YES;
}

- (void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath
{
    
}

-(NSArray *)tableView:(UITableView *)tableView editActionsForRowAtIndexPath:(NSIndexPath *)indexPath {
    
    addressId = @"";

    //Change images the row previous
    AddressTableViewCell *prevCell = ((AddressTableViewCell*)[tableView cellForRowAtIndexPath:ind]);
    prevCell.imgEdit.image = [UIImage imageNamed:@"Edit"];
  
    NSString *imgName = prevCell.imgLabel.accessibilityIdentifier;
    
    
    if([imgName isEqualToString:@"home"])
    {
        prevCell.imgLabel.image = [UIImage imageNamed:@"Home.png"];
    }
    else if ([imgName isEqualToString:@"work"])
    {
        prevCell.imgLabel.image = [UIImage imageNamed:@"OfficeWhite.png"];
    }
    else
    {
        prevCell.imgLabel.image = [UIImage imageNamed:@"OtherWhite.png"];
    }
    
    //Change images the row selected
    AddressTableViewCell *tmpCell = ((AddressTableViewCell*)[tableView cellForRowAtIndexPath:indexPath]);
    tmpCell.imgEdit.image = [UIImage imageNamed:@"Edit"];
    
    
    if([imgName isEqualToString:@"home"])
    {
        prevCell.imgLabel.image = [UIImage imageNamed:@"HomeGris.png"];
    }
    else if ([imgName isEqualToString:@"work"])
    {
        prevCell.imgLabel.image = [UIImage imageNamed:@"OfficeGray.png"];
    }
    else
    {
        prevCell.imgLabel.image = [UIImage imageNamed:@"OtherGray.png"];
    }

    
    UITableViewRowAction *button = [UITableViewRowAction rowActionWithStyle:UITableViewRowActionStyleDefault title:@"X" handler:^(UITableViewRowAction *action, NSIndexPath *indexPath)
    {
        
           if([listAddress count] <= 1)
           {
               UIAlertView * alert = [[UIAlertView alloc] initWithTitle:@"We're sorry!"
                                                 message:@"You should keep one address at least"
                                                   delegate:nil
                                                   cancelButtonTitle:@"Ok"
                                                  otherButtonTitles:nil];
                   [alert show];
           }
           else
           {
               [self performSelectorInBackground:@selector(startAnimation) withObject:self];
               NSString *response = [Requests deleteAddressWithId:[[listAddress valueForKey:@"id"]objectAtIndex:indexPath.row]];
                                            
               if([response isEqualToString:@"1"])
               {
                     [Requests stopAnimationInView];
                     [self performSelectorInBackground:@selector(updateTableAddresses) withObject:self];
                     [listAddress removeObjectAtIndex:indexPath.row];
                     [tableView deleteRowsAtIndexPaths:@[indexPath] withRowAnimation:UITableViewRowAnimationAutomatic];
                    [self performSelector:@selector(adjustHeightOfTableview) withObject:nil afterDelay:0.25];
               }
               else
               {
                   [Requests stopAnimationInView];
                   UIAlertView * alert = [[UIAlertView alloc] initWithTitle:@"Something went wrong"
                       message:@"We couldn't delete your address"
                       delegate:nil
                       cancelButtonTitle:@"Ok"
                       otherButtonTitles:nil];
                   [alert show];
                }

            }
                                        
    }];
    button.backgroundColor = [[UIColor blackColor]colorWithAlphaComponent:0.1];
    
    return @[button];
}

-(void)updateTableAddresses
{
    [Requests getAddressForCustomer:[Customer getIdCustomer]];
    [listAddress removeAllObjects];
    [listAddress addObjectsFromArray:[Customer getAddresses]];
    [self.tableView reloadData];
    [self performSelector:@selector(adjustHeightOfTableview) withObject:nil afterDelay:0.25];
}

- (void)adjustHeightOfTableview
{
    CGFloat height = [listAddress count] *80;
    CGFloat maxHeight = self.tableView.superview.frame.size.height - self.tableView.frame.origin.y;
    
    // if the height of the content is greater than the maxHeight of
    // total space on the screen, limit the height to the size of the
    // superview.
    
    if (height > maxHeight)
        height = maxHeight;
    
    // now set the height constraint accordingly
    
    [UIView animateWithDuration:0.25 animations:^{
        self.tableViewHeightConstraint.constant = height;
        [self.view setNeedsUpdateConstraints];
    }];
}


#pragma mark - Add and Update Address

- (IBAction)addAddress:(id)sender {
    
    if([txZipCode.text isEqualToString:@""] || [txStreet.text isEqualToString:@""] || [txUnit.text isEqualToString:@""] || [txCity.text isEqualToString:@""] || [txState.text isEqualToString:@""] || [txAddresLabel.text isEqualToString:@""]  || [txPhoneNumber.text isEqualToString:@""])
    {
        
        UIAlertView * alert = [[UIAlertView alloc] initWithTitle:@"Whoops"
                                                         message:@"Seems like some fields are missing"
                                                        delegate:nil
                                               cancelButtonTitle:@"Ok"
                                               otherButtonTitles:nil];
        [alert show];
        
    }
    else if ([txZipCode.text length]< LENGTH_ZIPCODE)
    {
        UIAlertView * alert = [[UIAlertView alloc] initWithTitle:@"Could you check again?"
                                                         message:[NSString stringWithFormat:@"Your Zip Code should be %d digits long", LENGTH_ZIPCODE]
                                                        delegate:nil
                                               cancelButtonTitle:@"Ok"
                                               otherButtonTitles:nil];
        [alert show];
        
    }
    else if ([txPhoneNumber.text length]< LENGTH_PHONE)
    {
        UIAlertView * alert = [[UIAlertView alloc] initWithTitle:@"Could you check again?"
                                                         message:@"Your phone number should be 10 digits long"
                                                        delegate:nil
                                               cancelButtonTitle:@"Ok"
                                               otherButtonTitles:nil];
        [alert show];
        
    }
    else
    {
        
        [self performSelectorInBackground:@selector(startAnimation) withObject:self];
        
        NSString *success = [Requests addAddressForCustomer:[Customer getIdCustomer]deliveryZip:txZipCode.text deliveryStreet:txStreet.text deliveryUnit:txUnit.text deliveryCity:txCity.text deliveryState:txState.text placeAlias:txAddresLabel.text phoneNumber:txPhoneNumber.text];
        
        if([success isEqual:@"1"])
        {
            [self updateTableAddresses];
            NSUserDefaults *datos = [NSUserDefaults standardUserDefaults];
            [datos setObject:[[listAddress lastObject] valueForKey:@"id"] forKey:@"addressId"];
            [datos synchronize];
            
            [self menuForZip:txZipCode.text];
            
            txZipCode.text = @"";
            txStreet.text = @"";
            txUnit.text = @"";
            txCity.text = @"";
            txState.text = @"";
            txAddresLabel.text = @"";
            txPhoneNumber.text = @"";
            
        }
        else
        {
            [Requests stopAnimationInView];
            UIAlertView * alert = [[UIAlertView alloc] initWithTitle:@"Something went wrong"
                                                             message:@"We couldn’t save your address"
                                                            delegate:nil
                                                   cancelButtonTitle:@"Ok"
                                                   otherButtonTitles:nil];
            [alert show];
            
            [Requests stopAnimationInView];
            
        }
    }
    
}

-(IBAction)updateAddress:(UIButton *)sender{
    UpdateAddressViewController *updateAddress = [[UpdateAddressViewController alloc] init];
    
    updateAddress.address = [listAddress objectAtIndex:sender.tag];
    
    [self.navBar pushTransition:self];
    
    [self.navigationController pushViewController:updateAddress animated:NO];
}


#pragma mark - Back

-(void)back
{
    for (int i=0; i< [listAddress count]; i++)
    {
        if([[[listAddress valueForKey:@"id"] objectAtIndex:i] isEqualToString:addressId])
        {
            [self performSelectorInBackground:@selector(startAnimation) withObject:self];
            [self menuForZip:[[listAddress valueForKey:@"customer_zip"]objectAtIndex:i]];
            return;
        }
    }
    
    UIAlertView * alert = [[UIAlertView alloc] initWithTitle:@"Whoops"
                                                     message:@"We need to know where to deliver your food"
                                                    delegate:nil
                                           cancelButtonTitle:@"Ok"
                                           otherButtonTitles:nil];
    [alert show];
    
}


-(void)goToHome
{
    HomeViewController *home = [[HomeViewController alloc] init];
      home.haveAlreadyReceivedCoordinates = YES;
    [self.navBar pushTransition:self];
    
    [self.navigationController pushViewController:home animated:NO];

}

-(void)menuForZip:(NSString *)zipcode
{
    NSString *response = [Requests menuForZipCode:zipcode];
    if([response isEqualToString:@"-1"])
    {
        [Requests stopAnimationInView];
        UIAlertView * alert = [[UIAlertView alloc] initWithTitle:@"We’re sorry!"
                                                         message:@"Our awesome service is not available in this area. Please select another address"
                                                        delegate:nil
                                               cancelButtonTitle:@"Ok"
                                               otherButtonTitles:nil];
        [alert show];
    }
    else if ([response isEqualToString:@"-2"])
    {
        [Requests stopAnimationInView];
        UIAlertView * alert = [[UIAlertView alloc] initWithTitle:@"We missed you!"
                                                         message:@"Check yor internet conection and try again"
                                                        delegate:nil
                                               cancelButtonTitle:@"Ok"
                                               otherButtonTitles:nil];
        [alert show];
    }
    else
    {
        [self performSelector:@selector(goToHome) withObject:nil afterDelay:0.0];
    }
}

#pragma mark -

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


-(void)startAnimation
{
    [Requests startAnimationInView:self];
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
