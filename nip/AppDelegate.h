//
//  AppDelegate.h
//  nip
//
//  Created by MacCMS2 on 24/04/15.
//  Copyright (c) 2015 MacCMS2. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "LoginViewController.h"
#import "DeliveryZipViewController.h"
#import "HCSStarRatingView.h"

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;
@property (strong, nonatomic) LoginViewController *loginViewController;
@property (strong, nonatomic) DeliveryZipViewController *deliveryZipViewController;
@property (strong, nonatomic) UINavigationController *navController;
@property (strong, nonatomic) HCSStarRatingView *starRatingView;

@end

