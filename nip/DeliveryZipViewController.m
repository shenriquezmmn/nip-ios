//
//  DeliveryZipViewController.m
//  nip
//
//  Created by MacCMS2 on 24/04/15.
//  Copyright (c) 2015 MacCMS2. All rights reserved.
//

#import "DeliveryZipViewController.h"
#import "Requests.h"
#import "DeliveryZipDetailsViewController.h"
#import <CoreLocation/CoreLocation.h>
#import <CoreLocation/CLLocationManagerDelegate.h>

#define IS_OS_8_OR_LATER ([[[UIDevice currentDevice] systemVersion] floatValue] >= 8.0)

@interface DeliveryZipViewController ()

@end

@implementation DeliveryZipViewController
{
    CLLocationManager *locationManager;
}

DeliveryZipDetailsViewController *deliveryDetails;


- (void)viewDidLoad {
    [super viewDidLoad];
    
    //Header
    self.navBar = [[NavBarViewController alloc] init];
    self.navigationController.navigationBarHidden = YES;
    
    //Instantiate a location object.
    locationManager = [[CLLocationManager alloc] init];
    locationManager.delegate = self;
    locationManager.distanceFilter = kCLDistanceFilterNone;
    locationManager.desiredAccuracy = kCLLocationAccuracyBest;

    // Do any additional setup after loading the view from its nib.
   
    //SET THE LAST VIEW WHICH THE KEYBOARD WILL NOT HIDE
    self.lastVisibleView = self.txZipCode;
    self.visibleMargin = 15.;
    
    deliveryDetails = [[DeliveryZipDetailsViewController alloc] init];
    
    
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event
{
    [super touchesBegan:touches withEvent:event];
    [self.view endEditing:YES];
    
}

-(void)startAnimation
{
    [Requests startAnimationInView:self];
}

- (IBAction)goToDetails:(id)sender {
    
    if([self.txZipCode.text isEqualToString:@""])
    {
        UIAlertView * alert = [[UIAlertView alloc] initWithTitle:@"Whoops!"
                                                         message:@"Please enter your Zip Code to get the best food"
                                                        delegate:nil
                                               cancelButtonTitle:@"Ok"
                                               otherButtonTitles:nil];
        [alert show];

        
    }
    else
    {
        deliveryDetails.zipCode = self.txZipCode.text;
        
        [self.navBar pushTransition:self];
        
        [self.navigationController pushViewController:deliveryDetails animated:NO];
    }  

}
#pragma mark - GPS Location

- (IBAction)findLocation:(id)sender {    
    
#ifdef __IPHONE_8_0
    if(IS_OS_8_OR_LATER) {
        // Use one or the other, not both. Depending on what you put in info.plist
        [locationManager requestWhenInUseAuthorization];
      //  [locationManager requestAlwaysAuthorization];
    }
#endif
       [locationManager startUpdatingLocation];

}

-(void)locationManager:(CLLocationManager *)manager didUpdateLocations:(NSArray *)locations
{
    [self performSelectorInBackground:@selector(startAnimation) withObject:self];

    [locationManager stopUpdatingLocation];
    [self reverseGeocode:locationManager.location];
}

- (void)reverseGeocode:(CLLocation *)location {
    
    CLGeocoder *geocoder = [[CLGeocoder alloc] init];
    [geocoder reverseGeocodeLocation:location completionHandler:^(NSArray *placemarks, NSError *error) {
        NSLog(@"Finding address");
        if (error) {
            NSLog(@"Error %@", error.description);
        } else {
            CLPlacemark *placemark = [placemarks lastObject];
           
            deliveryDetails.zipCode = placemark.postalCode;
            deliveryDetails.street = placemark.thoroughfare;
            deliveryDetails.numStreet = placemark.subThoroughfare;
            deliveryDetails.city = placemark.locality;
            deliveryDetails.state = placemark.administrativeArea;        
            
            [self.navBar pushTransition:self];
            
            [self.navigationController pushViewController:deliveryDetails animated:NO];

        }
     }];
    
}

/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
