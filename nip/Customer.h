//
//  Customer.h
//  nip
//
//  Created by MacCMS2 on 24/04/15.
//  Copyright (c) 2015 MacCMS2. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Customer : NSObject

+(void)setCustomer : (NSDictionary *) dataCustomer;
+(NSDictionary *)getCustomer;
+(NSString *)getIdCustomer;
+(NSString *)getNameCustomer;
+(NSString *)getIdPhotoCustomer;
+(void)setAddresses: (NSMutableArray *) dataAddresss;
+(NSMutableArray *)getAddresses;
+(NSString *)getEmail;
+(NSString *)getFirsttName;
+(NSString *)getLastName;
+(void)setListCards: (NSMutableArray *) cards;
+(NSMutableArray *)getListCards;
+(void)removeAll;
+(bool)isGetListCards;
+(void)setIsGetAddresses: (bool) getAddresses;
+(bool)IsGetAddresses;

@end
