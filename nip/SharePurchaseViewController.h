//
//  SharePurchaseViewController.h
//  NIP
//
//  Created by MacCMS2 on 28/5/15.
//  Copyright (c) 2015 MacCMS2. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "HCSStarRatingView.h"

@interface SharePurchaseViewController : UIViewController

@property (weak, nonatomic) IBOutlet UIImageView *imgDish;

@property (weak, nonatomic) IBOutlet UILabel *lbDish;
@property (weak, nonatomic) IBOutlet UILabel *lbRestaurant;
@property (weak, nonatomic) IBOutlet UILabel *lbRate;
@property (weak, nonatomic) IBOutlet HCSStarRatingView *start;
@property (weak, nonatomic) IBOutlet UIView *viewPopUp;
-(void)showInView:(UIViewController*)view;
@property (weak, nonatomic) IBOutlet UIButton *btClose;
-(void)removerFromView;
@property (weak, nonatomic) IBOutlet UIButton *btShareGray;
@property (weak, nonatomic) IBOutlet UIButton *btShareGreen;
@property (weak, nonatomic) IBOutlet UIButton *btRating;

@end
