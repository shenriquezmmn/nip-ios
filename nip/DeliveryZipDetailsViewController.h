//
//  DeliveryZipDetailsViewController.h
//  nip
//
//  Created by MacCMS2 on 27/04/15.
//  Copyright (c) 2015 MacCMS2. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "VisibleFormViewController.h"

@interface DeliveryZipDetailsViewController : VisibleFormViewController

//Header
@property (strong, nonatomic) NavBarViewController *navBar;

@property (weak, nonatomic) IBOutlet UIImageView *imgLogo;
@property (weak, nonatomic) IBOutlet UITextField *txStreet;
@property (weak, nonatomic) IBOutlet UITextField *txUnit;
@property (weak, nonatomic) IBOutlet UITextField *txCity;
@property (weak, nonatomic) IBOutlet UITextField *txState;
@property (weak, nonatomic) IBOutlet UITextField *txAddresLabel;
@property (weak, nonatomic) IBOutlet UITextField *txPhoneNumber;
@property (weak, nonatomic) IBOutlet UITextField *txZipCode;
@property (weak, nonatomic) IBOutlet UIButton *btContinue;

@property (strong, nonatomic) NSString *zipCode;
@property (strong, nonatomic) NSString *street;
@property (strong, nonatomic) NSString *numStreet;
@property (strong, nonatomic) NSString *city;
@property (strong, nonatomic) NSString *state;

@end
