//
//  TutorialViewController.m
//  NIP
//
//  Created by MacCMS2 on 10/8/15.
//  Copyright (c) 2015 MacCMS2. All rights reserved.
//

#import "TutorialViewController.h"
#import "HomeViewController.h"

@interface TutorialViewController ()

@end

@implementation TutorialViewController

@synthesize imgTutorial;

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    
    
    if([[UIScreen mainScreen] bounds].size.height == 480)
    {
        imgTutorial.image = [UIImage imageNamed:@"Tutorial3"];
    }
    else if([[UIScreen mainScreen] bounds].size.height == 568)
    {
        imgTutorial.image = [UIImage imageNamed:@"Tutorial2"];
    }
    else
    {
        imgTutorial.image = [UIImage imageNamed:@"Tutorial1"];
    }
    
    UITapGestureRecognizer *hidden = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(hiddenTutorial)];
    imgTutorial.userInteractionEnabled = YES;
    [imgTutorial addGestureRecognizer:hidden];
    imgTutorial.contentMode = UIViewContentModeScaleAspectFill;
 

}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)hiddenTutorial
{
    HomeViewController *home = [[HomeViewController alloc] init];
    home.haveAlreadyReceivedCoordinates = NO;
    home.logoTransparente.hidden = NO;
    [self.navigationController pushViewController:home animated:NO];
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
