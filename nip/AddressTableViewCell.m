//
//  AddressTableViewCell.m
//  NIP
//
//  Created by MacCMS2 on 18/05/15.
//  Copyright (c) 2015 MacCMS2. All rights reserved.
//

#import "AddressTableViewCell.h"

@implementation AddressTableViewCell
@synthesize lbCity, lbAddress, imgLabel, btEdit;

- (void)awakeFromNib {
    // Initialization code
    lbAddress.textColor = LABEL_COLOR;
    lbCity.textColor = LABEL_COLOR;
      
}

- (void)setSelected:(BOOL)selected animated:(BOOL)animated {
    [super setSelected:selected animated:animated];

    // Configure the view for the selected state
}
- (IBAction)editAddress:(UIButton *)sender {
    
}

@end
