//
//  PopUpViewController.h
//  Prueba
//
//  Created by MacCMS2 on 07/04/15.
//  Copyright (c) 2015 MacCMS2. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <QuartzCore/QuartzCore.h>

@interface PopUpViewController : UIViewController

@property (weak, nonatomic) IBOutlet UIView *popUp;
@property (weak, nonatomic) IBOutlet UILabel *lbText;
@property (weak, nonatomic) IBOutlet UILabel *lbTitle;

- (void)showInView:(UIView *)aView;

@end
