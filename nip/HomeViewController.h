//
//  HomeViewController.h
//  nip
//
//  Created by MacCMS2 on 27/04/15.
//  Copyright (c) 2015 MacCMS2. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "iCarousel.h"
#import "PopUpViewController.h"
#import <CoreLocation/CoreLocation.h>
#import <CoreLocation/CLLocationManagerDelegate.h>
#import "SharePurchaseViewController.h"


@interface HomeViewController : UIViewController <iCarouselDataSource, iCarouselDelegate, UIAlertViewDelegate, CLLocationManagerDelegate>

//Header
@property (strong, nonatomic) NavBarViewController *navBar;

//Carousel
@property (weak, nonatomic) IBOutlet iCarousel *carousel;
@property (weak, nonatomic) IBOutlet UILabel *lbAddress;
@property (weak, nonatomic) IBOutlet UIImageView *imgAddress;
@property (weak, nonatomic) IBOutlet UIView *viewPopUp;

@property (weak, nonatomic) IBOutlet UIView *viewDetailsMenu;
@property (nonatomic)  BOOL cancelOrder;
@property (nonatomic)  BOOL haveAlreadyReceivedCoordinates;
@property (weak, nonatomic) IBOutlet UILabel *lbText;

@property (strong, nonatomic) PopUpViewController *popViewController;
@property (weak, nonatomic) IBOutlet UILabel *lbTitle;
@property (weak, nonatomic) IBOutlet UIView *popUp;
@property (weak, nonatomic) IBOutlet UIButton *btAccept;
@property (weak, nonatomic) IBOutlet UIButton *btCancel;
@property (weak, nonatomic) IBOutlet UIImageView *imgFace;

//GridMenu
@property (nonatomic, strong) IBOutlet UIButton *showGridButton;

@property (strong, nonatomic) SharePurchaseViewController *share;

@property (weak, nonatomic) IBOutlet UIView *logoTransparente;

@end
