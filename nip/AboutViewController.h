//
//  AboutViewController.h
//  NIP
//
//  Created by MacCMS2 on 29/5/15.
//  Copyright (c) 2015 MacCMS2. All rights reserved.
//

#import <UIKit/UIKit.h>
#import <MessageUI/MFMailComposeViewController.h>

@interface AboutViewController : UIViewController<MFMailComposeViewControllerDelegate>

//Header
@property (strong, nonatomic) NavBarViewController *navBar;
@property (weak, nonatomic) IBOutlet UILabel *lbEmail;
@property (weak, nonatomic) IBOutlet UILabel *lbWebSite;
@property (weak, nonatomic) IBOutlet UILabel *lbTwitter;


@end
