//
//  PaymentViewController.h
//  nip
//
//  Created by MacCMS2 on 12/05/15.
//  Copyright (c) 2015 MacCMS2. All rights reserved.
//

#import <UIKit/UIKit.h>
#import "PTKView.h"
#import "STPCard.h"

@interface PaymentViewController : UIViewController<UIScrollViewDelegate,PTKViewDelegate, UITableViewDataSource, UITableViewDelegate>
@property(weak, nonatomic) PTKView *paymentView;

//Stripe
@property (strong, nonatomic) STPCard* stripeCard;
@property ( nonatomic) int addressIndex;

//Header
@property (strong, nonatomic) NavBarViewController *navBar;

//Table view
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property (weak, nonatomic) IBOutlet UILabel *lbSelect;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *tableViewHeightConstraint;

//Add card
@property (weak, nonatomic) IBOutlet UIView *viewAddCard;
@property (strong, nonatomic) IBOutlet UITableViewCell *cellNewCard;
@property (weak, nonatomic) IBOutlet UITextField *textField;
@property (weak, nonatomic) IBOutlet UILabel *lbTitle;
@property (weak, nonatomic) IBOutlet UIButton *btAdd;
@property (weak, nonatomic) IBOutlet UIScrollView *scrollView;
@end
