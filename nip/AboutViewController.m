//
//  AboutViewController.m
//  NIP
//
//  Created by MacCMS2 on 29/5/15.
//  Copyright (c) 2015 MacCMS2. All rights reserved.
//

#import "AboutViewController.h"
#import "AboutNipViewController.h"

@interface AboutViewController ()

@end

@implementation AboutViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view from its nib.
    self.view.backgroundColor = ORANGE_COLOR;
    self.navBar = [[NavBarViewController alloc] init];    
    
    UITapGestureRecognizer * tapTwitterLabel = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapOnTwitter:)];
    [self.lbTwitter addGestureRecognizer:tapTwitterLabel];
    
    UITapGestureRecognizer * tapEmailLabel = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapOnEmail:)];
    [self.lbEmail addGestureRecognizer:tapEmailLabel];
    
    UITapGestureRecognizer * tapSiteLabel = [[UITapGestureRecognizer alloc] initWithTarget:self action:@selector(tapOnSite:)];
    [self.lbWebSite addGestureRecognizer:tapSiteLabel];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}
- (IBAction)closeAbout:(id)sender {
    
    AboutNipViewController *about = [[AboutNipViewController alloc]init];
    [self.navBar pushTransition:self];
    [self.navigationController pushViewController:about animated:NO];
}

- (void) tapOnTwitter:(UITapGestureRecognizer *)tapRecognizer
{
    UIApplication * ourApplication = [UIApplication sharedApplication];
    
    if ([ourApplication canOpenURL:[NSURL URLWithString:@"twitter://"]])
        [ourApplication openURL:[NSURL URLWithString:@"twitter://user?screen_name=mobmedianet"]];
    else
        [ourApplication openURL:[NSURL URLWithString:@"http://twitter.com/mobmedianet"]];
}

- (void) tapOnEmail:(UITapGestureRecognizer *)tapRecognizer
{
    [self sendEmailTo:[NSArray arrayWithObjects:@"contacto@mobmedianet.com", nil]];
}

- (void) tapOnSite:(UITapGestureRecognizer *)tapRecognizer
{
    [[UIApplication sharedApplication] openURL:[NSURL URLWithString:@"http://www.mobilemedia.net"]];
}

#pragma mark - Email management

- (void) sendEmailTo:(NSArray *) recipients
{
    MFMailComposeViewController * mailViewController = [[MFMailComposeViewController alloc] init];
    
    mailViewController.mailComposeDelegate = self;
    [mailViewController setToRecipients:recipients];
    [mailViewController setSubject:@"Nip - Contact"];
    [mailViewController setMessageBody:@"" isHTML:NO];
    mailViewController.modalPresentationStyle = UIModalPresentationFormSheet;
    
    if (mailViewController)
        [self presentViewController:mailViewController animated:YES completion:nil];
}

- (void) mailComposeController:(MFMailComposeViewController *)controller
           didFinishWithResult:(MFMailComposeResult)result
                         error:(NSError *)error
{
    [self dismissViewControllerAnimated:YES completion:nil];
}


/*
#pragma mark - Navigation

// In a storyboard-based application, you will often want to do a little preparation before navigation
- (void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender {
    // Get the new view controller using [segue destinationViewController].
    // Pass the selected object to the new view controller.
}
*/

@end
