//
//  NavBarViewController.h
//  Prueba
//
//  Created by MacCMS2 on 07/04/15.
//  Copyright (c) 2015 MacCMS2. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface NavBarViewController : UIViewController

@property (weak, nonatomic) IBOutlet UIButton *btMenu;

- (void)showInView:(UIViewController *)aView withTitle: (NSString *)title hiddenButtom: (BOOL)yesOrNot;
-(void)popTransition: (UIViewController *)view;
-(void)pushTransition: (UIViewController *)view;
@property (weak, nonatomic) IBOutlet UILabel *lbAddress;
@property (weak, nonatomic) IBOutlet UIButton *btAddress;
@property (weak, nonatomic) IBOutlet UIImageView *imgAddress;
@property (weak, nonatomic) IBOutlet UIImageView *imgMenu;
@property (weak, nonatomic) IBOutlet UIImageView *imgBck;
@property (weak, nonatomic) IBOutlet UIImageView *imgCart;
@property (weak, nonatomic) IBOutlet UIButton *btCart;
@property (weak, nonatomic) IBOutlet UIImageView *btOrder;

@property (weak, nonatomic) IBOutlet NSLayoutConstraint *horizontalConstraint;
@property (weak, nonatomic) IBOutlet UIImageView *imgOrder;

@end
