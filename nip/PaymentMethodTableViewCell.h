//
//  PaymentMethodTableViewCell.h
//  nip
//
//  Created by MacCMS2 on 14/05/15.
//  Copyright (c) 2015 MacCMS2. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface PaymentMethodTableViewCell : UITableViewCell
@property (weak, nonatomic) IBOutlet UILabel *lbEndingIn;
@property (weak, nonatomic) IBOutlet UILabel *lbDate;
@property (weak, nonatomic) IBOutlet UIImageView *imgCardType;
@property (weak, nonatomic) IBOutlet UIImageView *imgEdit;

@end
